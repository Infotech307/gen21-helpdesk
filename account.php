<?php
require('client.inc.php');
require_once(INCLUDE_DIR.'class.ldap.php');
require_once(INCLUDE_DIR.'class.user.php');
$errors=array();
global $ldapcon;

if(!$thisclient || !$thisclient->isValid()){

}
if($ldapcon->ldapClientActive()){
	if($ldapcon->ldapActive())
	{
		if($ldapcon->ldapClientForceLogin())
		{
			if(!$thisclient)
			{
				//XXX: Ticket owner is assumed.
				@header('Location: login.php');
				require_once('login.php'); //Just in case of 'header already sent' error.
				exit;
			}
		}
	}
	else {
		@header('Location: login.php');
		require_once('login.php'); //Just in case of 'header already sent' error.
		exit;
	}
}
else if(!$thisclient){
	@header('Location: login.php');
	require_once('login.php'); //Just in case of 'header already sent' error.
	exit;
}
else {

}

if($_POST):
    $vars = $_POST;
    //User::Edit...checks for errors..
    if(($u=User::update($vars, $errors))){
        $msg='Success!';
		@header('Location: account.php?msg=Success');
    }else{
        $errors['err']=$errors['err']?$errors['err']:'Unable to register user. Please correct errors below and try again!';	
    }
	
endif;

//page
$nav->setActiveNav('new');
require(CLIENTINC_DIR.'header.inc.php');
require(CLIENTINC_DIR.'account.inc.php');
require(CLIENTINC_DIR.'footer.inc.php');
?>
