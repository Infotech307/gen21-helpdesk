<?php
/*********************************************************************
    class.clients.php

    Clients helper
**********************************************************************/

class Clients {
    var $id;

    var $div;

    var $parent;
    var $page;

    function Clients($id) {
        $this->id=0;
        $this->load($id);
    }

    function load($id=0) {

        if(!$id && !($id=$this->getId()))
            return false;

        $sql='SELECT * '            
            .' FROM ost_clients WHERE id='.db_input($id);

        if(!($res=db_query($sql)) || !db_num_rows($res))
            return false;

        $this->dv = db_fetch_array($res);
        $this->id = $this->dv['id'];
        $this->page = null;


        return true;
    }

    function reload() {
        return $this->load();
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->dv['name'];
    }
	
	function getGroupclients() {
        return $this->dv['groupclients_id'];
    }
	
	function getDefault() {
        return $this->dv['default'];
    }
	
	function getStatus() {
        return $this->dv['status'];
    }

    function update($vars, &$errors) {

        if(!$this->save($this->getId(), $vars, $errors))
            return false;

        $this->reload();
        return true;
    }

    function delete() {

        $sql='DELETE FROM ost_clients WHERE id='.db_input($this->getId()).' LIMIT 1';
        if(db_query($sql) && ($num=db_affected_rows())) {

        }

        return $num;
    }
    /*** Static functions ***/
    function create($vars, &$errors) { //used
        return self::save(0, $vars, $errors);
    }

    function getClientss($publicOnly=false) {

        $locations=array();
        $sql='SELECT ClientsID '
            .' FROM '.LOCATION_TABLE
            .' WHERE isactive=1';

        $sql.=' ORDER BY ClientsName';
        if(($res=db_query($sql)) && db_num_rows($res))
            while(list($id, $name)=db_fetch_row($res))
                $locations[$id]=$name;

        return $locations;
    }

//    function getPublicDivisions() {
//        return self::getDivisions(true);
//    }

    function getIdByName($name, $gid) { //used

        $sql='SELECT id FROM ost_clients WHERE name='.db_input($name).' and groupclients_id = '.$gid;
        if(($res=db_query($sql)) && db_num_rows($res))
            list($id) = db_fetch_row($res);

        return $id;
    }

    function lookup($id) {
        return ($id && is_numeric($id) && ($t= new Clients($id)) && $t->getId()==$id)?$t:null;
    }

    function save($id, $vars, &$errors) { //used

        $vars['name']=Format::striptags($vars['name']);
		if($vars['name'] == '' || empty($vars['name'])){
			$errors['name']="Name can't be empty";
		}
		if($vars['groupclients'] == '' || empty($vars['groupclients'])){
			$errors['groupclients']="Group Client must be choose";
		}
        if(($tid=self::getIdByName($vars['name'],$vars['groupclients'])) && $tid!=$id)
            $errors['name']="Client name for this group client already exists";

        if($errors) return false;

        $sql=' name='.db_input($vars['name'])           
            .',groupclients_id='.db_input($vars['groupclients'])
            .',status='.db_input($vars['status']);

      
        if($id) {
            $sql='UPDATE ost_clients SET '.$sql.' WHERE id='.db_input($id);
            if(db_query($sql)){
				if($vars['default'] == 1){
					
					$sql1 = 'update ost_clients set `default` = 0 where groupclients_id ='.db_input($vars['groupclients']);
					db_query($sql1);
					
					$sql2 = 'update ost_clients set `default` = 1 where id ='.db_input($id);
					db_query($sql2);
				}
                return true;
			}
            $errors['err']='Unable to update Client. Data already exist';
        } else {
            $sql='INSERT INTO ost_clients SET '.$sql.'';
            if(db_query($sql) && ($id=db_insert_id())){
				if($vars['default'] == 1){
					$sql1 = 'update ost_clients set default = 0 where groupclients_id ='.db_input($vars['groupclients']);
					db_query($sql1);
					
					$sql2 = 'update ost_clients set default = 1 where id ='.db_input($id);
					db_query($sql2);
				}
                return $id;
			}
            $errors['err']='Unable to create Client. Data already exist . ';
        }
		
        return false;
    }
}
?>
