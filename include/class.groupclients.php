<?php
/*********************************************************************
    class.groupclients.php

    Group Clients helper
**********************************************************************/

class Groupclients {
    var $id;

    var $div;

    var $parent;
    var $page;

    function Groupclients($id) {
        $this->id=0;
        $this->load($id);
    }

    function load($id=0) {

        if(!$id && !($id=$this->getId()))
            return false;

        $sql='SELECT * '            
            .' FROM ost_groupclients WHERE id='.db_input($id);

        if(!($res=db_query($sql)) || !db_num_rows($res))
            return false;

        $this->dv = db_fetch_array($res);
        $this->id = $this->dv['id'];
        $this->page = null;


        return true;
    }

    function reload() {
        return $this->load();
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->dv['name'];
    }
	
	function getStatus() {
        return $this->dv['status'];
    }

    function update($vars, &$errors) {

        if(!$this->save($this->getId(), $vars, $errors))
            return false;

        $this->reload();
        return true;
    }

    function delete() {

        $sql='DELETE FROM ost_groupclients WHERE id='.db_input($this->getId()).' LIMIT 1';
        if(db_query($sql) && ($num=db_affected_rows())) {

        }

        return $num;
    }
    /*** Static functions ***/
    function create($vars, &$errors) { //used
        return self::save(0, $vars, $errors);
    }

    function getGroupclientss($publicOnly=false) {

        $locations=array();
        $sql='SELECT GroupclientsID '
            .' FROM '.LOCATION_TABLE
            .' WHERE isactive=1';

        $sql.=' ORDER BY GroupclientsName';
        if(($res=db_query($sql)) && db_num_rows($res))
            while(list($id, $name)=db_fetch_row($res))
                $locations[$id]=$name;

        return $locations;
    }

//    function getPublicDivisions() {
//        return self::getDivisions(true);
//    }

    function getIdByName($name, $pid=0) { //used

        $sql='SELECT id FROM ost_groupclients WHERE name='.db_input($name);
        if(($res=db_query($sql)) && db_num_rows($res))
            list($id) = db_fetch_row($res);

        return $id;
    }

    function lookup($id) {
        return ($id && is_numeric($id) && ($t= new Groupclients($id)) && $t->getId()==$id)?$t:null;
    }

    function save($id, $vars, &$errors) { //used

        $vars['name']=Format::striptags($vars['name']);
		if($vars['name'] == '' || empty($vars['name'])){
			$errors['name']="Name can't be empty";
		}
        if(($tid=self::getIdByName($vars['name'])) && $tid!=$id)
            $errors['name']="Name already exists";


        if($errors) return false;

        $sql=' name='.db_input($vars['name'])           
            .',status='.db_input($vars['status']);

      
        if($id) {
            $sql='UPDATE ost_groupclients SET '.$sql.' WHERE id='.db_input($id);
            if(db_query($sql))
                return true;

            $errors['err']='Unable to update Group Client. Data already exist';
        } else {
            $sql='INSERT INTO ost_groupclients SET '.$sql.'';
            if(db_query($sql) && ($id=db_insert_id()))
                return $id;

            $errors['err']='Unable to create the Group Client. Data already exist';
        }

        return false;
    }
}
?>
