<?php
class issuetype {
    var $id;

    var $div;

    var $parent;
    var $page;

    function issuetype($id) {
        $this->id=0;
        $this->load($id);
    }

    function load($id=0) {

        if(!$id && !($id=$this->getId()))
            return false;

        $sql='SELECT * '            
            .' FROM '.ISSUETYPE_TABLE
            .' WHERE issueTypeID='.db_input($id);

        if(!($res=db_query($sql)) || !db_num_rows($res))
            return false;

        $this->dv = db_fetch_array($res);
        $this->id = $this->dv['issueTypeID'];
        $this->page = null;


        return true;
    }

    function reload() {
        return $this->load();
    }

    function asVar() {
        return $this->getName();
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->dv['issueTypeName'];
    }

    function getPageId() {
        return $this->dv['page_id'];
    }

    function getPage() {
        if(!$this->page && $this->getPageId())
            $this->page = Page::lookup($this->getPageId());

        return $this->page;
    }


    function isEnabled() {
         return ($this->dv['isactive']);
    }

    function isActive() {
        return $this->isEnabled();
    }


    function getHashtable() {
        return $this->dv;
    }

    function getInfo() {
        return $this->getHashtable();
    }

    function update($vars, &$errors) {

        if(!$this->save($this->getId(), $vars, $errors))
            return false;

        $this->reload();
        return true;
    }

    function delete() {

        $sql='DELETE FROM '.ISSUETYPE_TABLE.' WHERE issueTypeID='.db_input($this->getId()).' LIMIT 1';
        if(db_query($sql) && ($num=db_affected_rows())) {

        }

        return $num;
    }
    /*** Static functions ***/
    function create($vars, &$errors) {
        return self::save(0, $vars, $errors);
    }

    function getIssueTypes($publicOnly=false) {

        $issuetypes=array();
        $sql='SELECT issueTypeID '
            .' FROM '.ISSUETYPE_TABLE
            .' WHERE isactive=1';

        $sql.=' ORDER BY issueTypeName';
        if(($res=db_query($sql)) && db_num_rows($res))
            while(list($id, $name)=db_fetch_row($res))
                $issuetypes[$id]=$name;

        return $issuetypes;
    }

//    function getPublicDivisions() {
//        return self::getDivisions(true);
//    }

    function getIdByName($name, $pid=0) {

        $sql='SELECT issueTypeID FROM '.ISSUETYPE_TABLE
            .' WHERE issueTypeName='.db_input($name);
        if(($res=db_query($sql)) && db_num_rows($res))
            list($id) = db_fetch_row($res);

        return $id;
    }

    function lookup($id) {
        return ($id && is_numeric($id) && ($t= new issuetype($id)) && $t->getId()==$id)?$t:null;
    }

    function save($id, $vars, &$errors) {

        $vars['issueTypeName']=Format::striptags(trim($vars['issueTypeName']));

        if(($tid=self::getIdByName($vars['issueTypeName'])) && $tid!=$id)
            $errors['issuetype']='Issue Type already exists';


        if($errors) return false;

        $sql=' updated=NOW() '
            .',issueTypeName='.db_input($vars['issuetype'])           
            .',isactive='.db_input($vars['isactive']);

      
        if($id) {
            $sql='UPDATE '.ISSUETYPE_TABLE.' SET '.$sql.' WHERE issueTypeID='.db_input($id);
            if(db_query($sql))
                return true;

            $errors['err']='Unable to update issue type. Data already exist';
        } else {
            $sql='INSERT INTO '.ISSUETYPE_TABLE.' SET '.$sql.',created=NOW()';
            if(db_query($sql) && ($id=db_insert_id()))
                return $id;

            $errors['err']='Unable to create issue type. Data already exist';
        }

        return false;
    }
}
?>
