<?php
/*********************************************************************
    class.topic.php

    Help topic helper

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

class kategori {
    var $id;

    var $ht;

    var $parent;
    var $page;
	var $res;



    function getKategori() {

     //   $kats=array();
        $sql='SELECT * FROM '.CATEGORY_TABLE;

		if(!($res=db_query($sql)) || !db_num_rows($res))
            return NULL;

        $this->ht=db_fetch_array($res);
        $this->id  = $this->ht['CategoryID'];
		$this->name= $this->ht['CategoryName'];		
		
		  	function getID(){
        	return $this->id;
			}
		
			function getName(){
				return $this->name;
			} 
      //  return $kats;
    }

    function getKategoriList() {
        return self::getKategori(true);
    }
	

}
?>
