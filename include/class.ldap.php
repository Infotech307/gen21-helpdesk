<?php
include_once(ROOT_DIR.'main.inc.php');
class LDAP {
	const ldapdb_version=1;
	var $global_settings;
	
	function LDAP()
	{
		$this->global_settings=null;
		$sqlquery='SHOW TABLES LIKE "' . TABLE_PREFIX . 'ldap_globalconfig";';
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			$sqlquery='SELECT dbversion, maxlogs, ldap_client_active, ldap_client_autofill, ldap_client_forcelogin, ldap_client_force_ldaplogin, ldap_client_overwrite_email, ldap_use_sso, ldap_auth_var, ldap_debug FROM ' .TABLE_PREFIX. 'ldap_globalconfig;';
			if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
			{
				$this->global_settings=db_fetch_array($tmp_res);
			}
		}
		else
		{
			$this->upgradeDB();
		}
	}
	
	function ldapSqlConnect($ldap_id,&$outp=NULL,$debug=false) {
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_controller, ' . TABLE_PREFIX . 'ldap_config.ldap_port from ' . TABLE_PREFIX . 'ldap_config WHERE ldap_id = '.$ldap_id.';';
		$this->ldapLog(__FUNCTION__."(".__LINE__."): fetching ldap connections");
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			$tmp_row=db_fetch_array($tmp_res);
			if($debug==true)
			{
				$outp.='calling ldap_connect with: "';
			}
			if($this->useSSL($ldap_id)==true)
			{
				$ldap = ldap_connect('ldaps://'.$tmp_row['ldap_controller'].':'.$tmp_row['ldap_port']);
				$this->ldapLog(__FUNCTION__."(".__LINE__."): calling ldap_connect with ".'ldaps://'.$tmp_row['ldap_controller'].':'.$tmp_row['ldap_port'].'"');
				$outp.='ldaps://'.$tmp_row['ldap_controller'].':'.$tmp_row['ldap_port'].'"<br>';
				if(!$ldap)
				{
					if($debug==true)
					{
						$outp.=ldap_error($ldap).'<br>';
						$outp.='errno: '.strval(ldap_errno($ldap)).'<br>';
					}
					$this->ldapLog(__FUNCTION__."(".__LINE__."): error connecting to ldap:");
					$this->ldapLog(__FUNCTION__."(".__LINE__."): ".ldap_error($ldap));
					$this->ldapLog(__FUNCTION__."(".__LINE__."): errno: ".strval(ldap_errno($ldap)));
				}
			}
			else
			{
				$ldap = ldap_connect($tmp_row['ldap_controller'], $tmp_row['ldap_port']);
				$this->ldapLog(__FUNCTION__."(".__LINE__."): calling ldap_connect with ".$tmp_row['ldap_controller']." and port ".$tmp_row['ldap_port']);
				if($debug==true)
				{
					$outp.=$tmp_row['ldap_controller'] . '" and port "' . $tmp_row['ldap_port'] . '"<br>';
				}
				if(!$ldap)
				{
					$this->ldapLog(__FUNCTION__."(".__LINE__."): error connecting to ldap:");
					$this->ldapLog(__FUNCTION__."(".__LINE__."): ".ldap_error($ldap));
					$this->ldapLog(__FUNCTION__."(".__LINE__."): errno: ".strval(ldap_errno($ldap)));
				}
			}
		}
		else
		{
			if($debug==true)
			{
				$outp.='no ldap config<br>';
			}
		}
		if($ldap)
		{
			if($debug==true)
			{
				$outp.='setting LDAP_OPT_PROTOCOL_VERSION to 3 and LDAP_OPT_REFERRALS to 0<br>';
			}
			ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
			$this->ldapLog(__FUNCTION__."(".__LINE__."): set LDAP_OPT_PROTOCOL_VERSION to 3");
			ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
			$this->ldapLog(__FUNCTION__."(".__LINE__."): set LDAP_OPT_REFERRALS to 0");
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
        return $ldap ? $ldap : false;
	}

    function ldapAuthenticate($username, $password) {
		return $this->ldapSqlAuthenticate($username, $password);
	}
	
    function ldapSqlAuthenticate($username, $password,$ldap_id=-1,&$outp=NULL, $debug=false) {
	    
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
        if($password=='')
        {
			$this->ldapLog(__FUNCTION__."(".__LINE__."): password empty, done");
            return false;
        }
        $sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_id, ' . TABLE_PREFIX . 'ldap_config.ldap_suffix, ' . TABLE_PREFIX . 'ldap_config.ldap_rdn, ' . TABLE_PREFIX . 'ldap_config.ldap_admin, ' . TABLE_PREFIX . 'ldap_config.ldap_admin_cn from ' . TABLE_PREFIX . 'ldap_config';
        if($ldap_id!=-1)
        {
            $sqlquery.=' WHERE ' . TABLE_PREFIX . 'ldap_config.ldap_id='.$ldap_id;
        }
        $sqlquery.=' ORDER BY ' . TABLE_PREFIX . 'ldap_config.priority';
		$this->ldapLog(__FUNCTION__."(".__LINE__."): fetching settings from the database");
        if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
        {
            while($rowset = db_fetch_array($tmp_res)) {
                $ldap = $this->ldapSqlConnect($rowset['ldap_id']);
				//retno\
				//$ldap = "infotech--.MAP.local";
				$this->ldapLog(__FUNCTION__."(".__LINE__."): disabling php error reporting");
                $old_error_reporting = error_reporting();
                
                if($debug==false)
                {
                    error_reporting (E_ERROR);
                }
                $ldapusr="";
				if(!$this->useRDN($rowset['ldap_id']))
				{
					$this->ldapLog(__FUNCTION__."(".__LINE__."): using username@suffix for this connection");
					if(strpos($username,$rowset['ldap_suffix'])!==false)
					{
						$ldapusr=$username;
					}
					else
					{
						$ldapusr=$username . $rowset['ldap_suffix'];
					}
				}
				else
				{
					$this->ldapLog(__FUNCTION__."(".__LINE__."): using RDN for this connection");
					$ldapusr=str_replace('%UID%',$username,$rowset['ldap_rdn']);
					if($username==$rowset['ldap_admin'])
					{
						$ldapusr=str_replace('%CN%',$rowset['ldap_admin_cn'],$ldapusr);
					}
					else
					{
						$usercn=$this->ldapGetField($rowset['ldap_id'],'cn',$username);
						$ldapusr=str_replace('%CN%',$usercn,$ldapusr);
					}
					if($debug==true)
					{
						$outp.='using rdn for binding<br>';
					}
				}
                if($debug==true)
                {
                    $outp.='binding to ldap with "'.$ldapusr.'" and his password<br>';
                }
				$this->ldapLog(__FUNCTION__."(".__LINE__."): binding to ldap with ".$ldapusr." and his password");
				
                $bind = ldap_bind($ldap, $ldapusr, $password);
				//retno
				//$bind = ldap_bind($ldap,"Admin.Helpdesk","Helpapp01");
				//echo $ldap." -****- ".$ldapusr." -****- ".$password;
                if(!$bind)
                {
                    if($debug==true)
                    {
                        $outp.=ldap_error($ldap).'<br>';
                        $outp.='errno: '.strval(ldap_errno($ldap)).'<br>';
                    }
					$this->ldapLog(__FUNCTION__."(".__LINE__."): bind error: ".ldap_error($ldap));
					$this->ldapLog(__FUNCTION__."(".__LINE__."): errno: ".strval(ldap_errno($ldap)));
                }
				else
				{
					$this->ldapLog(__FUNCTION__."(".__LINE__."): bind successful, authenticated");
				}
                ldap_unbind($ldap);
                if($debug==false)
                {
                    error_reporting($old_error_reporting);
                }
                if($bind)
                {
                    break;
                }
            }
			$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
            return $bind;
        }
        else
        {
            if($debug==true)
            {
                $outp.='no ldap config<br>';
            }
        }
		$this->ldapLog(__FUNCTION__."(".__LINE__."): no configuration, done");
        return false;
    }
	
	function ldapGetField($ldap_id, $field,$username, &$outp=NULL, $debug=false)
	{
		$getSlash = strpos($username,"\\");
		if($getSlash){
			$indeksAfterSlash = $getSlash + 1;
			$lastIndeks = strlen($username);
			$username = substr($username,$indeksAfterSlash,$lastIndeks);
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_id, ' . TABLE_PREFIX . 'ldap_config.ldap_suffix, ' . TABLE_PREFIX . 'ldap_config.ldap_rdn, ' . TABLE_PREFIX . 'ldap_config.ldap_admin_cn, ' . TABLE_PREFIX . 'ldap_config.ldap_filter, ' . TABLE_PREFIX . 'ldap_config.ldap_admin, ' . TABLE_PREFIX . 'ldap_config.ldap_domain from ' . TABLE_PREFIX . 'ldap_config WHERE ldap_id='.$ldap_id.';';
		$fieldcontent="";
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			$rowset = db_fetch_array($tmp_res); 
			$ldap = $this->ldapSqlConnect($rowset['ldap_id'],$outp,true);
			if ($ldap) {
				if($this->ldapSqlAuthenticate($rowset['ldap_admin'],$this->getPasswd($rowset['ldap_id']),$ldap_id,$outp,true))
				{
					$ldapusr="";
					if(!$this->useRDN($rowset['ldap_id']))
					{
						$ldapusr=$rowset['ldap_admin'] . $rowset['ldap_suffix'];
					}
					else
					{
						$ldapusr=str_replace('%UID%',$rowset['ldap_admin'],$rowset['ldap_rdn']);
						$ldapusr=str_replace('%CN%',$rowset['ldap_admin_cn'],$ldapusr);
					}
					$this->ldapLog(__FUNCTION__."(".__LINE__."): binding with ".$ldapusr);
					$bind = ldap_bind($ldap, $ldapusr, $this->getPasswd($rowset['ldap_id']));
					if($bind)
					{
						$this->ldapLog(__FUNCTION__."(".__LINE__."): bind successful");
						$LDAPFieldsToFind = array($field);
						if($username=="")
						{
							$username=$rowset['ldap_admin'];
						}
						$ldapFilter="";
						if($rowset['ldap_filter']==""||$rowset['ldap_filter']==NULL)
						{
							$ldapFilter='(&(sAMAccountName='.$username.'))';
							if(debug==true)
							{
								$outp.='using the default filter: "'.$ldapFilter.'"<br>';
							}
						}
						else
						{
							//$ldapFilter=str_replace('%USERNAME%',$username,$rowset['ldap_filter']);
							//retno
							$ldapFilter=str_replace('%USERNAME%',$username,$rowset['ldap_filter']);
							if(debug==true)
							{
								$outp.='using the filter: "'.$ldapFilter.'"<br>';
							}
						}
						if($debug==true)
						{
							$outp.='calling ldap_search with the domain: "'.$rowset['ldap_domain'].'", the Filter: "'.$ldapFilter.'" and the Attributes: "array("'.$field.'")"<br>';
						}
						$this->ldapLog(__FUNCTION__."(".__LINE__."): ".'calling ldap_search with the domain: "'.$rowset['ldap_domain'].'", the Filter: "'.$ldapFilter.'" and the Attributes: "array("'.$field.'")"');
						$results = ldap_search($ldap, $rowset['ldap_domain'], $ldapFilter, $LDAPFieldsToFind);
						$info = ldap_get_entries($ldap, $results);
						$row=0;

						if($info["count"] > 0){
							$fieldcontent=$info[0][$field][0];
							if($debug==true)
							{
								$outp.='LDAP returned field data: "'.$fieldcontent.'"<br>';
							}
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP returned field data: ".$fieldcontent);
						}
						ldap_unbind($ldap);
					}
					else
					{
						if($debug==true)
						{
							$outp.="Bind failed, couldn't get field data<br>";
						}
						$this->ldapLog(__FUNCTION__."(".__LINE__."): bind error: ".ldap_error($ldap));
						$this->ldapLog(__FUNCTION__."(".__LINE__."): errno: ".strval(ldap_errno($ldap)));
					}
				}
				else
				{
					if($debug==true)
					{
						$outp.='Cannot authenticate with LDAP server.<br>';
					}
				}
			}else{
				if($debug==true)
				{
					$outp.='Cannot connect to LDAP server.<br>';
				}
			}
		}
		else
		{
			if($debug==true)
			{
				$outp.='no ldap config<br>';
			}
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
		return $fieldcontent;
	}
	
	function useSSL($ldap_id)
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_ssl"';
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_ssl from ' . TABLE_PREFIX . 'ldap_config WHERE ldap_id = '.$ldap_id.';';
			if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
			{
				$tmp_row=db_fetch_array($tmp_res);
				$this->ldapLog(__FUNCTION__."(".__LINE__."): ldaps ".($tmp_row['ldap_ssl']?"enabled":"disabled").", done");
				return $tmp_row['ldap_ssl'];
			}
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): ldaps disabled, done");
		return false;
	}
	
	function useRDN($ldap_id)
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_use_rdn"';
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_use_rdn from ' . TABLE_PREFIX . 'ldap_config WHERE ldap_id = '.$ldap_id.';';
			if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
			{
				$tmp_row=db_fetch_array($tmp_res);
				$this->ldapLog(__FUNCTION__."(".__LINE__."): ".($tmp_row['ldap_use_rdn']?"use ":"don't use")." RDN, done");
				return $tmp_row['ldap_use_rdn'];
			}
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): don't use RDN, done");
		return false;
	}
	
	function getTemporaryTicketNum($email)
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$sqlquery='SELECT email, subject FROM '.TICKET_TABLE.' T1 WHERE subject LIKE "ldap_temporary" AND email LIKE "'.$email.'"';
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			$this->ldapLog(__FUNCTION__."(".__LINE__."): found ".db_num_rows($tmp_res)." temporary ticket/s, done");
			return db_num_rows($tmp_res);
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): 0 temporary tickets, done");
		return 0;
	}
	
	function ldapDebug()
	{
		if($this->global_settings!=null)
		{
			return $this->global_settings["ldap_debug"];
		}
        return false;
	}
	
	function ldapClientForceLogin()
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		if($this->global_settings!=null)
		{
			return $this->global_settings["ldap_client_forcelogin"];
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): don't force client log in, done");
        return false;
	}
	
	function ldapClientForceLDAP()
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		if($this->global_settings!=null)
		{
			return $this->global_settings["ldap_client_force_ldaplogin"];
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): don't force client ldap, done");
        return false;
	}
	
	function ldapClientOverwriteEmail()
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		if($this->global_settings!=null)
		{
			return $this->global_settings["ldap_client_overwrite_email"];
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): don't force client ldap, done");
        return false;
	}
	
	function ldapGetAuthvar()
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		if($this->global_settings!=null)
		{
			return $this->global_settings["ldap_auth_var"];
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): AuthVar empty, done");
        return "";
	}
	
	function useSSO()
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		if($this->global_settings!=null)
		{
			return $this->global_settings["ldap_use_sso"];
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): don't use SSO, done");
        return false;
	}
	
	function ldapClientAutofill()
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		if($this->global_settings!=null)
		{
			return $this->global_settings["ldap_client_autofill"];
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): don't use Autofill, done");
        return false;
	}
	
	function ldapClientActive()
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		if($this->global_settings!=null)
		{
			return $this->global_settings["ldap_client_active"];
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): disable ldap for clients, done");
        return false;
	}
	
	function upgradeDB()
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting  ".__FUNCTION__);
		$sqlquery='SHOW TABLES LIKE "' . TABLE_PREFIX . 'ldap_config";';
		$this->ldapLog(__FUNCTION__."(".__LINE__."): checking if ldap Tables exist in the Database");
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			if($this->getLdapDBVersion()!=LDAP::ldapdb_version)
			{
				$this->ldapLog(__FUNCTION__."(".__LINE__."): checking if tables need an update");
				if($this->getLdapDBVersion()<1)
				{
					$this->ldapLog(__FUNCTION__."(".__LINE__."): updating tables");
					//check if the column 'ldap_filter' exists
					$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_filter"';
					if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
					{
						//do nothing
					}
					else
					{
						$sqlquery="ALTER TABLE `". TABLE_PREFIX . "ldap_config` ADD `ldap_filter` text COLLATE utf8_unicode_ci NOT NULL AFTER `ldap_suffix` ";
						db_query($sqlquery);
						$this->ldapLog(__FUNCTION__."(".__LINE__."): added column ldap_filter to ldap_config");
					}
					//check if the column 'ldap_rdn' exists
					$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_rdn"';
					if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
					{
						//do nothing
					}
					else
					{
						$sqlquery="ALTER TABLE `". TABLE_PREFIX . "ldap_config` ADD `ldap_rdn` text COLLATE utf8_unicode_ci NOT NULL AFTER `ldap_suffix` ";
						db_query($sqlquery);
						$this->ldapLog(__FUNCTION__."(".__LINE__."): added column ldap_filter to ldap_config");
					}
					//check if the column 'ldap_admin_cn' exists
					$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_admin_cn"';
					if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
					{
						//do nothing
					}
					else
					{
						$sqlquery="ALTER TABLE `". TABLE_PREFIX . "ldap_config` ADD `ldap_admin_cn` text COLLATE utf8_unicode_ci NOT NULL AFTER `ldap_suffix` ";
						db_query($sqlquery);
						$this->ldapLog(__FUNCTION__."(".__LINE__."): added column ldap_admin_cn to ldap_config");
					}
					//check if the column 'ldap_use_rdn' exists
					$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_use_rdn"';
					if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
					{
						//do nothing
					}
					else
					{
						$sqlquery="ALTER TABLE `". TABLE_PREFIX . "ldap_config` ADD `ldap_use_rdn` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `ldap_client_autofill` ";
						db_query($sqlquery);
						$this->ldapLog(__FUNCTION__."(".__LINE__."): added column ldap_use_rdn to ldap_config");
					}
					//check if the column 'ldap_use_sso' exists
					$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_use_sso"';
					if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
					{
						//do nothing
					}
					else
					{
						$sqlquery="ALTER TABLE `". TABLE_PREFIX . "ldap_config` ADD `ldap_use_sso` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `ldap_ssl` ";
						db_query($sqlquery);
						$this->ldapLog(__FUNCTION__."(".__LINE__."): added column ldap_use_sso to ldap_config");
					}
					//check if the column 'ldap_client_force_ldaplogin' exists
					$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_client_force_ldaplogin"';
					if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
					{
						//do nothing
					}
					else
					{
						$sqlquery="ALTER TABLE `". TABLE_PREFIX . "ldap_config` ADD `ldap_client_force_ldaplogin` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `ldap_use_sso` ";
						db_query($sqlquery);
						$this->ldapLog(__FUNCTION__."(".__LINE__."): added column ldap_client_force_ldaplogin to ldap_config");
					}
				}
				$sqlquery="CREATE TABLE IF NOT EXISTS `". TABLE_PREFIX . "ldaplogs` (
					`log_id` int(11) NOT NULL AUTO_INCREMENT,
					`data` text COLLATE utf8_unicode_ci NOT NULL,
					`created` DATETIME(6) COLLATE utf8_unicode_ci NOT NULL,
					 PRIMARY KEY (`log_id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;";
				db_query($sqlquery);
				$this->globalizeLDAP();
				$this->ldapLog(__FUNCTION__."(".__LINE__."): setting ldap dbversion to current");
				$sqlquery='UPDATE ' . TABLE_PREFIX . 'ldap_globalconfig SET ' . TABLE_PREFIX . 'ldap_globalconfig.dbversion = "'.LDAP::ldapdb_version.'" WHERE ' . TABLE_PREFIX . 'ldap_globalconfig.ldap_global_id = 1;';
				db_query($sqlquery);
			}
		}
		else
		{
			$this->ldapLog(__FUNCTION__."(".__LINE__."): creating ldap_config table");
			$sqlquery="CREATE TABLE IF NOT EXISTS `". TABLE_PREFIX . "ldap_config` (
				  `ldap_id` int(11) NOT NULL AUTO_INCREMENT,
				  `priority` int(11) NOT NULL,
				  `ldap_domain` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_suffix` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_filter` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_controller` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_use_rdn` tinyint(1) unsigned NOT NULL DEFAULT '0',
				  `ldap_rdn` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_admin` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_admin_cn` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_admin_pw` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_email_field` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_firstname_field` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_lastname_field` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_user_field` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_phone_field` text COLLATE utf8_unicode_ci NOT NULL,
				  `ldap_ext_length` int(11) NOT NULL,
				  `ldap_port` int(11) NOT NULL,
				  `ldap_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
				  `ldap_ssl` tinyint(1) unsigned NOT NULL DEFAULT '0',
				  PRIMARY KEY (`ldap_id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;";
				db_query($sqlquery);
				
			$this->ldapLog(__FUNCTION__."(".__LINE__."): creating ldaplogs table");
			$sqlquery="CREATE TABLE IF NOT EXISTS `". TABLE_PREFIX . "ldaplogs` (
				`log_id` int(11) NOT NULL AUTO_INCREMENT,
				`data` text COLLATE utf8_unicode_ci NOT NULL,
				`created` DATETIME COLLATE utf8_unicode_ci NOT NULL,
				 PRIMARY KEY (`log_id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;";
			db_query($sqlquery);
			$this->ldapLog(__FUNCTION__."(".__LINE__."): creating ldap_globalconfig table");
			$sqlquery="CREATE TABLE IF NOT EXISTS `". TABLE_PREFIX . "ldap_globalconfig` (
				`ldap_global_id` int(11) NOT NULL AUTO_INCREMENT,
				`dbversion` int(11) DEFAULT '1',
				`maxlogs` int(11) DEFAULT '2000',
				`ldap_client_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
				`ldap_client_autofill` tinyint(1) unsigned NOT NULL DEFAULT '0',
				`ldap_client_forcelogin` tinyint(1) unsigned NOT NULL DEFAULT '0',
				`ldap_client_force_ldaplogin` tinyint(1) unsigned NOT NULL DEFAULT '0',
				`ldap_client_overwrite_email` tinyint(1) unsigned NOT NULL DEFAULT '0',
				`ldap_debug` tinyint(1) unsigned NOT NULL DEFAULT '0',
				`ldap_use_sso` tinyint(1) unsigned NOT NULL DEFAULT '0',
				`ldap_auth_var` text COLLATE utf8_unicode_ci NOT NULL,
				 PRIMARY KEY (`ldap_global_id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";
			db_query($sqlquery);
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
	}
	
	function ldapActive()
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting  ".__FUNCTION__);
		$this->ldapLog(__FUNCTION__."(".__LINE__."): checking if there is an active ldap connection");
		$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_active FROM ' . TABLE_PREFIX . 'ldap_config WHERE ldap_active=1;';
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			$this->ldapLog(__FUNCTION__."(".__LINE__."): active ldap connection found, done with function ldapActive");
			return true;
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
        return false;
	}
	
	function getLdapDBVersion()
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		if($this->global_settings!=null)
		{
			return $this->global_settings["dbversion"];
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): table ldap_globalconfig doesn't exist yet, done");
		return 0;
	}
	
	function globalizeLDAP()
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$rows=array(array());
		$rownum=0;
		$saved_auth="";
		$this->ldapLog(__FUNCTION__."(".__LINE__."): checking if the table ldap_globalconfig exists already");
		$sqlquery='SHOW TABLES LIKE "' . TABLE_PREFIX . 'ldap_globalconfig";';
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			$this->ldapLog(__FUNCTION__."(".__LINE__."): ldap_globalconfig exists already, do nothing");
			//do nothing for now
		}
		else
		{
			$this->ldapLog(__FUNCTION__."(".__LINE__."): checking if ldap_auth_var still resides in ldap_config");
			$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_id, ' . TABLE_PREFIX . 'ldap_config.ldap_auth_var FROM ' . TABLE_PREFIX . 'ldap_config;';
			if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
			{
				$this->ldapLog(__FUNCTION__."(".__LINE__."): fetching all entries of ldap_auth_var and writing them back temporarily");
				while($rowset = db_fetch_array($tmp_res)) {
					$rows[$rownum]['id']=$rowset['ldap_id'];
					$rows[$rownum]['auth_var']=$rowset['ldap_auth_var'];
					if($saved_auth==""&&$rowset['ldap_auth_var']!=""&&strlen($rowset['ldap_auth_var'])>0)
					{
						$saved_auth=$rowset['ldap_auth_var'];
					}
					$rownum++;
				}
				for($i=0;$i<$rownum;$i++)
				{
					if($rows[$i]['auth_var']=="")
					{
						$sqlquery='UPDATE ' . TABLE_PREFIX . 'ldap_config SET ' . TABLE_PREFIX . 'ldap_config.ldap_auth_var = "'.$saved_auth.'" WHERE ' . TABLE_PREFIX . 'ldap_config.ldap_id = ' . $rows[$i]['id'] . ';';
						db_query($sqlquery);
					}
				}
			}
			$this->ldapLog(__FUNCTION__."(".__LINE__."): checking ldapdbversion");
			if($this->getLdapDBVersion()<1)
			{
				$this->ldapLog(__FUNCTION__."(".__LINE__."): creating the table ldap_globalconfig if it doesn't exist yet");
				$sqlquery="CREATE TABLE IF NOT EXISTS `". TABLE_PREFIX . "ldap_globalconfig` (
					`ldap_global_id` int(11) NOT NULL AUTO_INCREMENT,
					`dbversion` int(11) DEFAULT '1',
					`maxlogs` int(11) DEFAULT '2000',
					`ldap_client_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
					`ldap_client_autofill` tinyint(1) unsigned NOT NULL DEFAULT '0',
					`ldap_client_forcelogin` tinyint(1) unsigned NOT NULL DEFAULT '0',
					`ldap_client_force_ldaplogin` tinyint(1) unsigned NOT NULL DEFAULT '0',
					`ldap_client_overwrite_email` tinyint(1) unsigned NOT NULL DEFAULT '0',
					`ldap_debug` tinyint(1) unsigned NOT NULL DEFAULT '0',
					`ldap_use_sso` tinyint(1) unsigned NOT NULL DEFAULT '0',
					`ldap_auth_var` text COLLATE utf8_unicode_ci NOT NULL,
						PRIMARY KEY (`ldap_global_id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";
				db_query($sqlquery);
				
				$this->ldapLog(__FUNCTION__."(".__LINE__."): fetching settings, that have to be moved to the ldap_globalconfig table");
				
				$sqlquery='SELECT ldap_auth_var, MAX(ldap_client_active) as ldap_client_active, MAX(ldap_client_force_ldaplogin) as ldap_client_force_ldaplogin, MAX(ldap_client_autofill) as ldap_client_autofill, MAX(ldap_client_forcelogin) as ldap_client_forcelogin, MAX(ldap_use_sso) as ldap_use_sso FROM `'. TABLE_PREFIX . 'ldap_config` WHERE 1 GROUP BY ldap_auth_var';
				if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
				{
					$rowset = db_fetch_array($tmp_res);
					$ldap_client_active_tmp=$rowset['ldap_client_active'];
					$ldap_client_autofill_tmp=$rowset['ldap_client_autofill'];
					$ldap_client_forcelogin_tmp=$rowset['ldap_client_forcelogin'];
					$ldap_client_force_ldaplogin_tmp=$rowset['ldap_client_force_ldaplogin'];
					$ldap_use_sso_tmp=$rowset['ldap_use_sso'];
					
					$this->ldapLog(__FUNCTION__."(".__LINE__."): moving settings to the ldap_globalconfig table");
					$sqlquery="INSERT INTO ". TABLE_PREFIX . "ldap_globalconfig (ldap_client_active, ldap_client_autofill, ldap_client_forcelogin, ldap_client_force_ldaplogin, ldap_use_sso, ldap_auth_var) VALUES (".$rowset['ldap_client_active'].",".$rowset['ldap_client_autofill'].",".$rowset['ldap_client_forcelogin'].",".$rowset['ldap_client_force_ldaplogin'].",".$rowset['ldap_use_sso'].",'".$saved_auth."');";
					db_query($sqlquery);
				}
				$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_client_active"';
				if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
				{
					$sqlquery='alter table '. TABLE_PREFIX . 'ldap_config drop column `ldap_client_active`;';
					db_query($sqlquery);
					$this->ldapLog(__FUNCTION__."(".__LINE__."): dropped column ldap_client_active from table ldap_config");
				}
				$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_client_autofill"';
				if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
				{
					$sqlquery='alter table '. TABLE_PREFIX . 'ldap_config drop column `ldap_client_autofill`;';
					db_query($sqlquery);
					$this->ldapLog(__FUNCTION__."(".__LINE__."): dropped column ldap_client_autofill from table ldap_config");
				}
				$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_client_forcelogin"';
				if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
				{
					$sqlquery='alter table '. TABLE_PREFIX . 'ldap_config drop column `ldap_client_forcelogin`;';
					db_query($sqlquery);
					$this->ldapLog(__FUNCTION__."(".__LINE__."): dropped column ldap_client_forcelogin from table ldap_config");
				}
				$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_client_force_ldaplogin"';
				if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
				{
					$sqlquery='alter table '. TABLE_PREFIX . 'ldap_config drop column `ldap_client_force_ldaplogin`;';
					db_query($sqlquery);
					$this->ldapLog(__FUNCTION__."(".__LINE__."): dropped column ldap_client_force_ldaplogin from table ldap_config");
				}
				$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_use_sso"';
				if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
				{
					$sqlquery='alter table '. TABLE_PREFIX . 'ldap_config drop column `ldap_use_sso`;';
					db_query($sqlquery);
					$this->ldapLog(__FUNCTION__."(".__LINE__."): dropped column ldap_use_sso from table ldap_config");
				}
				$sqlquery='SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "'. TABLE_PREFIX . 'ldap_config" AND COLUMN_NAME = "ldap_auth_var"';
				if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
				{
					$sqlquery='alter table '. TABLE_PREFIX . 'ldap_config drop column `ldap_auth_var`;';
					db_query($sqlquery);
					$this->ldapLog(__FUNCTION__."(".__LINE__."): dropped column ldap_auth_var from table ldap_config");
				}
			}
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
	}
	
	function getPasswd($id)
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_id, ' . TABLE_PREFIX . 'ldap_config.ldap_admin_pw FROM ' . TABLE_PREFIX . 'ldap_config WHERE ldap_id='.$id.';';
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			$tmparray=db_fetch_array($tmp_res);
			if(strlen($tmparray['ldap_admin_pw'])>1)
			{
				$this->ldapLog(__FUNCTION__."(".__LINE__."): fetched password, done");
				return $tmparray['ldap_admin_pw']?Crypto::decrypt($tmparray['ldap_admin_pw'],SECRET_SALT):'';
			}
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
        return '';
	}

    function save($id,$vars,&$errors) {
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
        global $cfg;
		
        //very basic checks

        $vars['ldap_domain']=Format::striptags(trim($vars['ldap_domain']));
        $vars['ldap_admin']=Format::striptags(trim($vars['ldap_admin']));
        $vars['ldap_admin_cn']=Format::striptags(trim($vars['ldap_admin_cn']));
        $vars['ldap_controller']=Format::striptags(trim($vars['ldap_controller']));
        $vars['ldap_suffix']=Format::striptags(trim($vars['ldap_suffix']));
        $vars['ldap_filter']=Format::striptags(trim($vars['ldap_filter']));
        $vars['ldap_rdn']=Format::striptags(trim($vars['ldap_rdn']));
        $vars['ldap_admin_cn']=Format::striptags(trim($vars['ldap_admin_cn']));
        $vars['ldap_email_field']=Format::striptags(trim($vars['ldap_email_field']));
        $vars['ldap_firstname_field']=Format::striptags(trim($vars['ldap_firstname_field']));
        $vars['ldap_lastname_field']=Format::striptags(trim($vars['ldap_lastname_field']));
        $vars['ldap_user_field']=Format::striptags(trim($vars['ldap_user_field']));
        //$vars['ldap_auth_var']=Format::striptags(trim($vars['ldap_auth_var']));
        $vars['ldap_phone_field']=Format::striptags(trim($vars['ldap_phone_field']));
        $vars['ldap_ou_field']=Format::striptags(trim($vars['ldap_ou_field']));

        if(!$vars['ldap_domain'])
            $errors['ldap_domain']='Domain required';
			
        if(!$vars['ldap_controller'])
            $errors['ldap_controller']='Controller required';
			
        if(!$vars['ldap_port'])
            $errors['ldap_port']='Port required';
			
        if(!$vars['ldap_suffix'])
            $errors['ldap_suffix']='Suffix required';
			
        if(!$vars['ldap_filter'])
            $errors['ldap_filter']='Filter required';
			
        if(!$vars['ldap_email_field'])
            $errors['ldap_email_field']='Email Field required';
			
        if(!$vars['ldap_firstname_field'])
            $errors['ldap_firstname_field']='First Name Field required';
			
        if(!$vars['ldap_lastname_field'])
            $errors['ldap_lastname_field']='Last Name Field required';
			
        if(!$vars['ldap_user_field'])
            $errors['ldap_user_field']='User Field required';
			
        if(!$vars['ldap_phone_field'])
            $errors['ldap_phone_field']='Phone Field required';
		
		if(!$vars['ldap_ou_field'])
            $errors['ldap_ou_field']='Organizational Unit required';
		
		if($vars['ldap_use_rdn'])
		{
			if(!$vars['ldap_rdn'])
			{
				$errors['ldap_rdn']='RDN required';
			}
			if(!$vars['ldap_admin_cn'])
			{
				$errors['ldap_admin_cn']='CN required';
			}
		}
		
        if(!$vars['priority'])
		{
            $errors['priority']='Priority required';
		}
		else if($vars['priority']<1||$vars['priority']>99)
		{
            $errors['priority']='Invalid Priority';
		}

        if($vars['ldap_active']) {
            if(!$vars['ldap_admin'])
                $errors['ldap_admin']='Admin missing';
                
            if(!$vars['ldap_admin_pw'] && (!$vars['dpasswd'] || $vars['dpasswd']==''))
                $errors['ldap_admin_pw']='Password required';
        }

        //abort on errors
        if($errors) return false;
        
        if(!$errors) {
            $sql='SELECT ldap_id FROM ' . TABLE_PREFIX . 'ldap_config'
                .' WHERE ldap_domain='.db_input($vars['ldap_domain']).' AND ldap_controller='.db_input($vars['ldap_controller']);
            if($id)
                $sql.=' AND ldap_id!='.db_input($id);
                
            if(db_num_rows(db_query($sql)))
                $errors['ldap_domain']=$errors['ldap_controller']='Domain/Controller combination already in-use.';
        }
        
        $passwd=$vars['ldap_admin_pw']?$vars['ldap_admin_pw']:$vars['dpasswd'];
        if(!$errors && $vars['ldap_active']) {
			$ldap = ldap_connect($vars['ldap_controller'], $vars['ldap_port']);
			ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
			ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
            if(!$ldap) {
                $errors['err']=sprintf("Couldn't connect to %s with current settings.",$vars['ldap_controller']);
                $errors['ldap_controller']=$errors['err'];
			}
        }
		
        if($errors) return false;
       
        $sql='ldap_domain='.db_input($vars['ldap_domain']).
             ',ldap_admin='.db_input(Format::striptags($vars['ldap_admin'])).
             ',ldap_admin_cn='.db_input(Format::striptags($vars['ldap_admin_cn'])).
             ',ldap_controller='.db_input($vars['ldap_controller']).
             ',ldap_suffix='.db_input($vars['ldap_suffix']).
             ',ldap_filter='.db_input($vars['ldap_filter']).
             ',ldap_rdn='.db_input($vars['ldap_rdn']).
             ',ldap_use_rdn='.db_input($vars['ldap_use_rdn']).
             ',ldap_active='.db_input($vars['ldap_active']).
             ',priority='.db_input($vars['priority']).
             ',ldap_ou_field='.db_input($vars['ldap_ou_field']).
             ',ldap_email_field='.db_input($vars['ldap_email_field']).
             ',ldap_firstname_field='.db_input($vars['ldap_firstname_field']).
             ',ldap_lastname_field='.db_input($vars['ldap_lastname_field']).
             ',ldap_user_field='.db_input($vars['ldap_user_field']).
             ',ldap_phone_field='.db_input($vars['ldap_phone_field']).
             ',ldap_port='.db_input($vars['ldap_port']).
             ',ldap_ext_length='.db_input($vars['ldap_ext_length']).
             ',ldap_ssl='.db_input($vars['ldap_ssl']);
        
        if($vars['ldap_admin_pw']) //New password - encrypt.
            $sql.=',ldap_admin_pw='.db_input(Crypto::encrypt($vars['ldap_admin_pw'],SECRET_SALT));
        
        if($id) { //update
            $sql='UPDATE '.TABLE_PREFIX . 'ldap_config SET '.$sql.' WHERE ldap_id='.db_input($id);
            if(db_query($sql))
			{
				$this->ldapLog(__FUNCTION__."(".__LINE__."): updated the entry, done");
                return true;
			}
                
            $errors['err']='Unable to update ldap connection. Internal error occurred';
        }else {
            $sql='INSERT INTO '.TABLE_PREFIX . 'ldap_config SET '.$sql;
            if(db_query($sql) && ($id=db_insert_id()))
			{
				$this->ldapLog(__FUNCTION__."(".__LINE__."): created the entry, done");
                return $id;
			}

            $errors['err']='Unable to add ldap connection. Internal error';
        }
        
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
        return false;
    }
	
	function saveGlobal($vars,&$errors)
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
        global $cfg;
		$vars['ldap_auth_var']=Format::striptags(trim($vars['ldap_auth_var']));
		
		if($vars['ldap_use_sso'])
		{
			if(!$vars['ldap_auth_var'])
				$errors['ldap_auth_var']='Auth Variable required';
		}
		
		if($vars['ldap_client_autofill']&&!$vars['ldap_client_active'])
		{
			$errors['ldap_client_autofill']='Client Authentication via LDAP must be active';
		}
		if($vars['ldap_client_force_ldaplogin']&&!$vars['ldap_client_forcelogin'])
		{
			$errors['ldap_client_force_ldaplogin']='Force Clients to Log In must be active';
		}
		
		if($vars['ldap_client_active'])
		{
			$sqlquery='SELECT `key`, value FROM ' . TABLE_PREFIX . 'config WHERE `key` = "show_related_tickets";';
			if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
			{
				$tmparray=db_fetch_array($tmp_res);
				if(!$tmparray['value'])
				{
					$errors['ldap_client_active']='The <b>Show Related Tickets</b> setting must be active.';
				}
			}
		}
		
		$rows=array();
		$rownum=0;
		$updatesuc=false;
		
		$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_globalconfig.ldap_global_id FROM ' . TABLE_PREFIX . 'ldap_globalconfig;';
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			$tmparray=db_fetch_array($tmp_res);
			$sql='ldap_auth_var='.db_input($vars['ldap_auth_var']).
				 ',ldap_client_active='.db_input($vars['ldap_client_active']).
				 ',ldap_client_autofill='.db_input($vars['ldap_client_autofill']).
				 ',ldap_client_forcelogin='.db_input($vars['ldap_client_forcelogin']).
				 ',ldap_client_force_ldaplogin='.db_input($vars['ldap_client_force_ldaplogin']).
				 ',ldap_client_overwrite_email='.db_input($vars['ldap_client_overwrite_email']).
				 ',ldap_debug='.db_input($vars['ldap_debug']).
				 ',maxlogs='.db_input($vars['maxlogs']).
				 ',ldap_use_sso='.db_input($vars['ldap_use_sso']);
			$sql='UPDATE '.TABLE_PREFIX . 'ldap_globalconfig SET '.$sql.' WHERE ldap_global_id='.$tmparray['ldap_global_id'];
			if(db_query($sql)&&$updatesuc==false)
			{
				$updatesuc=true;
			}
		}
		else
		{
			$errors['err']='You have to create a LDAP Connection first!';
		}
		
		if($updatesuc==false&&$errors['err']=="")
		{
			$errors['err']='Unable to update ldap settings. Internal error occurred';
		}
		
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
        if($errors) return false;
		
		return $updatesuc;
                
	}

    function create($vars,&$errors) {
        return $this->save(0,$vars,$errors);
    }

    function delete($id) {
		if($this->checkID($id))
		{
			$sql='DELETE FROM '.TABLE_PREFIX . 'ldap_config WHERE ldap_id='.db_input($id).' LIMIT 1';
			if(db_query($sql) && ($num=db_affected_rows())) {
				return $num;
			}
		}
        return 0;
    }
	
	function update($id,$vars,&$errors)
	{
		$vars=$vars;
        $vars['dpasswd']=$this->getPasswd($id); //Current decrypted password.

        if(!$this->save($id, $vars, $errors))
            return false;
			
		return true;
	}
	
	function updateGlobal($vars,&$errors)
	{
		$vars=$vars;

        if(!$this->saveGlobal($vars, $errors))
            return false;
			
		return true;
	}
	
	function checkID($id)
	{
		$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_id FROM ' . TABLE_PREFIX . 'ldap_config WHERE ldap_id='.$id.';';
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			return true;
		}
		return false;
	}
	
	function ldapGetUsernameFromEmail($email,&$outp=NULL,$debug=false)
	{
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_id, ' . TABLE_PREFIX . 'ldap_config.ldap_suffix, ' . TABLE_PREFIX . 'ldap_config.ldap_rdn, ' . TABLE_PREFIX . 'ldap_config.ldap_admin_cn, ' . TABLE_PREFIX . 'ldap_config.ldap_admin, ' . TABLE_PREFIX . 'ldap_config.ldap_domain, ' . TABLE_PREFIX . 'ldap_config.ldap_email_field, ' . TABLE_PREFIX . 'ldap_config.ldap_user_field from ' . TABLE_PREFIX . 'ldap_config ORDER BY ' . TABLE_PREFIX . 'ldap_config.priority;';
		$user="";
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			while($rowset = db_fetch_array($tmp_res)) {
				$this->ldapLog(__FUNCTION__."(".__LINE__."): attempting to connect to ".$rowset['ldap_domain']);
				$ldap = $this->ldapSqlConnect($rowset['ldap_id']);
				if ($ldap) {
					$this->ldapLog(__FUNCTION__."(".__LINE__."): connection established");
					if($debug==true)
					{
						$outp.='<br><br>Debug of function ldapGetUsernameFromEmail(): <br><br>';
						$outp.='getting the user of email: "'.$email.'"<br>';
					}
					$ldapusr="";
					if(!$this->useRDN($rowset['ldap_id']))
					{
						$ldapusr=$rowset['ldap_admin'] . $rowset['ldap_suffix'];
					}
					else
					{
						$ldapusr=str_replace('%UID%',$rowset['ldap_admin'],$rowset['ldap_rdn']);
						$ldapusr=str_replace('%CN%',$rowset['ldap_admin_cn'],$ldapusr);
						if($debug==true)
						{
							$outp.='using rdn for binding<br>';
						}
					}
					if($debug==true)
					{
						$outp.='binding to ldap with "'.$ldapusr.'" and his password<br>';
					}
					$this->ldapLog(__FUNCTION__."(".__LINE__."): binding to ldap with '".$ldapusr."' and his password");
					$bind = ldap_bind($ldap, $ldapusr, $this->getPasswd($rowset['ldap_id']));
					if($bind)
					{
						$this->ldapLog(__FUNCTION__."(".__LINE__."): binding successful");
						$LDAPFieldsToFind = array($rowset['ldap_user_field']);
						if($debug==true)
						{
							$outp.='calling ldap_search with the domain: "'.$rowset['ldap_domain'].'", the Filter: "(&('.$rowset['ldap_email_field'].'='.$email.'))" and the Attributes: "array("'.$rowset['ldap_user_field'].'")"<br>';
						}
						$this->ldapLog(__FUNCTION__."(".__LINE__."): ".'calling ldap_search with the domain: "'.$rowset['ldap_domain'].'", the Filter: "(&('.$rowset['ldap_email_field'].'='.$email.'))" and the Attributes: "array("'.$rowset['ldap_user_field'].'")"');
						$results = ldap_search($ldap, $rowset['ldap_domain'], "(&(".$rowset['ldap_email_field']."=".$email."))", $LDAPFieldsToFind);
						$info = ldap_get_entries($ldap, $results);
						$row=0;

						if($info["count"] > 0){
							$user=$info[0][$rowset['ldap_user_field']][0];
							if($debug==true)
							{
								$outp.='LDAP returned field data: "'.$user.'"<br>';
							}
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP 'ldap_user_field' contains '".$user."'");
							if(trim($user)!="")
							{
								break;
							}
						}
						else
						{
							if($debug==true)
							{
								$outp.='LDAP returned nothing...<br>';
							}
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP returned nothing..");
						}
						
						ldap_unbind($ldap);
					}
					else
					{
						if($debug==true)
						{
							$outp.=ldap_error($ldap).'<br>';
							$outp.='errno: '.strval(ldap_errno($ldap)).'<br>';
						}
						$this->ldapLog(__FUNCTION__."(".__LINE__."): binding failed");
						$this->ldapLog(__FUNCTION__."(".__LINE__."): ".ldap_error($ldap));
						$this->ldapLog(__FUNCTION__."(".__LINE__."): errno: ".strval(ldap_errno($ldap)));
						echo 'Cannot authenticate with LDAP server.';
					}
				}else{
					$this->ldapLog(__FUNCTION__."(".__LINE__."): Cannot connect to LDAP server.");
					echo 'Cannot connect to LDAP server.';
				}
			}
		}
		else
		{
			$this->ldapLog(__FUNCTION__."(".__LINE__."): no ldap config");
			echo 'no ldap config';
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
		return $user;
	}
	
	function ldapGetEmail($username,&$outp=NULL,$debug=false)
	{
		$getSlash = strpos($username,"\\");
		if($getSlash){
			$indeksAfterSlash = $getSlash + 1;
			$lastIndeks = strlen($username);
			$username = substr($username,$indeksAfterSlash,$lastIndeks);
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_id, ' . TABLE_PREFIX . 'ldap_config.ldap_suffix, ' . TABLE_PREFIX . 'ldap_config.ldap_rdn, ' . TABLE_PREFIX . 'ldap_config.ldap_admin_cn, ' . TABLE_PREFIX . 'ldap_config.ldap_filter, ' . TABLE_PREFIX . 'ldap_config.ldap_admin, ' . TABLE_PREFIX . 'ldap_config.ldap_domain, ' . TABLE_PREFIX . 'ldap_config.ldap_email_field from ' . TABLE_PREFIX . 'ldap_config ORDER BY ' . TABLE_PREFIX . 'ldap_config.priority;';
		$email="";
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			while($rowset = db_fetch_array($tmp_res)) {
				$this->ldapLog(__FUNCTION__."(".__LINE__."): attempting to connect to ".$rowset['ldap_domain']);
				$ldap = $this->ldapSqlConnect($rowset['ldap_id']);
				if ($ldap) {
					$this->ldapLog(__FUNCTION__."(".__LINE__."): connection established");
					if($debug==true)
					{
						$outp.='<br><br>Debug of function ldapGetEmail(): <br><br>';
						$outp.='getting the email of user: "'.$username.'"<br>';
					}
					$ldapusr="";
					if(!$this->useRDN($rowset['ldap_id']))
					{
						$ldapusr=$rowset['ldap_admin'] . $rowset['ldap_suffix'];
					}
					else
					{
						$ldapusr=str_replace('%UID%',$rowset['ldap_admin'],$rowset['ldap_rdn']);
						$ldapusr=str_replace('%CN%',$rowset['ldap_admin_cn'],$ldapusr);
						if($debug==true)
						{
							$outp.='using rdn for binding<br>';
						}
					}
					if($debug==true)
					{
						$outp.='binding to ldap with "'.$ldapusr.'" and his password<br>';
					}
					$this->ldapLog(__FUNCTION__."(".__LINE__."): binding to ldap with '".$ldapusr."' and his password");
					$bind = ldap_bind($ldap, $ldapusr, $this->getPasswd($rowset['ldap_id']));
					if($bind)
					{
						$this->ldapLog(__FUNCTION__."(".__LINE__."): binding successful");
						$ldapFilter="";
						if($rowset['ldap_filter']==""||$rowset['ldap_filter']==NULL)
						{
							$ldapFilter='(&(sAMAccountName='.$username.'))';
						}
						else
						{
							$ldapFilter=str_replace('%USERNAME%',$username,$rowset['ldap_filter']);
						}
						$LDAPFieldsToFind = array($rowset['ldap_email_field']);
						if($debug==true)
						{
							$outp.='calling ldap_search with the domain: "'.$rowset['ldap_domain'].'", the Filter: "'.$ldapFilter.'" and the Attributes: "array("'.$rowset['ldap_email_field'].'")"<br>';
						}
						
						$this->ldapLog(__FUNCTION__."(".__LINE__."): ".'calling ldap_search with the domain: "'.$rowset['ldap_domain'].'", the Filter: "'.$ldapFilter.'" and the Attributes: "array("'.$rowset['ldap_email_field'].'")"');
						$results = ldap_search($ldap, $rowset['ldap_domain'], $ldapFilter, $LDAPFieldsToFind);
						$info = ldap_get_entries($ldap, $results);
						$row=0;

						if($info["count"] > 0){
							$email=$info[0][$rowset['ldap_email_field']][0];
							if($debug==true)
							{
								$outp.='LDAP returned field data: "'.$email.'"<br>';
							}
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP 'ldap_email_field' contains '".$email."'");
							if(trim($email)!="")
							{
								break;
							}
						}
						else
						{
							if($debug==true)
							{
								$outp.='LDAP returned nothing...<br>';
							}
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP returned nothing..");
						}
						ldap_unbind($ldap);
					}
					else
					{
						if($debug==true)
						{
							$outp.=ldap_error($ldap).'<br>';
							$outp.='errno: '.strval(ldap_errno($ldap)).'<br>';
						}
						$this->ldapLog(__FUNCTION__."(".__LINE__."): binding failed");
						$this->ldapLog(__FUNCTION__."(".__LINE__."): ".ldap_error($ldap));
						$this->ldapLog(__FUNCTION__."(".__LINE__."): errno: ".strval(ldap_errno($ldap)));
						echo 'Cannot authenticate with LDAP server.';
					}
				}else{
					$this->ldapLog(__FUNCTION__."(".__LINE__."): Cannot connect to LDAP server.");
					echo 'Cannot connect to LDAP server.';
				}
			}
		}
		else
		{
			$this->ldapLog(__FUNCTION__."(".__LINE__."): no ldap config");
			echo 'no ldap config';
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
		return $email;
	}
	
	function ldapGetPhone($username)
	{
		$getSlash = strpos($username,"\\");
		if($getSlash){
			$indeksAfterSlash = $getSlash + 1;
			$lastIndeks = strlen($username);
			$username = substr($username,$indeksAfterSlash,$lastIndeks);
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_id, ' . TABLE_PREFIX . 'ldap_config.ldap_suffix, ' . TABLE_PREFIX . 'ldap_config.ldap_rdn, ' . TABLE_PREFIX . 'ldap_config.ldap_admin_cn, ' . TABLE_PREFIX . 'ldap_config.ldap_filter, ' . TABLE_PREFIX . 'ldap_config.ldap_admin, ' . TABLE_PREFIX . 'ldap_config.ldap_domain, ' . TABLE_PREFIX . 'ldap_config.ldap_phone_field, ' . TABLE_PREFIX . 'ldap_config.ldap_ext_length from ' . TABLE_PREFIX . 'ldap_config ORDER BY ' . TABLE_PREFIX . 'ldap_config.priority;';
		$phone="";
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			while($rowset = db_fetch_array($tmp_res)) {
				$this->ldapLog(__FUNCTION__."(".__LINE__."): attempting to connect to ".$rowset['ldap_domain']);
				$ldap = $this->ldapSqlConnect($rowset['ldap_id']);
				if ($ldap) {
					$this->ldapLog(__FUNCTION__."(".__LINE__."): connection established");
					$ldapusr="";
					if(!$this->useRDN($rowset['ldap_id']))
					{
						$ldapusr=$rowset['ldap_admin'] . $rowset['ldap_suffix'];
					}
					else
					{
						$ldapusr=str_replace('%UID%',$rowset['ldap_admin'],$rowset['ldap_rdn']);
						$ldapusr=str_replace('%CN%',$rowset['ldap_admin_cn'],$ldapusr);
					}
					$this->ldapLog(__FUNCTION__."(".__LINE__."): binding to ldap with '".$ldapusr."' and his password");
					$bind = ldap_bind($ldap, $ldapusr, $this->getPasswd($rowset['ldap_id']));
					if($bind)
					{
						$this->ldapLog(__FUNCTION__."(".__LINE__."): binding successful");
						$ldapFilter="";
						if($rowset['ldap_filter']==""||$rowset['ldap_filter']==NULL)
						{
							$ldapFilter='(&(sAMAccountName='.$username.'))';
						}
						else
						{
							$ldapFilter=str_replace('%USERNAME%',$username,$rowset['ldap_filter']);
						}
						$LDAPFieldsToFind = array($rowset['ldap_phone_field']);
						$this->ldapLog(__FUNCTION__."(".__LINE__."): ".'calling ldap_search with the domain: "'.$rowset['ldap_domain'].'", the Filter: "'.$ldapFilter.'" and the Attributes: "array("'.$rowset['ldap_phone_field'].'")"');
						$results = ldap_search($ldap, $rowset['ldap_domain'], $ldapFilter, $LDAPFieldsToFind);
						$info = ldap_get_entries($ldap, $results);
						$row=0;

						if($info["count"] > 0){
							$phone=$info[0][$rowset['ldap_phone_field']][0];
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP 'ldap_phone_field' contains '".$phone."'");
							if($rowset['ldap_ext_length']>0)
							{
								$this->ldapLog(__FUNCTION__."(".__LINE__."): need to remove phone extension");
								$phone=substr($phone,0,$rowset['ldap_ext_length']*(-1));
								$phone=trim($phone);
								if($phone!="")
								{
									$this->ldapLog(__FUNCTION__."(".__LINE__."): returning phone value: ". $phone);
									break;
								}
							}
						}
						else
						{
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP returned nothing..");
						}
						ldap_unbind($ldap);
					}
					else
					{
						$this->ldapLog(__FUNCTION__."(".__LINE__."): binding failed");
						$this->ldapLog(__FUNCTION__."(".__LINE__."): ".ldap_error($ldap));
						$this->ldapLog(__FUNCTION__."(".__LINE__."): errno: ".strval(ldap_errno($ldap)));
						echo 'Cannot authenticate with LDAP server.';
					}
				}else{
					$this->ldapLog(__FUNCTION__."(".__LINE__."): Cannot connect to LDAP server.");
					echo 'Cannot connect to LDAP server.';
				}
			}
		}
		else
		{
			$this->ldapLog(__FUNCTION__."(".__LINE__."): no ldap config");
			echo 'no ldap config';
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
		return $phone;
	}
	
	function ldapGetPhoneExt($username)
	{
		$getSlash = strpos($username,"\\");
		if($getSlash){
			$indeksAfterSlash = $getSlash + 1;
			$lastIndeks = strlen($username);
			$username = substr($username,$indeksAfterSlash,$lastIndeks);
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_id, ' . TABLE_PREFIX . 'ldap_config.ldap_suffix, ' . TABLE_PREFIX . 'ldap_config.ldap_rdn, ' . TABLE_PREFIX . 'ldap_config.ldap_admin_cn, ' . TABLE_PREFIX . 'ldap_config.ldap_filter, ' . TABLE_PREFIX . 'ldap_config.ldap_admin, ' . TABLE_PREFIX . 'ldap_config.ldap_domain, ' . TABLE_PREFIX . 'ldap_config.ldap_phone_field, ' . TABLE_PREFIX . 'ldap_config.ldap_ext_length from ' . TABLE_PREFIX . 'ldap_config ORDER BY ' . TABLE_PREFIX . 'ldap_config.priority;';
		$phone="";
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			while($rowset = db_fetch_array($tmp_res)) {
				$this->ldapLog(__FUNCTION__."(".__LINE__."): attempting to connect to ".$rowset['ldap_domain']);
				$ldap = $this->ldapSqlConnect($rowset['ldap_id']);
				if ($ldap) {
					$this->ldapLog(__FUNCTION__."(".__LINE__."): connection established");
					$ldapusr="";
					if(!$this->useRDN($rowset['ldap_id']))
					{
						$ldapusr=$rowset['ldap_admin'] . $rowset['ldap_suffix'];
					}
					else
					{
						$ldapusr=str_replace('%UID%',$rowset['ldap_admin'],$rowset['ldap_rdn']);
						$ldapusr=str_replace('%CN%',$rowset['ldap_admin_cn'],$ldapusr);
					}
					$this->ldapLog(__FUNCTION__."(".__LINE__."): binding to ldap with '".$ldapusr."' and his password");
					$bind = ldap_bind($ldap, $ldapusr, $this->getPasswd($rowset['ldap_id']));
					if($bind)
					{
						$this->ldapLog(__FUNCTION__."(".__LINE__."): binding successful");
						$ldapFilter="";
						if($rowset['ldap_filter']==""||$rowset['ldap_filter']==NULL)
						{
							$ldapFilter='(&(sAMAccountName='.$username.'))';
						}
						else
						{
							$ldapFilter=str_replace('%USERNAME%',$username,$rowset['ldap_filter']);
						}
						$LDAPFieldsToFind = array($rowset['ldap_phone_field']);
						$this->ldapLog(__FUNCTION__."(".__LINE__."): ".'calling ldap_search with the domain: "'.$rowset['ldap_domain'].'", the Filter: "'.$ldapFilter.'" and the Attributes: "array("'.$rowset['ldap_phone_field'].'")"');
						$results = ldap_search($ldap, $rowset['ldap_domain'], $ldapFilter, $LDAPFieldsToFind);
						$info = ldap_get_entries($ldap, $results);
						$row=0;

						if($info["count"] > 0){
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP 'ldap_phone_field' contains '".$phone."'");
							if($rowset['ldap_ext_length']>0)
							{ 
								$phone=$info[0][$rowset['ldap_phone_field']][0];
								$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP 'ldap_phone_field' contains '".$phone."'");
								$extlen=$rowset['ldap_ext_length'];
								$this->ldapLog(__FUNCTION__."(".__LINE__."): phone extension length: ".$extlen);
								$phone=substr($phone,$extlen*(-1));
								$phone=trim($phone);
								if($phone!="")
								{
									$this->ldapLog(__FUNCTION__."(".__LINE__."): returning extension value: ". $phone);
									break;
								}
							}
						}
						else
						{
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP returned nothing..");
						}
						ldap_unbind($ldap);
					}
					else
					{
						$this->ldapLog(__FUNCTION__."(".__LINE__."): binding failed");
						$this->ldapLog(__FUNCTION__."(".__LINE__."): ".ldap_error($ldap));
						$this->ldapLog(__FUNCTION__."(".__LINE__."): errno: ".strval(ldap_errno($ldap)));
						echo 'Cannot authenticate with LDAP server.';
					}
				}else{
					$this->ldapLog(__FUNCTION__."(".__LINE__."): Cannot connect to LDAP server.");
					echo 'Cannot connect to LDAP server.';
				}
			}
		}
		else
		{
			$this->ldapLog(__FUNCTION__."(".__LINE__."): no ldap config");
			echo 'no ldap config';
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
		return $phone;
	}
	
	function ldapMaxLogCount()
	{
		if($this->global_settings!=null)
		{
			return $this->global_settings["maxlogs"];
		}
		return 2000;
	}
	
	function ldapLogCount()
	{
		$sqlquery='SHOW TABLES LIKE "' . TABLE_PREFIX . 'ldaplogs";';
		$tmp_res=db_query($sqlquery);
		if(db_num_rows($tmp_res)>0)
		{
			$sqlquery='SELECT COUNT(*) FROM ' . TABLE_PREFIX . 'ldaplogs';
			$tmp_res=db_query($sqlquery);
			$rowset = db_fetch_array($tmp_res);
			return $rowset["COUNT(*)"];
		}
		return 0;
	}
	
	function ldapLog($logtext)
	{
		if($this->ldapDebug())
		{
			$sqlquery='SHOW TABLES LIKE "' . TABLE_PREFIX . 'ldaplogs";';
			$tmp_res=db_query($sqlquery);
			if($tmp_res && db_num_rows($tmp_res)>0)
			{
			}
			else
			{
				$sqlquery="CREATE TABLE IF NOT EXISTS `". TABLE_PREFIX . "ldaplogs` (
					  `log_id` int(11) NOT NULL AUTO_INCREMENT,
					  `data` text COLLATE utf8_unicode_ci NOT NULL,
					  `created` DATETIME(6) COLLATE utf8_unicode_ci NOT NULL,
						PRIMARY KEY (`log_id`)
					) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
					db_query($sqlquery);
			}
			$sqlquery="INSERT INTO " . TABLE_PREFIX . "ldaplogs (data, created) VALUES (".db_input($logtext).",NOW(6));";
			db_query($sqlquery);
			while($this->ldapLogCount()>$this->ldapMaxLogCount())
			{
				$sqlquery="SELECT * FROM " . TABLE_PREFIX . "ldaplogs ORDER BY created ASC LIMIT 0, 1";
				$tmp_res=db_query($sqlquery);
				$rowset = db_fetch_array($tmp_res);
				$sqlquery="DELETE FROM " . TABLE_PREFIX . "ldaplogs WHERE log_id=".$rowset['log_id'];
				db_query($sqlquery);
			}
		}
	}
	
	function ldapGetName($username)
	{
		$getSlash = strpos($username,"\\");
		if($getSlash){
			$indeksAfterSlash = $getSlash + 1;
			$lastIndeks = strlen($username);
			$username = substr($username,$indeksAfterSlash,$lastIndeks);
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): starting ".__FUNCTION__);
		$sqlquery='SELECT ' . TABLE_PREFIX . 'ldap_config.ldap_id, ' . TABLE_PREFIX . 'ldap_config.ldap_suffix, ' . TABLE_PREFIX . 'ldap_config.ldap_rdn, ' . TABLE_PREFIX . 'ldap_config.ldap_admin_cn, ' . TABLE_PREFIX . 'ldap_config.ldap_filter, ' . TABLE_PREFIX . 'ldap_config.ldap_admin, ' . TABLE_PREFIX . 'ldap_config.ldap_domain, ' . TABLE_PREFIX . 'ldap_config.ldap_firstname_field, ' . TABLE_PREFIX . 'ldap_config.ldap_lastname_field from ' . TABLE_PREFIX . 'ldap_config ORDER BY ' . TABLE_PREFIX . 'ldap_config.priority;';
		$name="";
		if(($tmp_res=db_query($sqlquery)) && db_num_rows($tmp_res)>0)
		{
			while($rowset = db_fetch_array($tmp_res)) {
				$this->ldapLog(__FUNCTION__."(".__LINE__."): attempting to connect to ".$rowset['ldap_domain']);
				$ldap = $this->ldapSqlConnect($rowset['ldap_id']);
				if ($ldap) {
					$this->ldapLog(__FUNCTION__."(".__LINE__."): connection established");
					$ldapusr="";
					if(!$this->useRDN($rowset['ldap_id']))
					{
						$ldapusr=$rowset['ldap_admin'] . $rowset['ldap_suffix'];
					}
					else
					{
						$ldapusr=str_replace('%UID%',$rowset['ldap_admin'],$rowset['ldap_rdn']);
						$ldapusr=str_replace('%CN%',$rowset['ldap_admin_cn'],$ldapusr);
					}
					$this->ldapLog(__FUNCTION__."(".__LINE__."): binding to ldap with '".$ldapusr."' and his password");
					$bind = ldap_bind($ldap, $ldapusr, $this->getPasswd($rowset['ldap_id']));
					if($bind)
					{
						$this->ldapLog(__FUNCTION__."(".__LINE__."): binding successful");
						$ldapFilter="";
						if($rowset['ldap_filter']==""||$rowset['ldap_filter']==NULL)
						{
							$ldapFilter='(&(sAMAccountName='.$username.'))';
						}
						else
						{
							$ldapFilter=str_replace('%USERNAME%',$username,$rowset['ldap_filter']);
						}
						$LDAPFieldsToFind = array($rowset['ldap_firstname_field'],$rowset['ldap_lastname_field']);
						$this->ldapLog(__FUNCTION__."(".__LINE__."): ".'calling ldap_search with the domain: "'.$rowset['ldap_domain'].'", the Filter: "'.$ldapFilter.'" and the Attributes: "array("'.$rowset['ldap_firstname_field'].','.$rowset['ldap_lastname_field'].'")"');
						$results = ldap_search($ldap, $rowset['ldap_domain'], $ldapFilter, $LDAPFieldsToFind);
						$info = ldap_get_entries($ldap, $results);
						$row=0;

						if($info["count"] > 0){
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP 'ldap_firstname_field' contains '".$info[0][$rowset['ldap_firstname_field']][0]."'");
							$name.=$info[0][$rowset['ldap_firstname_field']][0];
							$name.=' ';
							$name.=$info[0][$rowset['ldap_lastname_field']][0];
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP 'ldap_lastname_field' contains '".$info[0][$rowset['ldap_lastname_field']][0]."'");
							if(trim($rowset['ldap_firstname_field'])!=""||trim($rowset['ldap_lastname_field'])!="")
							{
								$this->ldapLog(__FUNCTION__."(".__LINE__."): returning name value: ".$name."'");
								break;
							}
						}
						else
						{
							$this->ldapLog(__FUNCTION__."(".__LINE__."): LDAP returned nothing..");
						}
						ldap_unbind($ldap);
					}
					else
					{
						$this->ldapLog(__FUNCTION__."(".__LINE__."): binding failed");
						$this->ldapLog(__FUNCTION__."(".__LINE__."): ".ldap_error($ldap));
						$this->ldapLog(__FUNCTION__."(".__LINE__."): errno: ".strval(ldap_errno($ldap)));
						echo 'Cannot authenticate with LDAP server.';
					}
				}else{
					$this->ldapLog(__FUNCTION__."(".__LINE__."): Cannot connect to LDAP server.");
					echo 'Cannot connect to LDAP server.';
				}
			}
		}
		else
		{
			$this->ldapLog(__FUNCTION__."(".__LINE__."): no ldap config");
			echo 'no ldap config';
		}
		$this->ldapLog(__FUNCTION__."(".__LINE__."): done");
		return $name;
	}
}

global $ldapcon;
$ldapcon=null;

?>
