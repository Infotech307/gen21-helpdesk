<?php
/*********************************************************************
    class.topic.php

    Help topic helper

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

class Location {
    var $id;

    var $div;

    var $parent;
    var $page;

    function Location($id) {
        $this->id=0;
        $this->load($id);
    }

    function load($id=0) {

        if(!$id && !($id=$this->getId()))
            return false;

        $sql='SELECT * '            
            .' FROM '.LOCATION_TABLE
            .' WHERE LocationID='.db_input($id);

        if(!($res=db_query($sql)) || !db_num_rows($res))
            return false;

        $this->dv = db_fetch_array($res);
        $this->id = $this->dv['LocationID'];
        $this->page = null;


        return true;
    }

    function reload() {
        return $this->load();
    }

    function asVar() {
        return $this->getName();
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->dv['LocationName'];
    }

    function getPageId() {
        return $this->dv['page_id'];
    }

    function getPage() {
        if(!$this->page && $this->getPageId())
            $this->page = Page::lookup($this->getPageId());

        return $this->page;
    }


    function isEnabled() {
         return ($this->dv['isactive']);
    }

    function isActive() {
        return $this->isEnabled();
    }


    function getHashtable() {
        return $this->dv;
    }

    function getInfo() {
        return $this->getHashtable();
    }

    function update($vars, &$errors) {

        if(!$this->save($this->getId(), $vars, $errors))
            return false;

        $this->reload();
        return true;
    }

    function delete() {

        $sql='DELETE FROM '.LOCATION_TABLE.' WHERE LocationID='.db_input($this->getId()).' LIMIT 1';
        if(db_query($sql) && ($num=db_affected_rows())) {

        }

        return $num;
    }
    /*** Static functions ***/
    function create($vars, &$errors) {
        return self::save(0, $vars, $errors);
    }

    function getLocations($publicOnly=false) {

        $locations=array();
        $sql='SELECT LocationID '
            .' FROM '.LOCATION_TABLE
            .' WHERE isactive=1';

        $sql.=' ORDER BY LocationName';
        if(($res=db_query($sql)) && db_num_rows($res))
            while(list($id, $name)=db_fetch_row($res))
                $locations[$id]=$name;

        return $locations;
    }

//    function getPublicDivisions() {
//        return self::getDivisions(true);
//    }

    function getIdByName($name, $pid=0) {

        $sql='SELECT LocationID FROM '.LOCATION_TABLE
            .' WHERE LocationName='.db_input($name);
        if(($res=db_query($sql)) && db_num_rows($res))
            list($id) = db_fetch_row($res);

        return $id;
    }

    function lookup($id) {
        return ($id && is_numeric($id) && ($t= new Location($id)) && $t->getId()==$id)?$t:null;
    }

    function save($id, $vars, &$errors) {

        $vars['LocationName']=Format::striptags(trim($vars['LocationName']));

//        if($id && $id!=$vars['id'])
//            $errors['err']='Internal error. Try again';
//
//        if(!$vars['DivisionName'])
//            $errors['division']='Division required';
//        elseif(strlen($vars['topic'])<5)
//            $errors['topic']='Topic is too short. 5 chars minimum';
        if(($tid=self::getIdByName($vars['LocationName'])) && $tid!=$id)
            $errors['location']='Location already exists';


        if($errors) return false;

        $sql=' updated=NOW() '
            .',LocationName='.db_input($vars['location'])           
            .',isactive='.db_input($vars['isactive']);

      
        if($id) {
            $sql='UPDATE '.LOCATION_TABLE.' SET '.$sql.' WHERE LocationID='.db_input($id);
            if(db_query($sql))
                return true;

            $errors['err']='Unable to update location. Data already exist';
        } else {
            $sql='INSERT INTO '.LOCATION_TABLE.' SET '.$sql.',created=NOW()';
            if(db_query($sql) && ($id=db_insert_id()))
                return $id;

            $errors['err']='Unable to create the location. Data already exist';
        }

        return false;
    }
}
?>
