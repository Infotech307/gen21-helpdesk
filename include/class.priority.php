<?php
/*********************************************************************
    class.priority.php

    Priority handle

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

class Priority {
    
    var $id;
    var $ht;

    function Priority($id){
        
        $this->id =0;
        $this->load($id);
    }

    function load($id){
        if(!$id && !($id=$this->getId()))
            return false;

       $sql='SELECT * FROM '.PRIORITY_TABLE.' WHERE priority_id='.db_input($id);
			
        if(!($res=db_query($sql)) || !db_num_rows($res))
            return false;

        $this->ht= db_fetch_array($res);
        $this->id= $this->ht['priority_id'];
		
        return true;
    }

    function getId() {
        return $this->id;
    }
	
	function getHashtable() {
        return $this->ht;
    }
	
	  function create($vars, &$errors) {
        return self::save(0, $vars, $errors);
    }
	
	function delete() {

        $id=$this->getId();
        $sql='DELETE FROM '.PRIORITY_TABLE.' WHERE priority_id='.db_input($id).'  LIMIT 1';
		db_query($sql);
   	 	return true;
    }
	
	 function getInfo() {
        return $this->getHashtable();
    }
	
    function getTag() {
        return $this->ht['priority'];
    }

    function getDesc() {
        return $this->ht['priority_desc'];
    }
	
	function getTime() {
        return $this->ht['priority_time'];
    }

    function getColor() {
        return $this->ht['priority_color'];
    }

    function getUrgency() {
        return $this->ht['priority_urgency'];
    }
	
	 function getSla() {
        return $this->ht['sla_id'];
    }

    function isPublic() {
        return ($this->ht['ispublic']);
    }

    /* ------------- Static ---------------*/
    function lookup($id) {
        return ($id && is_numeric($id) && ($p=new Priority($id)) && $p->getId()==$id)?$p:null;
    }

    function getPriorities( $publicOnly=false) {

        $priorities=array();
        $sql ='SELECT priority_id, priority_desc FROM '.PRIORITY_TABLE;
        if($publicOnly)
            $sql.=' WHERE ispublic=1';

        if(($res=db_query($sql)) && db_num_rows($res)) {
            while(list($id, $name)=db_fetch_row($res))
                $priorities[$id] = $name;
        }

        return $priorities;
    }

    function getPublicPriorities() {
        return self::getPriorities(true);
    }
	
	 function update($vars, &$errors) {

        if(!$this->save($this->getId(), $vars, $errors))
            return false;

        $this->reload();
        return true;
    }
	
	function reload() {
         return $this->load($this->getId());
    }
	
	    function save($id,$vars,&$errors) {


        if(!$vars['priority'])
            $errors['priority']='Priority required';
        
		if(!$vars['priority_desc'])
            $errors['priority_desc']='Priority Description required';
			
		if(!$vars['sla_id'])
            $errors['sla_id']='SLA required';
			
		if(!$vars['priority_color'])
            $errors['priority_color']='Priority Color required';
			
		if(!$vars['priority_urgency'])
            $errors['priority_urgency']='Priority Urgency required';
       	 elseif(!is_numeric($vars['priority_urgency']))
            $errors['priority_urgency']='Priority Urgency is numeric';

        if($errors) return false;

        $sql=' priority='.db_input($vars['priority']).
			 ',ispublic='.db_input($vars['ispublic']).
             ',priority_desc='.db_input($vars['priority_desc']).
             ',sla_id='.db_input($vars['sla_id']).
             ',priority_color='.db_input($vars['priority_color']).
             ',priority_urgency='.db_input($vars['priority_urgency']);

        if($id) {
            $sql='UPDATE '.PRIORITY_TABLE.' SET '.$sql.' WHERE priority_id='.db_input($id);
            if(db_query($sql))
                return true;

            $errors['err']='Unable to update Priority. Internal error occurred';
			echo $sql;
        }else{
            $sql='INSERT INTO '.PRIORITY_TABLE.' SET '.$sql.'';
            if(db_query($sql) && ($id=db_insert_id()))
                return $id;

            $errors['err']='Unable to add Priority. Internal error';
        }

        return false;
    }
	
	

}
?>
