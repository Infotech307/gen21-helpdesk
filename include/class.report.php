<?php
/*********************************************************************
    class.topic.php

    Help topic helper

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

class Report {
    var $id;

    var $div;

    var $parent;
    var $page;

    function Report($id) {
        $this->id=0;
        $this->load($id);
    }

    function load($id=0) {

        if(!$id && !($id=$this->getId()))
            return false;

        $sql='SELECT * '            
            .' FROM '.DIVISION_TABLE
            .' WHERE DivisionID='.db_input($id);

        if(!($res=db_query($sql)) || !db_num_rows($res))
            return false;

        $this->dv = db_fetch_array($res);
        $this->id = $this->dv['DivisionID'];
        $this->page = null;


        return true;
    }

    function reload() {
        return $this->load();
    }

    function asVar() {
        return $this->getName();
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->dv['DivisionName'];
    }

    function getPageId() {
        return $this->dv['page_id'];
    }

    function getPage() {
        if(!$this->page && $this->getPageId())
            $this->page = Page::lookup($this->getPageId());

        return $this->page;
    }


    function isEnabled() {
         return ($this->dv['isactive']);
    }

    function isActive() {
        return $this->isEnabled();
    }


    function getHashtable() {
        return $this->dv;
    }

    function getInfo() {
        return $this->getHashtable();
    }

    function update($vars, &$errors) {

        if(!$this->save($this->getId(), $vars, $errors))
            return false;

        $this->reload();
        return true;
    }

    function delete() {

        $sql='DELETE FROM '.DIVISION_TABLE.' WHERE DivisionID='.db_input($this->getId()).' LIMIT 1';
        if(db_query($sql) && ($num=db_affected_rows())) {

        }

        return $num;
    }
    /*** Static functions ***/
    function create($vars, &$errors) {
        return self::save(0, $vars, $errors);
    }

    function getReport($publicOnly=false) {

        $report=array();
        $sql='SELECT DivisionID '
            .' FROM '.DIVISION_TABLE
            .' WHERE isactive=1';

        $sql.=' ORDER BY DivisionName';
        if(($res=db_query($sql)) && db_num_rows($res))
            while(list($id, $name)=db_fetch_row($res))
                $divisions[$id]=$name;

        return $divisions;
    }

//    function getPublicDivisions() {
//        return self::getDivisions(true);
//    }

    function getIdByName($name, $pid=0) {

        $sql='SELECT DivisionID FROM '.DIVISION_TABLE
            .' WHERE DivisionName='.db_input($division);
        if(($res=db_query($sql)) && db_num_rows($res))
            list($id) = db_fetch_row($res);

        return $id;
    }

    function lookup($id) {
        return ($id && is_numeric($id) && ($t= new Division($id)) && $t->getId()==$id)?$t:null;
    }

    function save($id, $vars, &$errors) {

        $vars['DivisionName']=Format::striptags(trim($vars['DivisionName']));

//        if($id && $id!=$vars['id'])
//            $errors['err']='Internal error. Try again';
//
//        if(!$vars['DivisionName'])
//            $errors['division']='Division required';
//        elseif(strlen($vars['topic'])<5)
//            $errors['topic']='Topic is too short. 5 chars minimum';
        if(($tid=self::getIdByName($vars['DivisionName'])) && $tid!=$id)
            $errors['division']='Division already exists';


        if($errors) return false;

        $sql=' updated=NOW() '
            .',DivisionName='.db_input($vars['division'])           
            .',isactive='.db_input($vars['isactive']);

      
        if($id) {
            $sql='UPDATE '.DIVISION_TABLE.' SET '.$sql.' WHERE DivisionID='.db_input($id);
            if(db_query($sql))
                return true;

            $errors['err']='Unable to update division. Data already exist';
        } else {
            $sql='INSERT INTO '.DIVISION_TABLE.' SET '.$sql.',created=NOW()';
            if(db_query($sql) && ($id=db_insert_id()))
                return $id;

            $errors['err']='Unable to create the division. Data already exist';
        }

        return false;
    }
}
?>
