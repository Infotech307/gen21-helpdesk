<?php 
include_once(INCLUDE_DIR.'class.email.php');
Class User{
    function create($vars, &$errors) {
        global $ost, $cfg, $thisclient, $_FILES;
		
        // Drop extra whitespace 
        foreach (array('email', 'phone', 'name') as $f)
            if (isset($vars[$f]))
                $vars[$f] = trim($vars[$f]);

        //Check for 403
        if ($vars['email']  && Validator::is_email($vars['email'])) {
			
            //Make sure the email address is not banned
            if(TicketFilter::isBanned($vars['email'])) {
                $errors['err']='Registration denied. Error #403';
                $errors['errno'] = 403;
                $ost->logWarning('Registration denied', 'Banned email - '.$vars['email']);
                return 0;
            }
        }
        $id=0;
        $fields=array();
  		$fields['name']     	= array('type'=>'string',   'required'=>1, 'error'=>'Name required');
        $fields['email']    	= array('type'=>'email',    'required'=>1, 'error'=>'Valid email required');
		$fields['groupclients'] = array('type'=>'int', 		'required'=>1, 'error'=>'Group Clients required');
		$fields['division'] 	= array('type'=>'int', 		'required'=>1, 'error'=>'Division required');
		$fields['location'] 	= array('type'=>'int', 		'required'=>1, 'error'=>'Location required');
		$fields['designation']	= array('type'=>'int',		'required'=>1, 'error'=>'Designation required');
        $fields['phone']        = array('type'=>'phone',    'required'=>0, 'error'=>'Valid phone required');
        $fields['passwd1']        = array('type'=>'string',    'required'=>1, 'error'=>'Password required');
        $fields['status']        = array('type'=>'int',    'required'=>1, 'error'=>'Status required');
		
		if($vars['name']  && Validator::is_name($vars['name'])){
			$errors['name'] = "Please use only alphabetic characters";
		}

		if(!$vars['iduser']){
		
			if(Validator::is_email($vars['email'])){
				if(Validator::is_emailExist($vars['email'])){
					$errors['email'] = "Email already exist.";
				}
					
				//if(Validator::is_bufferExist($vars['email'])){
				//}
				//else $errors['email'] = "This email didn't have permission to register";
			}
			else $errors['email'] = "Invalid Email Address";
		}
		
		if($vars['passwd1']){
			if(Validator::is_passwordLength($vars['passwd1'])){
				$errors['passwd1'] = "Password must be between 6-18 characters";
			}
			if(Validator::is_passwordConfirm($vars['passwd1'],$vars['passwd2'])){
				$errors['passwd2'] = "Please re-type your password";
			}
		}
		
        if(!Validator::process($fields, $vars, $errors) && !$errors['err'])
            $errors['err'] ='Missing or invalid data - check the errors and try again';

        //Make sure phone extension is valid
        if($vars['phone_ext'] ) {
            if(!is_numeric($vars['phone_ext']) && !$errors['phone'])
                $errors['phone']='Invalid phone ext.';
            elseif(!$vars['phone']) //make sure they just didn't enter ext without phone # XXX: reconsider allowing!
                $errors['phone']='Phone number required';
        }
		
        //Any error above is fatal.
        if($errors)  return 0;
		//Generate random password
		//$randPassword = self::randomString();
        //Last minute checks
        $ipaddress=$vars['ip']?$vars['ip']:$_SERVER['REMOTE_ADDR'];
	
		//add to database
		$currentID = mysql_query("select user_id from ost_user order by user_id desc limit 1");
		if($currentID){
			$userID = mysql_result($currentID, 0, 0);
		}
		else $userID = 0;
		$userID = $userID + 1;
		
		$sql= 'INSERT INTO '.TABLE_PREFIX.'user SET created_date=NOW(),modified_date=NOW() '
			.' ,user_id='.db_input($userID)
			.' ,name='.db_input(Format::striptags($vars['name']))
			.' ,email='.db_input($vars['email'])
			.' ,password='.db_input(md5($vars['passwd1']))
			.' ,phone="'.db_input($vars['phone'],false) . '"'
			.' ,phone_ext='.db_input($vars['phone_ext']?$vars['phone_ext']:'')
			.' ,groupclients_id='.db_input($vars['groupclients'])
			.' ,DivisionID='.db_input($vars['division'])
			.' ,LocationID='.db_input($vars['location'])
			.' ,DesignationID='.db_input($vars['designation'])
			.' ,source='.db_input($source)
			.' ,ip_address='.db_input($ipaddress).''
			.',allowreply='.db_input($vars['allowreply'])
			.' ,status='.db_input($vars['status']).'';
		db_query($sql);
		
        /* Phew! ... time for tea (KETEPA) */
		
        return $userID;
    }
	
	function update($vars, &$errors) {
        global $ost, $cfg, $thisclient, $_FILES;
		
        // Drop extra whitespace 
        foreach (array('email', 'phone', 'name') as $f)
            if (isset($vars[$f]))
                $vars[$f] = trim($vars[$f]);

        //Check for 403
        if ($vars['email']  && Validator::is_email($vars['email'])) {
			
            //Make sure the email address is not banned
            if(TicketFilter::isBanned($vars['email'])) {
                $errors['err']='Registration denied. Error #403';
                $errors['errno'] = 403;
                $ost->logWarning('Registration denied', 'Banned email - '.$vars['email']);
                return 0;
            }
        }
        $id=0;
        $fields=array();
  		$fields['name']     	= array('type'=>'string',   'required'=>1, 'error'=>'Name required');
        $fields['email']    	= array('type'=>'email',    'required'=>1, 'error'=>'Valid email required');
		$fields['groupclients'] = array('type'=>'int', 		'required'=>1, 'error'=>'Group Clients required');
		$fields['division'] 	= array('type'=>'int', 		'required'=>1, 'error'=>'Division required');
		$fields['location'] 	= array('type'=>'int', 		'required'=>1, 'error'=>'Location required');
		$fields['designation']	= array('type'=>'int',		'required'=>1, 'error'=>'Designation required');
        $fields['phone']        = array('type'=>'phone',    'required'=>0, 'error'=>'Valid phone required');
        $fields['status']        = array('type'=>'int',    'required'=>1, 'error'=>'Status required');
		
		if($vars['name']  && Validator::is_name($vars['name'])){
			$errors['name'] = "Please use only alphabetic characters";
		}

		if(!$vars['iduser']){
		
			if(Validator::is_email($vars['email'])){
				if(Validator::is_emailExist($vars['email']) && self::is_emailMember($vars['email'],$vars['id'])){
					$errors['email'] = "Email already exist.";
				}
					
				//if(Validator::is_bufferExist($vars['email'])){
				//}
				//else $errors['email'] = "This email didn't have permission to register";
			}
			else $errors['email'] = "Invalid Email Address";
		}
		
		if($vars['passwd1']){
			if(Validator::is_passwordLength($vars['passwd1'])){
				$errors['passwd1'] = "Password must be between 6-18 characters";
			}
			if(Validator::is_passwordConfirm($vars['passwd1'],$vars['passwd2'])){
				$errors['passwd2'] = "Please re-type your password";
			}
		}
		
		if($vars['oldpassword'] || $vars['newpassword']){ 
			if(Validator::is_passwordExist($vars['id'],$vars['oldpassword'])){
				$errors['oldpassword'] = "Incorrect Password";
			}
			if(Validator::is_passwordLength($vars['newpassword'])){
				$errors['newpassword'] = "Password must be between 6-18 characters";
			}
			if(Validator::is_passwordConfirm($vars['newpassword'],$vars['rtnpassword'])){
				$errors['rtnpassword'] = "Please re-type your password";
			}
		}
		
        if(!Validator::process($fields, $vars, $errors) && !$errors['err'])
            $errors['err'] ='Missing or invalid data - check the errors and try again';

        //Make sure phone extension is valid
        if($vars['phone_ext'] ) {
            if(!is_numeric($vars['phone_ext']) && !$errors['phone'])
                $errors['phone']='Invalid phone ext.';
            elseif(!$vars['phone']) //make sure they just didn't enter ext without phone # XXX: reconsider allowing!
                $errors['phone']='Phone number required';
        }

        //Any error above is fatal.
        if($errors)  return 0;

		//add to database
		$userID = $vars['id'];
		$sql='UPDATE '.TABLE_PREFIX.'user SET modified_date=NOW()';
		if($vars['oldpassword'] && $vars['newpassword']){
		$sql .=	' ,password='.db_input(md5($vars['newpassword']));
		}else if($vars['passwd1'] && $vars['passwd2']){
		$sql .=	' ,password='.db_input(md5($vars['passwd1']));
		}
		$sql .= ' ,phone="'.db_input($vars['phone'],false) . '"'
				.' ,phone_ext='.db_input($vars['phone_ext']?$vars['phone_ext']:'')
				.' ,groupclients_id='.db_input($vars['groupclients'])
				.' ,DivisionID='.db_input($vars['division'])
				.' ,LocationID='.db_input($vars['location'])
				.' ,DesignationID='.db_input($vars['designation'])
				.' ,name='.db_input(Format::striptags($vars['name']))
				.' ,email='.db_input($vars['email'])
				.' ,allowreply='.db_input($vars['allowreply'])
				.' ,status='.db_input($vars['status'])
				.' WHERE user_id='.db_input($userID).'';
		db_query($sql);
        /* Phew! ... time for tea (KETEPA) */
		
        return $userID;
    }
		
	function resetpass($vars, &$errors) {
        global $ost, $cfg, $thisclient, $_FILES;
		
        // Drop extra whitespace 
        foreach (array('email') as $f)
            if (isset($vars[$f]))
                $vars[$f] = trim($vars[$f]);

        //Check for 403
        if ($vars['email']  && Validator::is_email($vars['email'])) {
			
            //Make sure the email address is not banned
            if(TicketFilter::isBanned($vars['email'])) {
                $errors['err']='Reset Password denied. Error #403';
                $errors['errno'] = 403;
                $ost->logWarning('Reset Password denied', 'Banned email - '.$vars['email']);
                return 0;
            }
        }
        $id=0;
        $fields=array();
        $fields['email']    	= array('type'=>'email',    'required'=>1, 'error'=>'Valid email required');
		
		if(!Validator::is_emailExist($vars['email'])){
			$errors['email'] = "Email not exist";
		}

        if(!Validator::process($fields, $vars, $errors) && !$errors['err'])
            $errors['err'] ='Missing or invalid data - check the errors and try again';
		
        //Any error above is fatal.
        if($errors)  return 0;
		//Generate random password
		$randPassword = self::randomString();
        //Last minute checks
	
		//add to database
		$sql='UPDATE '.TABLE_PREFIX.'user SET modified_date=NOW()';
		$sql .=	' ,password='.db_input(md5($randPassword));
		$sql .= ' WHERE email='.db_input($vars['email']);

		db_query($sql);
		
		/*send email to user*/
		if($vars['email']){
			$to = $vars['email'];
			$subject = "Request for Password";
			$message = "-- Do Not Reply --\n\nYour request for password has been granted.\n\nPlease login with your email and this following password ".$randPassword.". Please change your password in edit account menu. \n\nThank You";
			$from = sprintf('"Gen21Support"<%s>', NOREPLY_EMAIL);
			$sendmail = Mailer::sendmail($to, $subject, $message, $from);
		}
        /* Phew! ... time for tea (KETEPA) */
		
        return $vars['email'];
    }
	
	function randomString($length = 10) {
		$str = "";
		$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
		}
		return $str;
	}
	
	function is_emailMember($email,$id){
		if($id > 0){
			$sql = mysql_query("select count(email) from ".TABLE_PREFIX."user where email like '".$email."' and user_id = $id") or die ("Checking Mail Error. Please contact your administration");
			while ($query=mysql_fetch_array($sql)){
				$exist = $query['count(email)']; 			
			}
			if(empty($exist) || $exist == "" || $exist = 0 ){
				return true;
			}
			else return false;

		}else{
			return false;
		}
	}
		
}
?>