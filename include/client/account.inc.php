<?php
if(!defined('OSTCLIENTINC')) die('Access Denied!');
$info=array();
global $ldapcon;
if($thisclient && $thisclient->isValid()) {
	$info=array('name'=>$thisclient->getName(),
					'email'				=>$thisclient->getEmail(),
					'groupclients'		=>$thisclient->getGroupclient(),										'allowreply'		=>$thisclient->getAllowreply(),
					'status'			=>$thisclient->getStatus(),
					'divisionID'		=>$thisclient->getDivisionID(),
					'locationID'		=>$thisclient->getLocationID(),
					'designationID'		=>$thisclient->getDesignationID(),
					'phone'				=>$thisclient->getPhone(),
					'phone_ext'			=>$thisclient->getPhoneExt());
}
$info=($_POST && $errors)?Format::htmlchars($_POST):$info;

include "ost-config.php";
mysql_connect(DBHOST,DBUSER,DBPASS);
mysql_select_db(DBNAME);
?>
<script>
function showSubCat(str)
{
if (str=="")
  {
  document.getElementById("SubcategoryID").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("SubcategoryID").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","include/getSubCat.php?q="+str,true);
xmlhttp.send();
}
</script>
<h1>Edit Account</h1>
<form id="registrationForm" method="post" action="account.php" enctype="multipart/form-data">
  <?php csrf_token(); ?>
  <input type="hidden" name="a" value="open">
  <input type="hidden" name="id" value="<?php echo $thisclient->getId(); ?>">
  <table width="800" cellpadding="1" cellspacing="0" border="0">
	<input type="hidden" name="groupclients" id="groupclients" value="<?php echo $info['groupclients']; ?>" /> 	<input type="hidden" name="allowreply" id="allowreply" value="<?php echo $info['allowreply']; ?>" /> 
	<input type="hidden" name="status" id="status" value="<?php echo $info['status']; ?>" />
    <tr>
        <th class="required" width="160">Full Name:</th>
        <td><input id="name" type="text" name="name" size="30" value="<?php echo $info['name']; ?>" readonly="readonly">
		<font class="error"><?php echo $errors['name']; ?></font></td>
    </tr>
    <tr>
        <th class="required" width="160">Email Address:</th>
        <td><input id="email" type="text" name="email" size="30" value="<?php echo $info['email']; ?>" readonly="readonly"></td>
    </tr>
	<tr>
        <th class="required" width="160">Old Password:</th>
        <td>
			<input id="oldpassword" type="password" name="oldpassword" size="30" />
			<font class="error"><?php echo $errors['oldpassword']; ?></font>
        </td>
    </tr>
	<tr>
        <th class="required" width="160">New Password:</th>
        <td>
			<input id="newpassword" type="password" name="newpassword" size="30" />
			<font class="error"><?php echo $errors['newpassword']; ?></font>
        </td>
    </tr>
	<tr>
        <th class="required" width="160">Re-Type Password:</th>
        <td>
			<input id="rtnpassword" type="password" name="rtnpassword" size="30" />
			<font class="error"><?php echo $errors['rtnpassword']; ?></font>
        </td>
    </tr>
    <tr>
        <th>Telephone:</th>
        <td>
            <input id="phone" type="text" name="phone" size="17" value="<?php echo $info['phone']; ?>">
            <label for="ext" class="inline">Ext.:</label>
            <input id="ext" type="text" name="phone_ext" size="3" value="<?php echo $info['phone_ext']; ?>">
            <font class="error">&nbsp;<?php echo $errors['phone']; ?>&nbsp;&nbsp;<?php echo $errors['phone_ext']; ?></font>
        </td>   
    </tr>
    <td class="required">Division:</td>
        <td><?php
            if($thisclient && $thisclient->isValid()) {
                $idd= $thisclient->getDivisionID(); ?>
				<select id="division" name="division" style="width:250px;">
                <option value="" selected="selected">&mdash; Select a Division &mdash;</option>
                <?php					   
					   $sqll = mysql_query("select * from ".TABLE_PREFIX."division order by DivisionName") or die ("not connect");
					   while ($r=mysql_fetch_row($sqll)){?>
                       	<?php if ($r[0] == $idd){?> 
						<option value="<?php echo $r[0]; ?>" selected="selected"> <?php echo strtoupper($r[1]); ?></option>
						<?php } else { ?>
						<option value="<?php echo $r[0]; ?>"> <?php echo strtoupper($r[1]); ?></option>
						<?php } } ?>         
            </select>
           <?php } ?>
		<font class="error">*&nbsp;<?php echo $errors['division']; ?></font>
        </td>
    </tr>        <tr>
        <td class="required">Location:</td>
        <td>
       <?php
            if($thisclient && $thisclient->isValid()) {
                $idl= $thisclient->getLocationID(); ?>
				<select id="location" name="location" style="width:250px;">
                <option value="" selected="selected">&mdash; Select a Location &mdash;</option>
                <?php					   
					   $sqll = mysql_query("select * from ".TABLE_PREFIX."location order by LocationName") or die ("not connect");
					   while ($r=mysql_fetch_row($sqll)){?>
                       	<?php if ($r[0] == $idl){?> 
						<option value="<?php echo $r[0]; ?>" selected="selected"> <?php echo strtoupper($r[1]); ?></option>
						<?php } else { ?>
						<option value="<?php echo $r[0]; ?>"> <?php echo strtoupper($r[1]); ?></option>
						<?php } } ?>         
            </select>
           <?php } ?>
			<font class="error">*&nbsp;<?php echo $errors['location']; ?></font>
        </td>
    </tr>        <tr>
        <td class="required">Designation:</td>
        <td>
         <?php
            if($thisclient && $thisclient->isValid()) {
                $idds= $thisclient->getDesignationID(); ?>
				<select id="designation" name="designation" style="width:250px;">
                <option value="" selected="selected">&mdash; Select a Designation &mdash;</option>
                <?php					   
					   $sqll = mysql_query("select * from ".TABLE_PREFIX."designation order by DesignationName") or die ("not connect");
					   while ($r=mysql_fetch_row($sqll)){?>
                       	<?php if ($r[0] == $idds){?> 
						<option value="<?php echo $r[0]; ?>" selected="selected"> <?php echo strtoupper($r[1]); ?></option>
						<?php } else { ?>
						<option value="<?php echo $r[0]; ?>"> <?php echo strtoupper($r[1]); ?></option>
						<?php } } ?>         
            </select>
           <?php } ?>
			<font class="error">*&nbsp;<?php echo $errors['designation']; ?></font>
        </td>
    <tr><td colspan=2>&nbsp;</td></tr>
    <?php
    if($cfg && $cfg->isCaptchaEnabled() && (!$thisclient || !$thisclient->isValid())) {
        if($_POST && $errors && !$errors['captcha'])
            $errors['captcha']='Please re-enter the text again';
        ?>
    <tr class="captchaRow">
        <td class="required">CAPTCHA Text:</td>
        <td>
            <span class="captcha"><img src="captcha.php" border="0" align="left"></span>
            &nbsp;&nbsp;
            <input id="captcha" type="text" name="captcha" size="6">
            <em>Enter the text shown on the image.</em>
            <font class="error">*&nbsp;<?php echo $errors['captcha']; ?></font>
        </td>
    </tr>
    <?php
    } ?>
    <tr><td colspan=2>&nbsp;</td></tr>
  </table>
  <p style="padding-left:150px;">
        <input type="submit" value="Submit">
        <input type="reset" value="Reset">
        <input type="button" value="Cancel" onClick='window.location.href="index.php"'>
  </p>
  <input type="hidden" name="source" id="source" value="web" />
</form>
