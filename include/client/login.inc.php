<?php
if(!defined('OSTCLIENTINC')) die('Access Denied');

$email=Format::input($_POST['lemail']?$_POST['lemail']:$_GET['e']);
$ticketid=Format::input($_POST['lticket']?$_POST['lticket']:$_GET['t']);
//global $ldapcon;
?>
<h1>Check Ticket Status</h1>
<p>To view the status of a ticket, provide us with the login details below.</p>
<form action="login.php" method="post" id="clientLogin">
    <?php csrf_token(); ?>
    <strong><?php echo Format::htmlchars($errors['login']); ?></strong>
    <br>
    <div>
        <label for="email">Email :</label>
		<input id="email" type="text" name="lemail" size="30" value="" />
    </div>
    <div>
		<label for="pw">Password :</label>
		<input id="pw" type="password" name="lpassword" size="19" value=""></td>
    </div>
    <p>
        <input class="btn" type="submit" value="Log In">
    </p>
	<p><a href="password.php">[ Give me Password ]</a></p>
</form>
<br>
<!--<p>
If this is your first time contacting us, please <a href="register.php">register here</a>.    
</p>-->
