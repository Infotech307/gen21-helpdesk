<?php
if(!defined('OSTCLIENTINC')) die('Access Denied!');
include "ost-config.php";
?>
<h1>Reset Password</h1>
<p>Submit your email and we will give you new password</p>
<form id="resetPasswordForm" method="post" action="password.php" enctype="multipart/form-data">
  <?php csrf_token(); ?>
  <table width="800" cellpadding="1" cellspacing="0" border="0">
    <tr>
        <th class="required" width="160">Email Address:</th>
		<td><input type="text" id="email" name="email" value="" />
		<font class="error">*&nbsp;<?php echo $errors['email']; ?></font>
		</td>
	</tr>
    <?php
    if($cfg && $cfg->isCaptchaEnabled() && (!$thisclient || !$thisclient->isValid())) {
        if($_POST && $errors && !$errors['captcha'])
            $errors['captcha']='Please re-enter the text again';
        ?>
    <tr class="captchaRow">
        <td class="required">CAPTCHA Text:</td>
        <td>
            <span class="captcha"><img src="captcha.php" border="0" align="left"></span>
            &nbsp;&nbsp;
            <input id="captcha" type="text" name="captcha" size="6">
            <em>Enter the text shown on the image.</em>
            <font class="error">*&nbsp;<?php echo $errors['captcha']; ?></font>
        </td>
    </tr>
    <?php
    } ?>
    <tr><td colspan=2>&nbsp;</td></tr>
  </table>
  <p style="padding-left:150px;">
        <input type="submit" value="Give Me New Password">
  </p>
</form>
