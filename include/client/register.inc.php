<?php
if(!defined('OSTCLIENTINC')) die('Access Denied!');
//global $ldapcon;

include "ost-config.php";
mysql_connect(DBHOST,DBUSER,DBPASS);
mysql_select_db(DBNAME);
?>
<script>
function showSubCat(str)
{
if (str=="")
  {
  document.getElementById("SubcategoryID").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("SubcategoryID").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","include/getSubCat.php?q="+str,true);
xmlhttp.send();
}
</script>
<h1>New User Registration</h1>
<p>Please fill in the form below to register a new user.</p>
<form id="registrationForm" method="post" action="register.php" enctype="multipart/form-data">
  <?php csrf_token(); ?>
  <input type="hidden" name="a" value="open">
  <table width="800" cellpadding="1" cellspacing="0" border="0">
    <tr>
        <th class="required" width="160">Full Name:</th>
		<td><input type="text" id="name" name="name" value="" />
		<font class="error">*&nbsp;<?php echo $errors['name']; ?></font></td>
		
    </tr>
    <tr>
        <th class="required" width="160">Email Address:</th>
		<td><input type="text" id="email" name="email" value="" />
		<font class="error">*&nbsp;<?php echo $errors['email']; ?></font>
		</td>
	</tr>
    <tr>
        <th>Telephone:</th>
        <td>
            <input id="phone" type="text" name="phone" size="17" value="<?php echo $ldapphone; ?>" />
            <label for="ext" class="inline">Ext.:</label>
            <input id="ext" type="text" name="phone_ext" size="3" value="<?php echo $ldapext; ?>" />
            <font class="error">&nbsp;<?php echo $errors['phone']; ?>&nbsp;&nbsp;<?php echo $errors['phone_ext']; ?></font>
        </td>   
    </tr>
    <td class="required">Division:</td>
        <td>
            <select id="division" name="division" style="width:250px;">
                <option value="" selected="selected">&mdash; Select a Division &mdash;</option>
                <?php					   
					   $sqll = mysql_query("select * from ".TABLE_PREFIX."division") or die ("not connect");
					   while ($r=mysql_fetch_row($sqll)){?>
						<option value="<?php echo $r[0]; ?>"> <?php echo $r[1]; ?></option>
						<?php } ?>         
            </select>
		<font class="error">*&nbsp;<?php echo $errors['division']; ?></font>
        </td>
    </tr>        <tr>
        <td class="required">Location:</td>
        <td>
            <select id="location" name="location" style="width:250px;">
                <option value="" selected="selected">&mdash; Select a Location &mdash;</option>
                <?php					   
					   $sqll = mysql_query("select * from ".TABLE_PREFIX."location") or die ("not connect");
					   while ($r=mysql_fetch_row($sqll)){?>
						<option value="<?php echo $r[0]; ?>"> <?php echo $r[1]; ?></option>
						<?php } ?>         
            </select>
			<font class="error">*&nbsp;<?php echo $errors['location']; ?></font>
        </td>
    </tr>        <tr>
        <td class="required">Designation:</td>
        <td>
            <select id="designation" name="designation" style="width:250px;">
                <option value="" selected="selected">&mdash; Select a Designation &mdash;</option>
                <?php					   
					   $sqll = mysql_query("select * from ".TABLE_PREFIX."designation") or die ("not connect");
					   while ($r=mysql_fetch_row($sqll)){?>
						<option value="<?php echo $r[0]; ?>"> <?php echo $r[1]; ?></option>
						<?php } ?>         
            </select>
			<font class="error">*&nbsp;<?php echo $errors['designation']; ?></font>
        </td>
    <tr><td colspan=2>&nbsp;</td></tr>
    <?php
    if($cfg && $cfg->isCaptchaEnabled() && (!$thisclient || !$thisclient->isValid())) {
        if($_POST && $errors && !$errors['captcha'])
            $errors['captcha']='Please re-enter the text again';
        ?>
    <tr class="captchaRow">
        <td class="required">CAPTCHA Text:</td>
        <td>
            <span class="captcha"><img src="captcha.php" border="0" align="left"></span>
            &nbsp;&nbsp;
            <input id="captcha" type="text" name="captcha" size="6">
            <em>Enter the text shown on the image.</em>
            <font class="error">*&nbsp;<?php echo $errors['captcha']; ?></font>
        </td>
    </tr>
    <?php
    } ?>
    <tr><td colspan=2>&nbsp;</td></tr>
  </table>
  <p style="padding-left:150px;">
        <input type="submit" value="Submit">
        <input type="reset" value="Reset">
        <input type="button" value="Cancel" onClick='window.location.href="index.php"'>
  </p>
  <input type="hidden" name="source" id="source" value="web" />
</form>
