<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');

$info=array();
$state = 'selected="selected"';$statd = '';
$defn = 'selected="selected"';$defy = '';
if($client && $_REQUEST['a']!='add'){
    //Editing Department.
    $title='Update Client';
    $action='update';
    $submit_text='Save Changes';
    $info['id']=$client->getId();
    $info['name'] = $client->getName();
    $info['groupclient'] = $client->getGroupclients();
    $info['default'] = $client->getDefault();
    $info['status'] = $client->getStatus();
	if($info['status'] == 1){
		$state = '';$statd = 'selected="selected"';
	}
	if($info['default'] == 1){
		$defn = ''; $defy = 'selected="selected"';
	}
}else {
    $title='Add New Client';
    $action='create';
    $submit_text='Add New Client';
}
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);
?>
<form id="clientsForm" method="post" action="clients.php" enctype="multipart/form-data">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="<?php echo $action; ?>">
 <input type="hidden" name="a" value="<?php echo Format::htmlchars($_REQUEST['a']); ?>">
 <input type="hidden" name="id" value="<?php echo $info['id']; ?>">
 <h2>Client Info</h2>
 <table class="form_table" width="1280" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4><?php echo $title; ?></h4>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="180" class="required">
                Name:
            </td>
            <td>
                <input type="text" size="30" name="name" value="<?php echo $info['name']; ?>">
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['name']; ?></span>
            </td>
        </tr>
		<tr>
            <td width="180" class="required">
                Group Client:
            </td>
            <td>
                <select id="groupclients" name="groupclients" style="width:250px;">
					<option value="" selected="selected">&mdash; Select a Group Clients &mdash;</option>
					<?php					   
						$sqll = mysql_query("select * from ost_groupclients") or die ("not connect");
						while ($r=mysql_fetch_row($sqll)){
							if($r[0] == $info['groupclient']){ ?>
								<option value="<?php echo $r[0]; ?>" selected="selected"> <?php echo $r[1]; ?></option>
							<?php }else{ ?>
								<option value="<?php echo $r[0]; ?>"> <?php echo $r[1]; ?></option>
							<?php } ?>         
						<?php } ?>         
				</select>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['groupclients']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                Status:
            </td>
            <td>
                <select id="status" name="status" style="width:250px;">
					<option value="0" <?php echo $state; ?>>Enable</option>
					<option value="1" <?php echo $statd; ?>>Disabled</option>        
				</select>
            </td>
        </tr>
		<tr>
            <td width="180" class="required">
                Default:
            </td>
            <td>
                <select id="default" name="default" style="width:250px;">
					<option value="0" <?php echo $defn; ?>>No</option>
					<option value="1" <?php echo $defy; ?>>Yes</option>        
				</select>
            </td>
        </tr>
    </tbody>
</table>
<p style="padding-left:250px;">
    <input type="submit" name="submit" value="<?php echo $submit_text; ?>">
    <input type="reset"  name="reset"  value="Reset">
    <input type="button" name="cancel" value="Cancel" onclick='window.location.href="clients.php"'>
</p>
</form>
