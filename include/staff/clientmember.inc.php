<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');

$info=array();
$state = 'selected="selected"';$statd = '';
if($client && $_REQUEST['a']!='add'){
    //Editing Department.
    $title='Update Client';
    $action='update';
    $submit_text='Save Changes';
    $passwd_text='To reset the password enter a new one below';
    $info['id']=$client->getId();
    $info['name']=$client->getName();
    $info['email']=$client->getEmail();
    $info['phone']=$client->getPhone();
    $info['phone_ext']=$client->getPhoneExt();
    $info['groupclients']=$client->getGroupclients();
    $info['division']=$client->getDivisionID();
    $info['location']=$client->getLocationID();
    $info['designation']=$client->getDesignationID();
    $info['status']=$client->getStatus();	$info['allowreply']=$client->getAllowreply();
	if($info['status'] == 1){
		$state = '';$statd = 'selected="selected"';
	}		if($info['allowreply'] == 1){		$statn = '';$staty = 'selected="selected"';	}
}else {
    $title='Add New Client';
    $action='create';
    $submit_text='Add Client';
    $passwd_text='Temp. password required &nbsp;<span class="error">&nbsp;*</span>';
    //Some defaults for new staff.
    $info['change_passwd']=1;
    $info['isactive']=1;
}
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);
?>
<form id="registrationForm" method="post" action="clientmembers.php" enctype="multipart/form-data">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="<?php echo $action; ?>">
 <input type="hidden" name="a" value="<?php echo Format::htmlchars($_REQUEST['a']); ?>">
 <input type="hidden" name="id" value="<?php echo $info['id']; ?>">
 <h2>Client Member Info</h2>
 <table class="form_table" width="1280" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4><?php echo $title; ?></h4>
                <em><strong>Client Information</strong></em>
            </th>
        </tr>
    </thead>
    <tbody>
	
        <tr>
            <td width="180" class="required">
                Full Name: 
            </td>
            <td>
                <input type="text" size="30" name="name" value="<?php echo $info['name']; ?>">
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['name']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                Email Address:
            </td>
            <td>
                <input type="text" size="30" name="email" value="<?php echo $info['email']; ?>">
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['email']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180">
                Telephone:
            </td>
            <td>
                <input type="text" size="18" name="phone" value="<?php echo $info['phone']; ?>">
                &nbsp;<span class="error">&nbsp;<?php echo $errors['phone']; ?></span>
                Ext <input type="text" size="5" name="phone_ext" value="<?php echo $info['phone_ext']; ?>">
                &nbsp;<span class="error">&nbsp;<?php echo $errors['phone_ext']; ?></span>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><strong>Account Password</strong>: <?php echo $passwd_text; ?> &nbsp;<span class="error">&nbsp;<?php echo $errors['temppasswd']; ?></span></em>
            </th>
        </tr>
        <tr>
            <td width="180">
                Password:
            </td>
            <td>
                <input type="password" size="18" name="passwd1" value="<?php echo $info['passwd1']; ?>">
                &nbsp;<span class="error">&nbsp;<?php echo $errors['passwd1']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180">
                Confirm Password:
            </td>
            <td>
                <input type="password" size="18" name="passwd2" value="<?php echo $info['passwd2']; ?>">
                &nbsp;<span class="error">&nbsp;<?php echo $errors['passwd2']; ?></span>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><strong>Account Status & Settings</strong></em>
            </th>
        </tr>
		<tr>
            <td width="180" class="required">
                Group Clients:
            </td>
            <td>
                <select id="groupclients" name="groupclients" style="width:250px;">
					<option value="" selected="selected">&mdash; Select a Group Clients &mdash;</option>
					<?php					   
						$sqll = mysql_query("select * from ost_groupclients") or die ("not connect");
						while ($r=mysql_fetch_row($sqll)){
							if($r[0] == $info['groupclients']){ ?>
								<option value="<?php echo $r[0]; ?>" selected="selected"> <?php echo $r[1]; ?></option>
							<?php }else{ ?>
								<option value="<?php echo $r[0]; ?>"> <?php echo $r[1]; ?></option>
							<?php } ?>         
						<?php } ?>         
				</select>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['groupclients']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                Division:
            </td>
            <td>
                <select id="division" name="division" style="width:250px;">
					<option value="" selected="selected">&mdash; Select a Division &mdash;</option>
					<?php					   
					   $sqll = mysql_query("select * from ".TABLE_PREFIX."division") or die ("not connect");
					   while ($r=mysql_fetch_row($sqll)){
							if($r[0] == $info['division']){ ?>
								<option value="<?php echo $r[0]; ?>" selected="selected"> <?php echo $r[1]; ?></option>
							<?php }else{ ?>
								<option value="<?php echo $r[0]; ?>"> <?php echo $r[1]; ?></option>
							<?php } ?>         
						<?php } ?>           
				</select>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['division']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                Location:
            </td>
            <td>
                <select id="location" name="location" style="width:250px;">
					<option value="" selected="selected">&mdash; Select a Location &mdash;</option>
					<?php					   
					   $sqll = mysql_query("select * from ".TABLE_PREFIX."location") or die ("not connect");
					   while ($r=mysql_fetch_row($sqll)){
							if($r[0] == $info['location']){ ?>
								<option value="<?php echo $r[0]; ?>" selected="selected"> <?php echo $r[1]; ?></option>
							<?php }else{ ?>
								<option value="<?php echo $r[0]; ?>"> <?php echo $r[1]; ?></option>
							<?php } ?>         
						<?php } ?>         
				</select>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['location']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                Designation:
            </td>
            <td>
                <select id="designation" name="designation" style="width:250px;">
					<option value="" selected="selected">&mdash; Select a Designation &mdash;</option>
					<?php	
						$sqll = mysql_query("select * from ".TABLE_PREFIX."designation") or die ("not connect");
					   while ($r=mysql_fetch_row($sqll)){
							if($r[0] == $info['designation']){ ?>
								<option value="<?php echo $r[0]; ?>" selected="selected"> <?php echo $r[1]; ?></option>
							<?php }else{ ?>
								<option value="<?php echo $r[0]; ?>"> <?php echo $r[1]; ?></option>
							<?php } ?>         
						<?php } ?>         
				</select>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['designation']; ?></span>
            </td>
        </tr>				<tr>            <td width="180" class="required">                Allow Reply All Tickets:            </td>            <td>                <select id="allowreply" name="allowreply" style="width:250px;">					<option value="0" <?php echo $statn; ?>>No</option>      					<option value="1" <?php echo $staty; ?>>Yes</option>      				</select>                &nbsp;<span class="error">*&nbsp;<?php echo $errors['status']; ?></span>            </td>        </tr>
		<tr>
            <td width="180" class="required">
                Status:
            </td>
            <td>
                <select id="status" name="status" style="width:250px;">
					<option value="0" <?php echo $state; ?>>Enabled</option>      
					<option value="1"<?php echo $statd; ?>>Disabled</option>      
				</select>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['status']; ?></span>
            </td>
        </tr>
    </tbody>
</table>
<p style="padding-left:250px;">
    <input type="submit" name="submit" value="<?php echo $submit_text; ?>">
    <input type="reset"  name="reset"  value="Reset">
    <input type="button" name="cancel" value="Cancel" onclick='window.location.href="staff.php"'>
</p>
</form>
