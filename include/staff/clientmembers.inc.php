<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');
$qstr='';
$select='SELECT u.user_id,u.name,u.email,u.status,d.DivisionName as division, l.LocationName as location, s.DesignationName as designation, g.name as groupclients';
$from='FROM ost_user u 
left join ost_division d on u.DivisionID = d.DivisionID
left join ost_location l on u.LocationID = l.LocationID
left join ost_designation s on u.DesignationID = s.DesignationID
left join ost_groupclients g on u.groupclients_id = g.id';
$where='WHERE 1 ';

if($_REQUEST['did'] && is_numeric($_REQUEST['did'])) {
    $where.=' AND groupclients_id='.db_input($_REQUEST['did']);
    $qstr.='&did='.urlencode($_REQUEST['did']);
}

if(is_numeric($_REQUEST['sid']) && $_REQUEST['sid'] != 'n') {
    $where.=' AND u.status='.db_input($_REQUEST['sid']);
    $qstr.='&sid='.urlencode($_REQUEST['sid']);
}

$sortOptions=array('name'=>'name','email'=>'email','status'=>'status',
                   'group'=>'grp.group_name','dept'=>'dept.dept_name','created'=>'staff.created','login'=>'staff.lastlogin');
$orderWays=array('DESC'=>'DESC','ASC'=>'ASC');
$sort=($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])])?strtolower($_REQUEST['sort']):'name';
//Sorting options...
if($sort && $sortOptions[$sort]) {
    $order_column =$sortOptions[$sort];
}
$order_column=$order_column?$order_column:'staff.firstname,staff.lastname';

if($_REQUEST['order'] && $orderWays[strtoupper($_REQUEST['order'])]) {
    $order=$orderWays[strtoupper($_REQUEST['order'])];
}

$order=$order?$order:'ASC';
if($order_column && strpos($order_column,',')){
    $order_column=str_replace(','," $order,",$order_column);
}
$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by="$order_column $order ";

$total=db_count('SELECT count(DISTINCT u.user_id) '.$from.' '.$where);
$page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
$pageNav=new Pagenate($total,$page,PAGE_LIMIT);
$pageNav->setURL('clientmembers.php',$qstr.'&sort='.urlencode($_REQUEST['sort']).'&order='.urlencode($_REQUEST['order']));
//Ok..lets roll...create the actual query
$qstr.='&order='.($order=='DESC'?'ASC':'DESC');
$query="$select $from $where ORDER BY $order_by LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();
?>
<h2>Client Members</h2>
<div style="width:1280px; float:left;">
    <form action="clientmembers.php" method="GET" name="filter">
     <input type="hidden" name="a" value="filter" >
        <select name="did" id="did">
             <option value="0">&mdash; All Grup Clients &mdash;</option>
             <?php
             $sql='SELECT g.id, g.name '.
                  'FROM ost_groupclients g '.
                  'INNER JOIN ost_user u ON(g.id=u.groupclients_id) '.
                  'GROUP BY g.id ORDER BY g.name';
				  
             if(($res=db_query($sql)) && db_num_rows($res)){
                 while(list($id,$name, $users)=db_fetch_row($res)){
                     $sel=($_REQUEST['did'] && $_REQUEST['did']==$id)?'selected="selected"':'';
                     echo sprintf('<option value="%d" %s>%s</option>',$id,$sel,$name);
                 }
             }
             ?>
        </select>
        <select name="sid" id="sid">
            <option value="n">&mdash; All Status &mdash;</option>
            <option value="0">Enable</option>
            <option value="1">Disabled</option>
        </select>
        &nbsp;&nbsp;
        <input type="submit" name="submit" value="Apply"/>
    </form>
 </div>
<div style="float:right;text-align:right;padding-right:5px;"><b><a href="clientmembers.php?a=add" class="Icon newstaff">Add New Client Member</a></b></div>
<div class="clear"></div>
<?php
$res=db_query($query);
if($res && ($num=db_num_rows($res)))        
    $showing=$pageNav->showing();
else
    $showing='No client members found!';
?>
<form action="clientmembers.php" method="POST" name="client" >
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="mass_process" >
 <input type="hidden" id="action" name="a" value="" >
 <table class="list" border="0" cellspacing="1" cellpadding="0" width="1280">
    <caption><?php echo $showing; ?></caption>
    <thead>
        <tr>
            <th width="7px">&nbsp;</th>        
            <th width="200"><a <?php echo $name_sort; ?> href="clientmembers.php?<?php echo $qstr; ?>&sort=name">Name</a></th>
            <th width="100"><a <?php echo $username_sort; ?> href="clientmembers.php?<?php echo $qstr; ?>&sort=email">Email</a></th>
            <th width="100"><a  <?php echo $status_sort; ?> href="clientmembers.php?<?php echo $qstr; ?>&sort=groupclients">Group Client</a></th>
            <th width="120"><a  <?php echo $group_sort; ?>href="clientmembers.php?<?php echo $qstr; ?>&sort=division">Division</a></th>
            <th width="150"><a  <?php echo $dept_sort; ?>href="clientmembers.php?<?php echo $qstr; ?>&sort=location">Location</a></th>
            <th width="100"><a <?php echo $created_sort; ?> href="clientmembers.php?<?php echo $qstr; ?>&sort=designation">Designation</a></th>
			<th width="145"><a <?php echo $login_sort; ?> href="clientmembers.php?<?php echo $qstr; ?>&sort=status">Status</a></th>
        </tr>
    </thead>
    <tbody>
    <?php
        if($res && db_num_rows($res)):
            $ids=($errors && is_array($_POST['ids']))?$_POST['ids']:null;
            while ($row = db_fetch_array($res)) {
                $sel=false;
                if($ids && in_array($row['user_id'],$ids))
                    $sel=true;
                ?>
               <tr id="<?php echo $row['user_id']; ?>">
                <td width=7px>
                  <input type="checkbox" class="ckb" name="ids[]" value="<?php echo $row['user_id']; ?>" <?php echo $sel?'checked="checked"':''; ?> >
                <td><a href="clientmembers.php?id=<?php echo $row['user_id']; ?>"><?php echo Format::htmlchars($row['name']); ?></a>&nbsp;</td>
                <td><?php echo $row['email']; ?></td>
                <td><?php echo $row['groupclients']; ?></td>
                <td><?php echo Format::htmlchars($row['division']); ?></td>
                <td><?php echo Format::htmlchars($row['location']); ?></td>
                <td><?php echo Format::htmlchars($row['designation']); ?></td>
				<?php if($row['status'] == 0){$statusText = "Enable";}else{$statusText = "<b>Disabled</b>";} ?>
                <td><?php echo $statusText; ?></td>
               </tr>
            <?php
            } //end of while.
        endif; ?>
    <tfoot>
     <tr>
        <td colspan="8">
            <?php if($res && $num){ ?>
            Select:&nbsp;
            <a id="selectAll" href="#ckb">All</a>&nbsp;&nbsp;
            <a id="selectNone" href="#ckb">None</a>&nbsp;&nbsp;
            <a id="selectToggle" href="#ckb">Toggle</a>&nbsp;&nbsp;
            <?php }else{
                echo 'No client members found!';
            } ?>
        </td>
     </tr>
    </tfoot>
</table>
<?php
if($res && $num): //Show options..
    echo '<div>&nbsp;Page:'.$pageNav->getPageLinks().'&nbsp;</div>';
?>
<p class="centered" id="actions">
    <input class="button" type="submit" name="enable" value="Enable" >
    &nbsp;&nbsp;
    <input class="button" type="submit" name="disable" value="Disabled" >
    &nbsp;&nbsp;
    <input class="button" type="submit" name="delete" value="Delete">
</p>
<?php
endif;
?>
</form>

<div style="display:none;" class="dialog" id="confirm-action">
    <h3>Please Confirm</h3>
    <a class="close" href="">&times;</a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="enable-confirm">
        Are you sure want to <b>enable</b> selected client member?
    </p>
    <p class="confirm-action" style="display:none;" id="disable-confirm">
        Are you sure want to <b>disable</b> selected client member?
        <br><br>Locked client member won't be able to create tickets.
    </p>
    <p class="confirm-action" style="display:none;" id="delete-confirm">
        <font color="red"><strong>Are you sure you want to DELETE selected client member?</strong></font>
        <br><br>Deleted client member CANNOT be recovered.
    </p>
    <div>Please confirm to continue.</div>
    <hr style="margin-top:1em"/>
    <p class="full-width">
        <span class="buttons" style="float:left">
            <input type="button" value="No, Cancel" class="close">
        </span>
        <span class="buttons" style="float:right">
            <input type="button" value="Yes, Do it!" class="confirm">
        </span>
     </p>
    <div class="clear"></div>
</div>

