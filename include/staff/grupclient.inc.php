<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');

$info=array();
$state = 'selected="selected"';$statd = '';
if($groupclients && $_REQUEST['a']!='add'){
    //Editing Department.
    $title='Update Group Client';
    $action='update';
    $submit_text='Save Changes';
    $passwd_text='To reset the password enter a new one below';
    $info['id']=$groupclients->getId();
    $info['name'] = $groupclients->getName();
    $info['status'] = $groupclients->getStatus();
	if($info['status'] == 1){
		$state = '';$statd = 'selected="selected"';
	}
}else {
    $title='Add New Group Client';
    $action='create';
    $submit_text='Add New Group Client';
}
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);
?>
<form id="groupclientForm" method="post" action="groupclients.php" enctype="multipart/form-data">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="<?php echo $action; ?>">
 <input type="hidden" name="a" value="<?php echo Format::htmlchars($_REQUEST['a']); ?>">
 <input type="hidden" name="id" value="<?php echo $info['id']; ?>">
 <h2>Group Client Info</h2>
 <table class="form_table" width="1280" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4><?php echo $title; ?></h4>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="180" class="required">
                Name:
            </td>
            <td>
                <input type="text" size="30" name="name" value="<?php echo $info['name']; ?>">
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['name']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                Status:
            </td>
            <td>
                <select id="status" name="status" style="width:250px;">
					<option value="0" <?php echo $state; ?>>Enable</option>
					<option value="1" <?php echo $statd; ?>>Disabled</option>        
				</select>
            </td>
        </tr>
    </tbody>
</table>
<p style="padding-left:250px;">
    <input type="submit" name="submit" value="<?php echo $submit_text; ?>">
    <input type="reset"  name="reset"  value="Reset">
    <input type="button" name="cancel" value="Cancel" onclick='window.location.href="groupclients.php"'>
</p>
</form>
