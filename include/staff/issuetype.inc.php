<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');
$info=array();
$qstr='';
if($div && $_REQUEST['a']!='add') {
    $title='Update Issue Type';
    $action='update';
    $submit_text='Save Changes';
    $info=$div->getInfo();
    $info['id']=$div->getId();
   // $info['pid']=$topic->getPid();
    $qstr.='&id='.$div->getId();
} else {
    $title='Add New Issue Type';
    $action='create';
    $submit_text='Add Issue Type';
    $info['isactive']=isset($info['isactive'])?$info['isactive']:1;
    //$info['ispublic']=isset($info['ispublic'])?$info['ispublic']:1;
    $qstr.='&a='.$_REQUEST['a'];
}
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);
?>
<script type="text/javascript">
function f(o){o.value=o.value.toUpperCase().replace(/([^0-9A-Z])/g," ");}
</script>
<form action="issuetype.php?<?php echo $qstr; ?>" method="post" id="save">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="<?php echo $action; ?>">
 <input type="hidden" name="a" value="<?php echo Format::htmlchars($_REQUEST['a']); ?>">
 <input type="hidden" name="id" value="<?php echo $info['issueTypeID']; ?>">
 <h2>Issue Type</h2>
 <table class="form_table" width="1280" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4><?php echo $title; ?></h4>
                <em>Issue Type Information</em>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="180" class="required">
               Issue Type Name
            </td>
            <td>
                <input type="text" size="30" name="issuetype" value="<?php echo $info['issueTypeName']; ?>" onkeydown="f(this)" onkeyup="f(this)" onclick="f(this)">
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['issuetype']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                Status:
            </td>
            <td>
                <input type="radio" name="isactive" value="1" <?php echo $info['isactive']?'checked="checked"':''; ?>>Active
                <input type="radio" name="isactive" value="0" <?php echo !$info['isactive']?'checked="checked"':''; ?>>Disabled
                &nbsp;<span class="error">*&nbsp;</span>
            </td>
        </tr>
      
    </tbody>
</table>
<p style="padding-left:225px;">
    <input type="submit" name="submit" value="<?php echo $submit_text; ?>">
    <input type="reset"  name="reset"  value="Reset">
    <input type="button" name="cancel" value="Cancel" onclick='window.location.href="issuetype.php"'>
</p>
</form>
