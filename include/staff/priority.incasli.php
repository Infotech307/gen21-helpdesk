<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');
$info=array();
$qstr='';
if($priority && $_REQUEST['a']!='add'){
    $title='Update Priority';
    $action='update';
    $submit_text='Save Changes';
    $qstr.='&id='.$priority->getId();
}else {
    $title='Add New Priority';
    $action='add';
    $submit_text='Add Priority';
    $qstr.='&a='.urlencode($_REQUEST['a']);
}
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);
?>
<form action="priority.php?<?php echo $qstr; ?>" method="post" id="save">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="<?php echo $action; ?>">
 <input type="hidden" name="a" value="<?php echo Format::htmlchars($_REQUEST['a']); ?>">
 <input type="hidden" name="id" value="<?php echo $priority->getId(); ?>">
 <h2>Priority</h2>
 <table class="form_table" width="1280" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4><?php echo $title; ?></h4>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="180" class="required">
              Name:
            </td>
            <td>
                <input type="text" size="30" name="priority" value="<?php echo $priority->getTag(); ?>">
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['priority']; ?></span>
            </td>
        </tr>
		<tr>
            <td width="180" class="required">
              Description:
            </td>
            <td>
                <input type="text" size="30" name="priority_desc" value="<?php echo $priority->getDesc(); ?>">
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['priority_desc']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
              Time (hrs):
            </td>
            <td>
                <input type="text" size="30" name="priority_time" value="<?php echo $priority->getTime(); ?>">
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['priority_time']; ?></span>
            </td>
        </tr>
		<tr>
            <td width="180" class="required">
              Color:
            </td>
            <td>
                <select id="priority_color" name="priority_color">
					<option value="#FFFFF0" <?php if($priority->getColor() == "#FFFFF0"){echo "selected='selected'";} ?>>White</option>
					<option value="#DDFFDD" <?php if($priority->getColor() == "#DDFFDD"){echo "selected='selected'";} ?>>Calm Green</option>
					<option value="#FEE7E7" <?php if($priority->getColor() == "#FEE7E7"){echo "selected='selected'";} ?>>Warm Red</option>
				</select>
				&nbsp;<span class="error">*&nbsp;<?php echo $errors['priority_color']; ?></span>
            </td>
        </tr>
		<tr>
            <td width="180" class="required">
              Priority Urgency:
            </td>
            <td>
                <input type="text" size="30" name="priority_urgency" value="<?php echo $priority->getUrgency(); ?>">
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['priority_urgency']; ?></span>
            </td>
        </tr>
    </tbody>
</table>
<p style="padding-left:225px;">
    <input type="submit" name="submit" value="<?php echo $submit_text; ?>">
    <input type="reset"  name="reset"  value="Reset">
    <input type="button" name="cancel" value="Cancel" onclick='window.location.href="priority.php"'>
</p>
</form>
