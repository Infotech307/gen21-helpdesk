<?php
if(!defined('OSTSCPINC') || !$thisstaff || !@$thisstaff->isStaff()) die('Access Denied');
$qstr= '&'; 
$search=($_REQUEST['a']=='search');

//make sure the search query is 3 chars min...defaults to no query with warning message
if($search) {
  if( ($_REQUEST['query'] && strlen($_REQUEST['query'])<3) 
      || (!$_REQUEST['query'] && isset($_REQUEST['search'])) ){ //Why do I care about this crap...
      $search=false; //Instead of an error page...default back to regular query..with no search.
      $errors['err']='Search term must be more than 3 chars';
      $searchTerm='';
  }
}
 //query
   // if($searchTerm){
        //$qstr.='&query='.urlencode($searchTerm);
        //$queryterm=db_real_escape($searchTerm,false);
if(($_REQUEST['engineer'])) {
				$queryterm=db_real_escape($_REQUEST['engineer'],false); //escape the term ONLY...no quotes.
				$qwhere.=" where osts.staff_id = '$queryterm'";
				$qstr.='&engineer='.urlencode($_REQUEST['engineer']);
			} 
		 	$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?strtotime($_REQUEST['startDate']):0;
			$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?strtotime($_REQUEST['endDate']):0;
			if( ($startTime && $startTime>time()) or ($startTime>$endTime && $endTime>0)){
				$errors['err']='Entered date span is invalid. Selection ignored.';
				$startTime=$endTime=0;
			}else{
				//Have fun with dates.
				if($startTime){
					$qwhere2.=' and ost.created>=FROM_UNIXTIME('.$startTime.')';
					$qwhere1.=' and ost.created>=FROM_UNIXTIME('.$startTime.')';
					$qstr.='&startDate='.urlencode($_REQUEST['startDate']);
								
				}
				if($endTime){
					$qwhere2.=' and ost.created<=FROM_UNIXTIME('.$endTime.')';
					$qwhere1.=' and ost.created<=FROM_UNIXTIME('.$endTime.')';
					$qstr.='&endDate='.urlencode($_REQUEST['endDate']);
				}		   }
	
$sql='SELECT osts.firstname, osts.lastname, '
.' count(IF (ost.status= "closed", 1, NULL)) as jmlClosed,'
.' count(IF (ost.status= "open", 1, NULL)) as jmlOpen,'
.' count(IF (ost.status= "answered", 1, NULL)) as jmlAnswered,'
.' count(IF (ost.status= "assigned", 1, NULL)) as jmlAssigned'
.' FROM ost_staff osts '
.' LEFT JOIN ost_ticket ost '
.' ON (osts.staff_id = ost.staff_id) ' . $qwhere2;

$sortOptions=array('name'=>'firstname','eng'=>'firstname','open'=>'jmlOpen','progress'=>'jmlProgress','closed'=>'jmlClosed','answered' => 'jmlAnswered','assigned' => 'jmlAssigned');
$orderWays=array('DESC'=>'DESC','ASC'=>'ASC');
$sort=($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])])?strtolower($_REQUEST['sort']):'name';
//Sorting options...
if($sort && $sortOptions[$sort]) {
    $order_column =$sortOptions[$sort];
}
$order_column=$order_column?$order_column:'firstname';

if($_REQUEST['order'] && $orderWays[strtoupper($_REQUEST['order'])]) {
    $order=$orderWays[strtoupper($_REQUEST['order'])];
}
$order=$order?$order:'ASC';

if($order_column && strpos($order_column,',')){
    $order_column=str_replace(','," $order,",$order_column);
}
$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by="$order_column $order ";

$total=db_count('SELECT count(*) FROM '.STAFF_TABLE);
$page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
$pageNav=new Pagenate($total, $page, PAGE_LIMIT);
$pageNav->setURL('report.php',$qstr.'&sort='.urlencode($_REQUEST['sort']).'&order='.urlencode($_REQUEST['order']));
//Ok..lets roll...create the actual query
//$qstr.='&order='.($order=='DESC'?'ASC':'DESC');
$query="$sql $qwhere GROUP BY osts.staff_id ORDER BY $order_by LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();

//die($query);
$hash = md5($query);
$_SESSION['search_'.$hash] = $query;
$res=db_query($query);
if($res && ($num=db_num_rows($res)))
    $showing=$pageNav->showing().' report';
else
    $showing='No report found!';

if($search)
    $results_type.= ' (Search Results)';

$negorder=$order=='DESC'?'ASC':'DESC'; //Negate the sorting..

?>
<div style="width:700px;padding-top:5px; float:left;">
 <h2>Report</h2>
 </div>
<div class="clear"></div>
<form action="report.php" method="post" id="search" name="search">
  <input type="hidden" name="a" value="search">
  <?php csrf_token(); ?>
<table>
    
    <tr>
      <th>Start Date</th><td> : 
    <input class="dp" type="input" size="20" name="startDate" style="width:150px;">
            <span>Until</span>
            <input class="dp" type="input" size="20" name="endDate" style="width:150px;">
    </td></tr>
    <tr><th>Engineer</th><td>
      : <select id="engineer" name="engineer" style="width:350px;">
        <option value="" selected="selected">&mdash; SELECT &mdash;</option>
        
        <?php

									   
					   $sqll = mysql_query("select staff_id, firstname, lastname from ".TABLE_PREFIX."staff") or die ("not connect");
				
					   while ($r=mysql_fetch_array($sqll)){						  					
						 echo sprintf('<option value="'.$r[0] .'">'.$r[1] .' '. $r[2].'</option>');
						 } 
                 ?>
        </select>
    </td></tr>
    <tr><th colspan="2">
    		<span class="buttons">
                <input type="submit" value="Search">
                <input type="reset" value="Reset">
                <!--<input type="button" value="Cancel" class="close">-->
            </span>
    </th></tr>
    <tr><th></th><td></td></tr>
</table>
</form>
<form action="report.php" method="POST" name="report">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="mass_process">
<input type="hidden" id="action" name="a" value="">
 <table class="list" border="0" cellspacing="1" cellpadding="0" width="1280">
    <caption><?php echo $showing; ?></caption>
    <thead>
        <tr>
            <th width="20">&nbsp;</th>        
            <th width="203" style="text-align:center;"><a <?php echo $engineer_sort; ?> href="report.php?sort=eng&order=<?php echo $negorder; ?><?php echo $qstr; ?>" 
                  title="Sort By Subject <?php echo $negorder; ?>">Engineer</a></th>
            <th colspan="2" style="text-align:center;"><a <?php echo $open_sort; ?> href="report.php?sort=open&order=<?php echo $negorder; ?><?php echo $qstr; ?>" 
                  title="Sort By Subject <?php echo $negorder; ?>">Open</a></th>
            <th colspan="2" nowrap style="text-align:center;"><a <?php echo $progress_sort; ?> href="report.php?sort=answered&order=<?php echo $negorder; ?><?php echo $qstr; ?>" 
                  title="Sort By Subject <?php echo $negorder; ?>">Answered</a></th>
            <th colspan="2" nowrap style="text-align:center;"><a <?php echo $progress_sort; ?> href="report.php?sort=assigned&order=<?php echo $negorder; ?><?php echo $qstr; ?>" 
                  title="Sort By Subject <?php echo $negorder; ?>">Assigned</a></th>
            <th colspan="2" nowrap style="text-align:center;"><a <?php echo $closed_sort; ?> href="report.php?sort=closed&order=<?php echo $negorder; ?><?php echo $qstr; ?>" 
                  title="Sort By Subject <?php echo $negorder; ?>">Closed</a></th>
            <th width="99" nowrap style="text-align:center;">Total</th>
            <th width="90" nowrap>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
     <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>Quantity</td>
              <td>%</td>
              <td>Quantity</td>
              <td>%</td>
              <td>Quantity</td>
              <td>%</td>
              <td>Quantity</td>
              <td>%</td>
              <td>Quantity</td>
              <td>%</td>
            </tr>
    <?php
        $total=0;
        $ids=($errors && is_array($_POST['ids']))?$_POST['ids']:null;
        if($res && db_num_rows($res)):
			//total new
			$queryJmlOpen = 'SELECT count(ticket_id) as new, osts.firstname FROM ost_staff osts left join `ost_ticket` ost on ost.staff_id = osts.staff_id where status= "open" '.$qwhere1.'';
			$resJmlOpen=db_query($queryJmlOpen);
			$rowJmlOpen = db_fetch_array($resJmlOpen);
			$totalOpen = $rowJmlOpen['new'];
			
			//total answered
			$queryJmlActive = 'SELECT count(ticket_id) as active, osts.firstname FROM ost_staff osts left join `ost_ticket` ost on ost.staff_id = osts.staff_id where status= "answered" '.$qwhere1.'';
			$resJmlActive=db_query($queryJmlActive);
			$rowJmlActive = db_fetch_array($resJmlActive);
			$totalActive = $rowJmlActive['active'];
			
			//total assigned
			$queryJmlAss = 'SELECT count(ticket_id) as assigned, osts.firstname FROM ost_staff osts left join `ost_ticket` ost on ost.staff_id = osts.staff_id where status= "assigned" '.$qwhere1.'';
			$resJmlAss=db_query($queryJmlAss);
			$rowJmlAss = db_fetch_array($resJmlAss);
			$totalAss = $rowJmlAss['assigned'];
			
			//total Closed
			$queryJmlClosed = 'SELECT count(ticket_id) as closed, osts.firstname FROM ost_staff osts left join `ost_ticket` ost on ost.staff_id = osts.staff_id where status= "closed" '.$qwhere1.'';
			$resJmlClosed=db_query($queryJmlClosed);
			$rowJmlClosed = db_fetch_array($resJmlClosed);
			$totalClosed = $rowJmlClosed['closed'];
			
			
            while ($row = db_fetch_array($res)) {
                $sel=false;
				$persenTotal = $row['jmlOpen'] + $row['jmlAnswered'] + $row['jmlClosed'] + $row['jmlAssigned']; 
				$tot = $row['jmlOpen'] + $row['jmlAnswered'] + $row['jmlClosed'] + $row['jmlAssigned']; 
				
                if($ids && in_array($row['DivisionID'],$ids))
                    $sel=true;
                ?>
          
            <tr id="<?php echo $row['DivisionID']; ?>">
                <td width=20>
                 <!-- <input type="checkbox" class="ckb" name="ids[]" value="<?php //echo $row['DivisionID']; ?>" 
                            <?php //echo $sel?'checked="checked"':''; ?>>-->
                </td>
                <td><?php echo strtoupper($row['firstname']) ." ".strtoupper($row['lastname']); ?>&nbsp;</td>
                <td width="67"><?php echo $row['jmlOpen']; ?></td>
                <td width="51"><?php echo number_format(@($row['jmlOpen'] / $tot)*100, 2, '.', ' '); ?></td>
                <td width="105"><?php echo $row['jmlAnswered']; ?></td>
                <td width="91"><?php echo number_format(@($row['jmlAnswered'] / $tot) *100, 2, '.', ' '); ?></td>
                <td width="105"><?php echo $row['jmlAssigned']; ?></td>
                <td width="91"><?php echo number_format(@($row['jmlAssigned'] / $tot) *100, 2, '.', ' '); ?></td>
                <td width="105"><?php echo $row['jmlClosed']; ?></td>
                <td width="98"><?php echo number_format(@($row['jmlClosed'] / $tot) *100, 2, '.', ' '); ?></td>
                <td><?php echo $tot; ?></td>
                <td><?php echo  number_format(@($tot/ $persenTotal) * 100, 2, '.', ' '); ?></td>
            </tr> 
            <?php
			$no = $no +1;
			$total = $total + $tot;
            } ?>
            <tr>
              <td></td>
              <td>Total</td>
              <td><?php echo $totalOpen; ?></td>
              <td>&nbsp;</td>
              <td><?php echo $totalActive; ?></td>
               <td>&nbsp;</td>
              <td><?php echo $totalAss; ?></td>
              <td>&nbsp;</td>
              <td><?php echo $totalClosed; ?></td>
              <td>&nbsp;</td>
              <td><?php echo $total; ?></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td></td>
              <td>Average</td>
              <td><?php echo number_format($totalOpen / $no, 2, '.', ' '); ?></td>
              <td>&nbsp;</td>
              <td><?php echo number_format($totalActive / $no, 2, '.', ' '); ?></td>
              <td>&nbsp;</td>
              <td><?php echo number_format($totalAss / $no, 2, '.', ' '); ?></td>
              <td>&nbsp;</td>
              <td><?php echo number_format($totalClosed / $no, 2, '.', ' '); ?></td>
              <td>&nbsp;</td>
              <td><?php echo number_format($total/ $no, 2, '.', ' '); ?></td>
              <td>&nbsp;</td>
            </tr>
            <?php
           //end of while.
        endif; ?>
    <tfoot>
     <tr style="display:none;">
        <td colspan="12">
            <?php if($res && $num){ ?>
            Select:&nbsp;
            <a id="selectAll" href="#ckb">All</a>&nbsp;&nbsp;
            <a id="selectNone" href="#ckb">None</a>&nbsp;&nbsp;
            <a id="selectToggle" href="#ckb">Toggle</a>&nbsp;&nbsp;
            <?php }else{
                echo 'No reports found';
            } ?>
        </td>
     </tr>
    </tfoot>
</table>

<?php
if($res && $num): //Show options..
    echo '<div>&nbsp;Page:'.$pageNav->getPageLinks().'&nbsp;';
        echo '<a class="export-csv" href="?a=export&h='
            .$hash.'">Export</a></div>';
?>

<?php
endif;
?>
</form>
<div style="display:none;" class="dialog" id="confirm-action">
    <h3>Please Confirm</h3>
    <a class="close" href="">&times;</a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="enable-confirm">
        Are you sure want to <b>enable</b> selected reports?
    </p>
    <p class="confirm-action" style="display:none;" id="disable-confirm">
        Are you sure want to <b>disable</b>  selected reports?
    </p>
    <p class="confirm-action" style="display:none;" id="delete-confirm">
        <font color="red"><strong>Are you sure you want to DELETE selected reports?</strong></font>
        <br><br>Deleted reports CANNOT be recovered.
    </p>
    <div>Please confirm to continue.</div>
    <hr style="margin-top:1em"/>
    <p class="full-width">
        <span class="buttons" style="float:left">
            <input type="button" value="No, Cancel" class="close">
        </span>
        <span class="buttons" style="float:right">
            <input type="button" value="Yes, Do it!" class="confirm">
        </span>
     </p>
    <div class="clear"></div>
</div>

