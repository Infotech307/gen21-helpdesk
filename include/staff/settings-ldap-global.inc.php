<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');
$info=array();
$qstr='t='.$_REQUEST['t'];
    $title='Edit global LDAP Settings';
    $action='update';
    $submit_text='Save Changes';
	$sql='SELECT ldap_auth_var, ldap_client_active, ldap_client_force_ldaplogin,  ldap_client_autofill,  ldap_client_forcelogin, ldap_client_overwrite_email, ldap_use_sso, ldap_debug, maxlogs FROM `'. TABLE_PREFIX . 'ldap_globalconfig` WHERE 1';
	if(($res=db_query($sql)) && db_num_rows($res))
	{
		$info=db_fetch_array($res);
	}
    if($info['ldap_admin_pw'])
        $passwdtxt='To change password enter new password above.';
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);
?>
<h2>Global LDAP Settings</h2>
<form action="settings.php?<?php echo $qstr; ?>" method="post" id="save">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="<?php echo $action; ?>">
 <input type="hidden" name="a" value="<?php echo Format::htmlchars($_REQUEST['a']); ?>">
 <table class="form_table" width="1280" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4><?php echo $title; ?></h4>
                <em><strong>LDAP Settings</strong>:</em>
            </th>
        </tr>
    </thead>
    <tbody>
		<tr><td>PHP Server Auth Variable</td>
            <td><input type="text" name="ldap_auth_var" size=35 value="<?php echo $info['ldap_auth_var']; ?>"> <em>The Auth Variable that your webserver fills. Examples: 'PHP_AUTH_USER' or 'AUTH_USER'</em>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['ldap_auth_var']; ?></font>
            </td>
        </tr>
        <tr><td>Use SSO</td>
            <td>
                <label><input type="radio" name="ldap_use_sso"  value="1"   <?php echo $info['ldap_use_sso']?'checked="checked"':''; ?> /><strong>Enable</strong></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="ldap_use_sso"  value="0"   <?php echo !$info['ldap_use_sso']?'checked="checked"':''; ?> />Disable</label>
				<br><em>Enables/Disables Single Sign On query. You have to configure your Webserver to SSO first. Only the login.php needs to be authenticated.</em>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['ldap_use_sso']; ?></font>
            </td>
        </tr>
        <tr><td>Client Authentication via LDAP</td>
            <td>
                <label><input type="radio" name="ldap_client_active"  value="1"   <?php echo $info['ldap_client_active']?'checked="checked"':''; ?> /><strong>Enable</strong></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="ldap_client_active"  value="0"   <?php echo !$info['ldap_client_active']?'checked="checked"':''; ?> />Disable</label>
				<br><em>When active, Users on Clientside can Log In with their LDAP Credentials.</em>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['ldap_client_active']; ?></font>
            </td>
        </tr>
        <tr><td>Autofill Clients Ticketfields</td>
            <td>
                <label><input type="radio" name="ldap_client_autofill"  value="1"   <?php echo $info['ldap_client_autofill']?'checked="checked"':''; ?> /><strong>Enable</strong></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="ldap_client_autofill"  value="0"   <?php echo !$info['ldap_client_autofill']?'checked="checked"':''; ?> />Disable</label>
				<em>Fills the Name, Email and Phone Fields in the Create Ticket Form for the user. <b>"Client Authentication via LDAP" must be enabled</b></em>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['ldap_client_autofill']; ?></font>
            </td>
        </tr>
        <tr><td>Force Clients to Log In</td>
            <td>
                <label><input type="radio" name="ldap_client_forcelogin"  value="1"   <?php echo $info['ldap_client_forcelogin']?'checked="checked"':''; ?> /><strong>Enable</strong></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="ldap_client_forcelogin"  value="0"   <?php echo !$info['ldap_client_forcelogin']?'checked="checked"':''; ?> />Disable</label>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['ldap_client_forcelogin']; ?></font>
            </td>
        </tr>
        <tr><td>Force Clients to Log In with LDAP</td>
            <td>
                <label><input type="radio" name="ldap_client_force_ldaplogin"  value="1"   <?php echo $info['ldap_client_force_ldaplogin']?'checked="checked"':''; ?> /><strong>Enable</strong></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="ldap_client_force_ldaplogin"  value="0"   <?php echo !$info['ldap_client_force_ldaplogin']?'checked="checked"':''; ?> />Disable</label>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['ldap_client_force_ldaplogin']; ?></font>
            </td>
        </tr>
        <tr><td>Allow Clients to Overwrite their Email Address</td>
            <td>
                <label><input type="radio" name="ldap_client_overwrite_email"  value="1"   <?php echo $info['ldap_client_overwrite_email']?'checked="checked"':''; ?> /><strong>Enable</strong></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="ldap_client_overwrite_email"  value="0"   <?php echo !$info['ldap_client_overwrite_email']?'checked="checked"':''; ?> />Disable</label>
				<em>Allows Clients to overwrite their email address in a new ticket.<br><b>The new tickets with the different email count as a new user. The user won't be able to access those tickets with a ldap login, as long as it isn't his default email address! You have been warned!</b></em>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['ldap_client_overwrite_email']; ?></font>
            </td>
        </tr>
        <tr><td>LDAP Debug Logging</td>
            <td>
                <label><input type="radio" name="ldap_debug"  value="1"   <?php echo $info['ldap_debug']?'checked="checked"':''; ?> /><strong>Enable</strong></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="ldap_debug"  value="0"   <?php echo !$info['ldap_debug']?'checked="checked"':''; ?> />Disable</label>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['ldap_debug']; ?></font>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                Max Amount of Log Entries
            </td>
            <td>
                <input type="text" size="35" name="maxlogs" value="<?php echo $info['maxlogs']; ?>"> <em>Default: 2000"</em>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['maxlogs']; ?></span>
            </td>
        </tr>
    </tbody>
</table>
<p style="padding-left:225px;">
    <input type="submit" name="submit" value="<?php echo $submit_text; ?>">
    <input type="reset"  name="reset"  value="Reset">
    <input type="button" name="cancel" value="Cancel" onclick='window.location.href="settings.php?t=ldap"'>
</p>
</form>