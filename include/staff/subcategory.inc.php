<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');
$info=array();
$qstr='';
if($topic && $_REQUEST['a']!='add') {
    $title='Update Subcategory';
    $action='update';
    $submit_text='Save Changes';
    $info=$topic->getInfo();
    $info['id']=$topic->getId();
    $info['pid']=$topic->getPid();
    $info['groupclient'] = $topic->getGroupclientId();
    $info['client'] = $topic->getClientId();
    $qstr.='&id='.$topic->getId();
} else {
    $title='Add New Subcategory';
    $action='create';
    $submit_text='Add Subcategory';
    $info['isactive']=isset($info['isactive'])?$info['isactive']:1;
    $info['ispublic']=isset($info['ispublic'])?$info['ispublic']:1;
    $qstr.='&a='.$_REQUEST['a'];
}
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);
?>
<form action="helpsubcategory.php?<?php echo $qstr; ?>" method="post" id="save">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="<?php echo $action; ?>">
 <input type="hidden" name="a" value="<?php echo Format::htmlchars($_REQUEST['a']); ?>">
 <input type="hidden" name="id" value="<?php echo $info['id']; ?>">
 <h2>Subcategory</h2>
 <table class="form_table" width="1280" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4><?php echo $title; ?></h4>
                <em>Subcategory Information</em>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="180" class="required">
               Subcategory:
            </td>
            <td>
                <input type="text" size="30" name="topic" value="<?php echo $info['topic']; ?>">
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['topic']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                Status:
            </td>
            <td>
                <input type="radio" name="isactive" value="1" <?php echo $info['isactive']?'checked="checked"':''; ?>>Active
                <input type="radio" name="isactive" value="0" <?php echo !$info['isactive']?'checked="checked"':''; ?>>Disabled
                &nbsp;<span class="error">*&nbsp;</span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                Type:
            </td>
            <td>
                <input type="radio" name="ispublic" value="1" <?php echo $info['ispublic']?'checked="checked"':''; ?>>Public
                <input type="radio" name="ispublic" value="0" <?php echo !$info['ispublic']?'checked="checked"':''; ?>>Private/Internal
                &nbsp;<span class="error">*&nbsp;</span>
            </td>
        </tr>
		<tr>
            <td width="180" class="required">
                Group Client:
            </td>
            <td>
                <select name="groupclient" id="groupclient">
                    <option value="">&mdash; Select Group Client &mdash;</option>
                    <?php
                    $sql='SELECT * FROM ost_groupclients WHERE status=0 '
                        .' ORDER by name';
                    if(($res=db_query($sql)) && db_num_rows($res)) {
                        while(list($id, $name)=db_fetch_row($res)) {
                            echo sprintf('<option value="%d|%s" %s>%s</option>',
                                    $id,$name, (($info['groupclient'] && $id==$info['groupclient'])?'selected="selected"':'') ,$name);
                        }
                    }
                    ?>
                </select> 
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['groupclient']; ?></span>
            </td>
        </tr>
		<tr>
            <td width="180" class="required">
                Client:
            </td>
            <td>
                <select name="client" id="client">
					<?php if($info['client_id']){ ?>
						<option value="">&mdash; Select Client &mdash;</option>
						<?php
						$sql='SELECT * FROM ost_clients WHERE status=0 and groupclients_id = '.$info['groupclient']
							.' ORDER by name';
						if(($res=db_query($sql)) && db_num_rows($res)) {
							while(list($id, $name)=db_fetch_row($res)) {
								echo sprintf('<option value="%d|%s" %s>%s</option>',
										$id,$name, (($info['client'] && $id==$info['client'])?'selected="selected"':'') ,$name);
							}
						}
						?>
					<?php } ?>
				</select> 
                <span class="error">*&nbsp;<?php echo $errors['client']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">
                Category: 
            </td>
            <td>
                <select id="pid" name="pid">
					<?php if($info['pid']){ ?>
						<option value="">&mdash; Select Category &mdash;</option>
						<?php
						$sql='SELECT topic_id,topic from ost_help_topic where topic_pid = 0 and client_id = '.$info['client'].' order by topic';
						echo $sql;
						if(($res=db_query($sql)) && db_num_rows($res)) {
							while(list($id, $name)=db_fetch_row($res)) {
								echo sprintf('<option value="%d" %s>%s</option>',
										$id, (($info['pid'] && $id==$info['pid'])?'selected="selected"':'') ,$name);
							}
						}
						?>
					<?php } ?>
				</select>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['pid']; ?></span>
            </td>
        </tr>

        <tr><th colspan="2"><em>New ticket options</em></th></tr>
		<input type="hidden" name="priority_id" value="2" />
        <!-- <tr>
            <td width="180" class="required">
                Priority:
            </td>
            <td>
                <select name="priority_id">
                    <option value="">&mdash; Select Priority &mdash;</option>
                    <?php
                    $sql='SELECT priority_id,priority_desc FROM '.PRIORITY_TABLE.' pri ORDER by priority_urgency DESC';
                    if(($res=db_query($sql)) && db_num_rows($res)){
                        while(list($id,$name)=db_fetch_row($res)){
                            $selected=($info['priority_id'] && $id==$info['priority_id'])?'selected="selected"':'';
                            echo sprintf('<option value="%d" %s>%s</option>',$id,$selected,$name);
                        }
                    }
                    ?>
                </select>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['priority_id']; ?></span>
            </td>
        </tr> -->
        <tr>
            <td width="180" class="required">
                Department:
            </td>
            <td>
                <select name="dept_id">
                    <option value="">&mdash; Select Department &mdash;</option>
                    <?php
                    $sql='SELECT dept_id,dept_name FROM '.DEPT_TABLE.' dept ORDER by dept_name';
                    if(($res=db_query($sql)) && db_num_rows($res)){
                        while(list($id,$name)=db_fetch_row($res)){
                            $selected=($info['dept_id'] && $id==$info['dept_id'])?'selected="selected"':'';
                            echo sprintf('<option value="%d" %s>%s</option>',$id,$selected,$name);
                        }
                    }
                    ?>
                </select>
                &nbsp;<span class="error">*&nbsp;<?php echo $errors['dept_id']; ?></span>
            </td>
        </tr>
        <!-- <tr>
            <td width="180">
                SLA Plan:
            </td>
            <td>
                <select name="sla_id">
                    <option value="0">&mdash; Department's Default &mdash;</option>
                    <?php
                    if($slas=SLA::getSLAs()) {
                        foreach($slas as $id =>$name) {
                            echo sprintf('<option value="%d" %s>%s</option>',
                                    $id, ($info['sla_id']==$id)?'selected="selected"':'',$name);
                        }
                    }
                    ?>
                </select>
                &nbsp;<span class="error">&nbsp;<?php echo $errors['sla_id']; ?></span>
                <em>(Overrides department's SLA)</em>
            </td>
        </tr> -->
        <!-- <tr>
            <td width="180">Thank-you Page:</td>
            <td>
                <select name="page_id">
                    <option value="">&mdash; System Default &mdash;</option>
                    <?php
                    if(($pages = Page::getActiveThankYouPages())) {
                        foreach($pages as $page) {
                            if(strcasecmp($page->getType(), 'thank-you')) continue;
                            echo sprintf('<option value="%d" %s>%s</option>',
                                    $page->getId(),
                                    ($info['page_id']==$page->getId())?'selected="selected"':'',
                                    $page->getName());
                        }
                    }
                    ?>
                </select>&nbsp;<font class="error"><?php echo $errors['page_id']; ?></font>
                <em>(Overrides global setting. Applies to web tickets only.)</em>
            </td>
        </tr> -->
        <tr>
            <td width="180">
                Auto-assign To:
            </td>
            <td>
                <select name="assign">
                    <option value="0">&mdash; Unassigned &mdash;</option>
                    <?php
                    $sql=' SELECT staff_id,CONCAT_WS(", ",lastname,firstname) as name '.
                         ' FROM '.STAFF_TABLE.' WHERE isactive=1 ORDER BY name';
                    if(($res=db_query($sql)) && db_num_rows($res)){
                        echo '<OPTGROUP label="Staff Members">';
                        while (list($id,$name) = db_fetch_row($res)){
                            $k="s$id";
                            $selected = ($info['assign']==$k || $info['staff_id']==$id)?'selected="selected"':'';
                            ?>
                            <option value="<?php echo $k; ?>"<?php echo $selected; ?>><?php echo $name; ?></option>

                        <?php }
                        echo '</OPTGROUP>';
                    }
                    $sql='SELECT team_id, name FROM '.TEAM_TABLE.' WHERE isenabled=1';
                    if(($res=db_query($sql)) && db_num_rows($res)){
                        echo '<OPTGROUP label="Teams">';
                        while (list($id,$name) = db_fetch_row($res)){
                            $k="t$id";
                            $selected = ($info['assign']==$k || $info['team_id']==$id)?'selected="selected"':'';
                            ?>
                            <option value="<?php echo $k; ?>"<?php echo $selected; ?>><?php echo $name; ?></option>
                        <?php
                        }
                        echo '</OPTGROUP>';
                    }
                    ?>
                </select>
                &nbsp;<span class="error">&nbsp;<?php echo $errors['assign']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="180">
                Ticket auto-response:
            </td>
            <td>
                <input type="checkbox" name="noautoresp" value="1" <?php echo $info['noautoresp']?'checked="checked"':''; ?> >
                    <strong>Disable</strong> new ticket auto-response for
                    this subcategory (Overrides Dept. settings).
            </td>
        </tr>

        <tr>
            <th colspan="2">
                <em><strong>Admin Notes</strong>: Internal notes about the subcategory.&nbsp;</em>
            </th>
        </tr>
        <tr>
            <td colspan=2>
                <textarea name="notes" cols="21" rows="8" style="width: 80%;"><?php echo $info['notes']; ?></textarea>
            </td>
        </tr>
    </tbody>
</table>
<p style="padding-left:225px;">
    <input type="submit" name="submit" value="<?php echo $submit_text; ?>">
    <input type="reset"  name="reset"  value="Reset">
    <input type="button" name="cancel" value="Cancel" onclick='window.location.href="helpsubcategory.php"'>
</p>
</form>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
	jQuery(document).on("change", "#groupclient", function() {
		groupclient = this.value;
		jQuery("#client").empty();
		jQuery.ajax({
			type: "POST",
			url: "ajax/clients.php",
			data: ({groupclient: groupclient}),
			success: function(response){
				jQuery("#client").html(response);
			}
		});
	});
	
	jQuery(document).on("change", "#client", function() {
		groupclient = this.value;
		jQuery("#pid").empty();
		jQuery.ajax({
			type: "POST",
			url: "ajax/category.php",
			data: ({groupclient: groupclient}),
			success: function(response){
				jQuery("#pid").html(response);
			}
		});
	});
	
</script>