<?php
if(!defined('OSTSCPINC') || !$thisstaff || !$thisstaff->canCreateTickets()) die('Access Denied');
$info=array();
$info=Format::htmlchars(($errors && $_POST)?$_POST:$info);

include "ost-config.php";

mysql_connect(DBHOST,DBUSER,DBPASS);
mysql_select_db(DBNAME);
?>
<!--<link rel="stylesheet" href="../css/jquery-ui.css">
<script src="../js/jquery-ui-1.10.3.js"></script>
<script src="../js/autodropdown.js"></script>-->
<script>
function showSubCat(str)
{
if (str=="")
  {
  //document.getElementById("SubcategoryID").innerHTML="";
  document.getElementById("subcatwrap").innerHTML="<select id='SubcategoryID' name='Subcategory' style='width:250px;'><option value='' selected='selected'>&mdash; Select Subcategory &mdash;</option></select>";
  return;
  }
  else{
	document.getElementById("subcatwrap").innerHTML="";
  }
  
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("subcatwrap").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","../include/getSubCat.php?q="+str,true);
xmlhttp.send();
}

function getUserInfo(str)
{
	if(str==""){
		document.getElementById("userInfo").innerHTML="";
	  return;
	}
	
	if(window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("userInfo").innerHTML=xmlhttp.responseText;
		}
	}
xmlhttp.open("GET","../include/getUserInfo.php?q="+str,true);
xmlhttp.send();
}

function getSLA(str)
{
	if(str==""){
		document.getElementById("SLA").innerHTML="";
	  return;
	}
	
	if(window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("SLA").innerHTML=xmlhttp.responseText;
		}
	}
xmlhttp.open("GET","../include/getSLA.php?q="+str,true);
xmlhttp.send();
}

function loadJSON(str)
{
   var data_file = "../include/getUserInfos.php?q="+str;
   var http_request = new XMLHttpRequest();
   try{
      // Opera 8.0+, Firefox, Chrome, Safari
      http_request = new XMLHttpRequest();
   }catch (e){
      // Internet Explorer Browsers
      try{
         http_request = new ActiveXObject("Msxml2.XMLHTTP");
      }catch (e) {
         try{
            http_request = new ActiveXObject("Microsoft.XMLHTTP");
         }catch (e){
            // Something went wrong
            alert("Your browser broke!");
            return false;
         }
      }
   }
   http_request.onreadystatechange  = function(){
      if (http_request.readyState == 4  )
      {
        // Javascript function JSON.parse to parse JSON data
        var jsonObj = JSON.parse(http_request.responseText);

        // jsonObj variable now contains the data structure and can
        // be accessed as jsonObj.name and jsonObj.country.
		document.getElementById("name").value =  jsonObj.name;
		document.getElementById("phone").value =  jsonObj.phone;
		document.getElementById("phone_ext").value =  jsonObj.phone_ext;
		document.getElementById("DivisionName").value =  jsonObj.DivisionName;
		document.getElementById("division").value =  jsonObj.DivisionID;
		document.getElementById("locationname").value =  jsonObj.LocationName;
		document.getElementById("location").value =  jsonObj.LocationID;
		document.getElementById("designationname").value =  jsonObj.DesignationName;
		document.getElementById("designation").value =  jsonObj.DesignationID;
		document.getElementById("groupclient").value =  jsonObj.groupclients;
		loadClients(jsonObj.groupclients);
      }
   }
   http_request.open("GET", data_file, true);
   http_request.send();
}
$(document).ready(function() {
    $(".autosearch").searchable({
        maxListSize: 10000,                       // if list size are less than maxListSize, show them all
        maxMultiMatch: 100,                      // how many matching entries should be displayed
        exactMatch: false,                      // Exact matching on search
        wildcards: true,                        // Support for wildcard characters (*, ?)
        ignoreCase: true,                       // Ignore case sensitivity
        latency: 200,                           // how many millis to wait until starting search
        warnMultiMatch: 'Top {0} matches ...',  // string to append to a list of entries cut short by maxMultiMatch
        warnNoMatch: 'No matches ...',          // string to show in the list when no entries match
        zIndex: 'auto'                          // zIndex for elements generated by this plugin
    });
});   

function loadClients(g){
	groupclient = g;
	jQuery("#client").empty();	jQuery("#topicId").empty();	jQuery("#SubcategoryID").empty();
	jQuery.ajax({
		type: "POST",
		url: "ajax/clients.php",
		data: ({groupclient: groupclient}),
		success: function(response){
			jQuery("#client").html(response);
		}
	});
}
</script>
<script type="text/javascript" src="../js/jquery.searchabledropdown-1.0.8.min.js"></script>
<form action="tickets.php?a=open" method="post" id="save"  enctype="multipart/form-data">
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="create">
 <input type="hidden" name="a" value="open">
 <input type="hidden" name="groupclient" id="groupclient" />
 <h2>Open New Ticket</h2>
 <table class="form_table" width="1280" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4>New Ticket</h4>
                <em><strong>User Information</strong></em>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="160" class="required">
                Email Address:
            </td>
            <td>
				<select id="email" name="email" class="autosearch" onChange="loadJSON(this.value)" style="width:250px;">
					<option value="0" selected="selected">&nbsp;</option>
					<?php				   
						$sqlr = mysql_query("select email from ".TABLE_PREFIX."user where status = 0 order by email") or die ("not connect");
						
						while ($r=mysql_fetch_assoc($sqlr)){	
							if($r['email'] == $info['email']){
								echo "<option selected='selected' value='".$r['email']."'>".$r['email']."</option>";
							}
							else echo "<option value='".$r['email']."'>".$r['email']."</option>";
						} 
						
					?>
				</select> 
				<span class="error">*&nbsp;<?php echo $errors['email']; ?></span>
            <?php 
            if($cfg->notifyONNewStaffTicket()) { ?>
               &nbsp;&nbsp;&nbsp;
               <input type="checkbox" name="alertuser" <?php echo (!$errors || $info['alertuser'])? 'checked="checked"': ''; ?>>Send alert to user.
            <?php 
             } ?>
            </td>
        </tr>
		<div id="userinfos">
        <tr>
            <td width="160" class="required">
                Full Name:
            </td>
            <td>
                <input type="text" size="50" name="name" id="name" readonly="readonly" value="<?php echo $info['name']; ?>">
                &nbsp;<span class="error">&nbsp;<?php echo $errors['name']; ?></span>
            </td>
        </tr>
        <tr>
            <td width="160">
                Phone Number:
            </td>
            <td>
                <input type="text" size="20" name="phone" id="phone" readonly="readonly" value="<?php echo $info['phone']; ?>">
                &nbsp;<span class="error">&nbsp;<?php echo $errors['phone']; ?></span>
                Ext <input type="text" size="6" name="phone_ext" id="phone_ext" readonly="readonly" value="<?php echo $info['phone_ext']; ?>">
                &nbsp;<span class="error">&nbsp;<?php echo $errors['phone_ext']; ?></span>
            </td>
        </tr>
        <tr>
        <td class="required">Division:</td>
        <td><input type="text" id="DivisionName" name="DivisionName" readonly="readonly" value="<?php echo $info['DivisionName']; ?>" />
			<input type="hidden" id="division" name="division" value="<?php echo $info['division']; ?>" />			
           <span class="error">&nbsp;<?php echo $errors['division']; ?></span>
        </td>
    </tr>        <tr>
        <td class="required">Location:</td>
        <td><input type="text" id="locationname" name="locationname" readonly="readonly" value="<?php echo $info['locationname']; ?>" />
			<input type="hidden" id="location" name="location" value="<?php echo $info['location']; ?>" />
          <span class="error">&nbsp;<?php echo $errors['location']; ?></span>
        </td>
    </tr>        <tr>
        <td class="required">Designation:</td>
        <td><input type="text" id="designationname" name="designationname" readonly="readonly" value="<?php echo $info['designationname']; ?>" />
			<input type="hidden" id="designation" name="designation" value="<?php echo $info['designation']; ?>" />
            <span class="error">&nbsp;<?php echo $errors['designation']; ?></span>
			
        </td>
        </tr>
		</div>
        <tr>
            <th colspan="2">
                <em><strong>Ticket Information &amp; Options</strong>:</em>
            </th>
        </tr>
        <tr><td>Issue Reported For:</td>
    	<td>
            <select id="reportedfor" name="reportedfor" class="autosearch" onChange="getUserInfo(this.value)" style="width:250px;">
                <option value="" selected="selected">&nbsp;</option>
                <?php				   
					$sqlr = mysql_query("select * from ".TABLE_PREFIX."user where status = 0 order by name") or die ("not connect");
					
					while ($r=mysql_fetch_array($sqlr)){	
						if($r[2] == $info['reportedfor']){
							echo "<option selected='selected' value='".$r[2]."'>".$r[1]."</option>";
						}
						else echo "<option value='".$r[2]."'>".$r[1]."</option>";
					} 
					
				?>
            </select>
			<div id="userInfo"></div></td>
    </tr>
	
        <tr>
            <td width="160" class="required">
                Ticket Source:
            </td>
            <td>
                <select name="source">
                    <option value="" selected >&mdash; Select Source &mdash;</option>
                    <option value="Phone" <?php echo ($info['source']=='Phone')?'selected="selected"':''; ?>>Phone</option>
                    <option value="Email" <?php echo ($info['source']=='Email')?'selected="selected"':''; ?>>Email</option>
                    <option value="Other" <?php echo ($info['source']=='Other')?'selected="selected"':''; ?>>Other</option>
                </select>
                &nbsp;<font class="error"><b>*</b>&nbsp;<?php echo $errors['source']; ?></font>
            </td>
        </tr>
        <tr>
            <td width="160" class="required">
                Department:
            </td>
            <td>
                <select name="deptId">
                    <option value="" selected >&mdash; Select Department &mdash;</option>
                    <?php
                    if($depts=Dept::getDepartments()) {
                        foreach($depts as $id =>$name) {
                            echo sprintf('<option value="%d" %s>%s</option>',
                                    $id, ($info['deptId']==$id)?'selected="selected"':'',$name);
                        }
                    }
                    ?>
                </select>
                &nbsp;<font class="error"><b>*</b>&nbsp;<?php echo $errors['deptId']; ?></font>
            </td>
        </tr>
	<tr>
		<td class="required">Client:</td>
		<td>
		<?php
			$client_id= $info['client_id']; ?>
			<select id="client" name="client">
				<option value="">&mdash; Select a Client &mdash;</option>
			</select>
		<font class="error">*&nbsp;<?php echo $errors['client']; ?></font>
        </td>
    </tr>
    <tr>
		<td class="required">Category:</td>
		<td>
		<?php
				$idds= $info['topicId']; ?>
			<select id="topicId" name="topicId" onchange="showSubCat(this.value)">
				<option value="">&mdash; Select a Category &mdash;</option>
				<?php
					   $sqll = mysql_query("select * from ".TOPIC_TABLE." where topic_pid=0 order by topic") or die ("not connect");
					   while ($r=mysql_fetch_row($sqll)){?>
						<?php if ($r[0] == $idds){?>
						<option value="<?php echo $r[0]; ?>" selected="selected"> <?php echo $r[11]; ?></option>
						<?php } else { ?>
						<option value="<?php echo $r[0]; ?>"> <?php echo $r[11]; ?></option>
						<?php } } ?>
			</select>
		<font class="error">*&nbsp;<?php echo $errors['topicId']; ?></font>
        </td>
    </tr>

    <tr>
        <td class="required">Subcategory:</td>
        <td><div id="subcatwrap">
            <select id="SubcategoryID" name="Subcategory" style="width:250px;">
                <option value="" selected="selected">&mdash; Select a Subcategory &mdash;</option>
				<?php if($idds != "" || $idds != 0){
				 $sql2 = mysql_query("select * from ".TOPIC_TABLE." where topic_pid=".$idds." order by topic") or die ("not connect");
					   while ($r1=mysql_fetch_row($sql2)){?>
						<?php if ($r1[0] == $info['Subcategory']){?>
						<option value="<?php echo $r1[0]; ?>" selected="selected"> <?php echo $r1[11]; ?></option>
						<?php } else { ?>
						<option value="<?php echo $r1[0]; ?>"> <?php echo $r1[11]; ?></option>
						<?php } } ?>
				<?php } ?>
            </select>
        </div></td>
    </tr>
	<tr>
		<td class="required">Issue Type:</td>
		<td>
		<?php $issID = $info['issuetype']; ?>
			<select id="issuetype" name="issuetype">
				<option value="">&mdash; Select Issue Type &mdash;</option>
				<?php
					   $sqll = mysql_query("select * from ".ISSUETYPE_TABLE." where isactive = 1 order by issueTypeName") or die ("not connect");
					   while ($r=mysql_fetch_row($sqll)){?>
						<?php if ($r[0] == $issID){?>
						<option value="<?php echo $r[0]; ?>" selected="selected"> <?php echo $r[1]; ?></option>
						<?php } else { ?>
						<option value="<?php echo $r[0]; ?>"> <?php echo $r[1]; ?></option>
						<?php } } ?>
			</select>
		<font class="error">*&nbsp;<?php echo $errors['issuetype']; ?></font>
        </td>
    </tr>
        <tr>
            <td width="160">
                Priority:
            </td>
            <td>
                <select name="priorityId" onchange="getSLA(this.value)">
                    <option value="0" selected >&mdash; System Default &mdash;</option>
                    <?php
                    if($priorities=Priority::getPriorities()) {
                        foreach($priorities as $id =>$name) {
                            echo sprintf('<option value="%d" %s>%s</option>',
                                    $id, ($info['priorityId']==$id)?'selected="selected"':'',$name);
                        }
                    }
                    ?>
                </select>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['priorityId']; ?></font>
				<div id="SLA"></div>
            </td>
         </tr>
         <!-- <tr>
            <td width="160">
                SLA Plan:
            </td>
            <td>
                <select name="slaId">
                    <option value="0" selected="selected" >&mdash; System Default &mdash;</option>
                    <?php
                    if($slas=SLA::getSLAs()) {
                        foreach($slas as $id =>$name) {
                            echo sprintf('<option value="%d" %s>%s</option>',
                                    $id, ($info['slaId']==$id)?'selected="selected"':'',$name);
                        }
                    }
                    ?>
                </select>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['slaId']; ?></font>
            </td>
         </tr> -->

         <!--<tr>
            <td width="160">
                Due Date:
            </td>
            <td>
                <input class="dp" id="duedate" name="duedate" value="<?php echo Format::htmlchars($info['duedate']); ?>" size="12" autocomplete=OFF>
                &nbsp;&nbsp;
                <?php
                $min=$hr=null;
                if($info['time'])
                    list($hr, $min)=explode(':', $info['time']);

                echo Misc::timeDropdown($hr, $min, 'time');
                ?>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['duedate']; ?> &nbsp; <?php echo $errors['time']; ?></font>
                <em>Time is based on your time zone (GMT <?php echo $thisstaff->getTZoffset(); ?>)</em>
            </td>
        </tr> -->

        <?php
        if($thisstaff->canAssignTickets()) { ?>
        <tr>
            <td width="160">Assign To:</td>
            <td>
                <select id="assignId" name="assignId">
                    <option value="0" selected="selected">&mdash; Select Staff Member OR a Team &mdash;</option>
                    <?php
                    if(($users=Staff::getAvailableStaffMembers())) {
                        echo '<OPTGROUP label="Staff Members ('.count($users).')">';
                        foreach($users as $id => $name) {
                            $k="s$id";
                            echo sprintf('<option value="%s" %s>%s</option>',
                                        $k,(($info['assignId']==$k)?'selected="selected"':''),$name);
                        }
                        echo '</OPTGROUP>';
                    }

                    if(($teams=Team::getActiveTeams())) {
                        echo '<OPTGROUP label="Teams ('.count($teams).')">';
                        foreach($teams as $id => $name) {
                            $k="t$id";
                            echo sprintf('<option value="%s" %s>%s</option>',
                                        $k,(($info['assignId']==$k)?'selected="selected"':''),$name);
                        }
                        echo '</OPTGROUP>';
                    }
                    ?>
                </select>&nbsp;<span class='error'>&nbsp;<?php echo $errors['assignId']; ?></span>
            </td>
        </tr>
        <?php
        } ?>
        <tr>
            <th colspan="2">
                <em><strong>Issue</strong>: The user will be able to see the issue summary below and any associated responses.</em>
            </th>
        </tr>
        <tr>
            <td colspan=2>
                <div>
                    <em><strong>Subject</strong>: Issue summary </em> &nbsp;<font class="error">*&nbsp;<?php echo $errors['subject']; ?></font><br>
                    <input type="text" name="subject" size="60" value="<?php echo $info['subject']; ?>">
                </div>
                <div><em><strong>Issue</strong>: Details on the reason(s) for opening the ticket.</em> <font class="error">*&nbsp;<?php echo $errors['issue']; ?></font></div>
                <textarea name="issue" cols="21" rows="8" style="width:80%;"><?php echo $info['issue']; ?></textarea>
            </td>
        </tr>
        <?php
        //is the user allowed to post replies??
        if($thisstaff->canPostReply()) {
            ?>
        <tr>
            <th colspan="2">
                <em><strong>Response</strong>: Optional response to the above issue.</em>
            </th>
        </tr>
        <tr>
            <td colspan=2>
            <?php
            if(($cannedResponses=Canned::getCannedResponses())) {
                ?>
                <div>
                    Canned Response:&nbsp;
                    <select id="cannedResp" name="cannedResp">
                        <option value="0" selected="selected">&mdash; Select a canned response &mdash;</option>
                        <?php
                        foreach($cannedResponses as $id =>$title) {
                            echo sprintf('<option value="%d">%s</option>',$id,$title);
                        }
                        ?>
                    </select>
                    &nbsp;&nbsp;&nbsp;
                    <label><input type='checkbox' value='1' name="append" id="append" checked="checked">Append</label>
                </div>
            <?php
            } ?>
                <textarea name="response" id="response" cols="21" rows="8" style="width:80%;"><?php echo $info['response']; ?></textarea>
                <table border="0" cellspacing="0" cellpadding="2" width="100%">
                <?php
                if($cfg->allowAttachments()) { ?>
                    <tr><td width="100" valign="top">Attachments:</td>
                        <td>
                            <div class="canned_attachments">
                            <?php
                            if($info['cannedattachments']) {
                                foreach($info['cannedattachments'] as $k=>$id) {
                                    if(!($file=AttachmentFile::lookup($id))) continue;
                                    $hash=$file->getHash().md5($file->getId().session_id().$file->getHash());
                                    echo sprintf('<label><input type="checkbox" name="cannedattachments[]" 
                                            id="f%d" value="%d" checked="checked" 
                                            <a href="file.php?h=%s">%s</a>&nbsp;&nbsp;</label>&nbsp;',
                                            $file->getId(), $file->getId() , $hash, $file->getName());
                                }
                            }
                            ?>
                            </div>
                            <div class="uploads"></div>
                            <div class="file_input">
                                <input type="file" class="multifile" name="attachments[]" size="30" value="" />
                            </div>
                        </td>
                    </tr>
                <?php
                } ?>

            <?php
            if($thisstaff->canCloseTickets()) { ?>
                <tr>
                    <td width="100">Ticket Status:</td>
                    <td>
                        <input type="checkbox" name="ticket_state" value="closed" <?php echo $info['ticket_state']?'checked="checked"':''; ?>>
                        <b>Close On Response</b>&nbsp;<em>(Only applicable if response is entered)</em>
                    </td>
                </tr>
            <?php
            } ?>
             <tr>
                <td width="100">Signature:</td>
                <td>
                    <?php
                    $info['signature']=$info['signature']?$info['signature']:$thisstaff->getDefaultSignatureType();
                    ?>
                    <label><input type="radio" name="signature" value="none" checked="checked"> None</label>
                    <?php
                    if($thisstaff->getSignature()) { ?>
                        <label><input type="radio" name="signature" value="mine"
                            <?php echo ($info['signature']=='mine')?'checked="checked"':''; ?>> My signature</label>
                    <?php
                    } ?>
                    <label><input type="radio" name="signature" value="dept"
                        <?php echo ($info['signature']=='dept')?'checked="checked"':''; ?>> Dept. Signature (if set)</label>
                </td>
             </tr>
            </table>
            </td>
        </tr>
        <?php
        } //end canPostReply
        ?>
        <tr>
            <th colspan="2">
                <em><strong>Internal Note</strong>: Optional internal note (recommended on assignment) <font class="error">&nbsp;<?php echo $errors['note']; ?></font></em>
            </th>
        </tr>
        <tr>
            <td colspan=2>
                <textarea name="note" cols="21" rows="6" style="width:80%;"><?php echo $info['note']; ?></textarea>
            </td>
        </tr>
    </tbody>
</table>
<p style="padding-left:250px;">
    <input type="submit" name="submit" value="Open">
    <input type="reset"  name="reset"  value="Reset">
    <input type="button" name="cancel" value="Cancel" onclick='window.location.href="tickets.php"'>
</p>
</form>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript">	
	jQuery(document).on("change", "#client", function() {
		groupclient = this.value;
		jQuery("#topicId").empty();
		jQuery.ajax({
			type: "POST",
			url: "ajax/category.php",
			data: ({groupclient: groupclient}),
			success: function(response){
				jQuery("#topicId").html(response);
			}
		});
	});
	
	jQuery(document).on("change", "#topicId", function() {
		category = this.value;
		jQuery("#SubcategoryID").empty();
		jQuery.ajax({
			type: "POST",
			url: "ajax/subcategory.php",
			data: ({category: category}),
			success: function(response){
				jQuery("#SubcategoryID").html(response);
			}
		});
	});
</script>