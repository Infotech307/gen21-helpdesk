<?php
/*********************************************************************
    index.php

    Helpdesk landing page. Please customize it to fit your needs.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/ 

require('client.inc.php');
$section = 'home';

if(!$thisclient){
	if($cfg && ($page = $cfg->getLandingPage())){
		if(strtolower($page->getBody()) == "login"){
			header('Location: view.php');
		}
	}
}

require(CLIENTINC_DIR.'header.inc.php');
//require_once(INCLUDE_DIR.'class.ldap.php');
//global $ldapcon;
/*if($ldapcon->ldapActive())
{
	if($ldapcon->ldapClientForceLogin())
	{
		if(!$thisclient)
		{
			//XXX: Ticket owner is assumed.
			@header('Location: login.php');
			require_once('login.php'); //Just in case of 'header already sent' error.
			exit;
		}
	}
}*/
?>
<div id="landing_page">
    <?php
    if($cfg && ($page = $cfg->getLandingPage())){
		echo $page->getBody();
	}
    else echo  '<h1>Welcome to the Support Center</h1>';
    ?>
    <div id="new_ticket" style="display:none;">
        <h3><?php //echo $ldapcon->ldapClientActive()?'Open Ticket as Guest':'Open A New Ticket';?>
		<?php if($thisclient){
				echo "Open a New Ticket";
				$msgtic = "Please provide as much detail as possible so we can best assist you";
			  }
			  else { echo "First Time User"; $msgtic = "If this is your first time contacting us, please register first"; }	  
		?>
		</h3>
        <br>
        <div><?php echo $msgtic; ?></div>
        <p> 
		<?php if($thisclient){ ?>
			<a href="open.php" class="green button">Open a New Ticket</a>
		<?php } else { ?>
			<a href="register.php" class="green button">Register</a> 
		<?php } ?>
        </p>
    </div>

    <div id="check_status">
        <h3>
		<?php if($thisclient){
				echo "My Tickets";
			}
			else echo "Log In";
		?>
		</h3>
        <br>
        <div>We provide archives and history of all your current and past support requests complete with responses.</div>
        <p>
		<?php if($thisclient){
					$msgbut = "My Tickets";
				}
				else {
				$msgbut = "Log In";
			} ?>
            <a href="view.php" class="blue button"><?php echo $msgbut; ?></a>
        </p>
		
    </div>
</div>
<div class="clear"></div>
<?php
if($cfg && $cfg->isKnowledgebaseEnabled()){
    //FIXME: provide ability to feature or select random FAQs ??
?>
<p>Be sure to browse our <a href="kb/index.php">Frequently Asked Questions (FAQs)</a>, before opening a ticket.</p>
</div>
<?php
} ?>
<?php require(CLIENTINC_DIR.'footer.inc.php'); ?>
