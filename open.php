<?php
/*********************************************************************
    open.php

    New tickets handle.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('client.inc.php');
require_once(INCLUDE_DIR.'class.ldap.php');
define('SOURCE','Web'); //Ticket source.
$ticket = null;
$errors=array();
global $ldapcon;

if($ldapcon->ldapClientActive()){
	if($ldapcon->ldapActive())
	{
		if($ldapcon->ldapClientForceLogin())
		{
			if(!$thisclient)
			{
				//XXX: Ticket owner is assumed.
				@header('Location: login.php');
				require_once('login.php'); //Just in case of 'header already sent' error.
				exit;
			}
		}
	}
	else {
		@header('Location: login.php');
		require_once('login.php'); //Just in case of 'header already sent' error.
		exit;
	}
}
else if(!$thisclient){
	@header('Location: login.php');
	require_once('login.php'); //Just in case of 'header already sent' error.
	exit;
}

if($_POST):
    $vars = $_POST;
    $vars['deptId']=$vars['emailId']=0; //Just Making sure we don't accept crap...only topicId is expected.
    if($thisclient) {
		if($ldapcon->ldapClientAutofill())
		{
			$ldapUser=$ldapcon->ldapGetUsernameFromEmail($thisclient->getEmail());
			$vars['name']=$ldapcon->ldapGetName($ldapUser);		
		}
		else
		{
			$vars['name']=$thisclient->getName();
		}
		if(!$ldapcon->ldapClientOverwriteEmail())
		{
			$vars['email']=$thisclient->getEmail();
		}
    } elseif($cfg->isCaptchaEnabled()) {
        if(!$_POST['captcha'])
            $errors['captcha']='Enter text shown on the image';
        elseif(strcmp($_SESSION['captcha'],md5($_POST['captcha'])))
            $errors['captcha']='Invalid - try again!';
    }

    if(!$errors && $cfg->allowOnlineAttachments() && $_FILES['attachments'])
        $vars['files'] = AttachmentFile::format($_FILES['attachments'], true);

    //Ticket::create...checks for errors..
    if(($ticket=Ticket::create($vars, $errors, SOURCE))){
        $msg='Support ticket request created';
        //Logged in...simply view the newly created ticket.
        if($thisclient && $thisclient->isValid()) {
			/*if($ldapcon->ldapClientActive()==true)
			{
				$_SESSION['_client']['key']= $ticket->getExtId();
				$sqlquery='DELETE FROM '.TABLE_PREFIX . 'ticket WHERE email LIKE "'.$thisclient->getEmail().'" AND subject LIKE "ldap_temporary";';
				if(!db_query($sqlquery))
					$errors['err'] = 'Failed deleting a temporary ticket';
			}
            if(!$cfg->showRelatedTickets())
                $_SESSION['_client']['key']= $ticket->getExtId(); //Resetting login Key to the current ticket!
            session_write_close();
            session_regenerate_id();*/
            @header('Location: tickets.php?id='.$ticket->getExtId());
        }
    }else{
        $errors['err']=$errors['err']?$errors['err']:'Unable to create a ticket. Please correct errors below and try again!';
    }
endif;

//page
$nav->setActiveNav('new');
require(CLIENTINC_DIR.'header.inc.php');
if($ticket
        && (
            (($topic = $ticket->getTopic()) && ($page = $topic->getPage()))
            || ($page = $cfg->getThankYouPage())
            )) { //Thank the user and promise speedy resolution!
    //Hide ticket number -  it should only be delivered via email for security reasons.
    echo Format::safe_html($ticket->replaceVars(str_replace(
                    array('%{ticket.number}', '%{ticket.extId}', '%{ticket}'), //ticket number vars.
                    array_fill(0, 3, 'XXXXXX'),
                    $page->getBody()
                    )));
} else {
    require(CLIENTINC_DIR.'open.inc.php');
}
require(CLIENTINC_DIR.'footer.inc.php');
?>
