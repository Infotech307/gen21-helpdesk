<?php
require('client.inc.php');
require_once(INCLUDE_DIR.'class.user.php');
define('SOURCE','Web'); //User registration source.
$errors=array();

if($thisclient){
	@header('Location: index.php');
	require_once('index.php'); //Just in case of 'header already sent' error.
	exit;
}

if($_POST):
    $vars = $_POST;
	
    if($cfg->isCaptchaEnabled()) {
        if(!$_POST['captcha'])
            $errors['captcha']='Enter text shown on the image';
        elseif(strcmp($_SESSION['captcha'],md5($_POST['captcha'])))
            $errors['captcha']='Invalid - try again!';
    }
    //User::create...checks for errors..
    if(($u=User::create($vars, $errors, SOURCE))){
        $msg='Please check your email and find your password there';
        //Logged in...simply view the newly created ticket.
        @header('Location: login.php?msg='.$msg.'');
    }else{
        $errors['err']=$errors['err']?$errors['err']:'Unable to register user. Please correct errors below and try again!';	
    }
	
endif;

//page
$nav->setActiveNav('new');
require(CLIENTINC_DIR.'header.inc.php');
require(CLIENTINC_DIR.'register.inc.php');
require(CLIENTINC_DIR.'footer.inc.php');
?>
