<?php
/*********************************************************************
    staff.php

    Evertything about staff members.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('admin.inc.php');
require_once(INCLUDE_DIR.'class.user.php');
require_once(INCLUDE_DIR.'class.client.php');
$client=null;
if($_REQUEST['id'] && !($client=Client::lookup($_REQUEST['id'])))
    $errors['err']='Unknown or invalid client member ID.';

if($_POST){
    switch(strtolower($_POST['do'])){
        case 'update':
            if(!$client){
                $errors['err']='Unknown or invalid client member.';
            }elseif($id=User::update($_POST, $errors)){
                $msg='client member updated successfully';
				$client=null;
            }elseif(!$errors['err']){
                $errors['err']='Unable to update client member. Correct any error(s) below and try again!';
            }
            break;
        case 'create':
            if(($id=User::create($_POST, $errors))){
                $msg=Format::htmlchars($_POST['name']).' added successfully';
                $_REQUEST['a']=null;
            }elseif(!$errors['err']){
				
                $errors['err']='Unable to add client member. Correct any error(s) below and try again.';
            }
            break;
        case 'mass_process':
            if(!$_POST['ids'] || !is_array($_POST['ids']) || !count($_POST['ids'])) {
                $errors['err'] = 'You must select at least one client member.';
            }  else {
                $count=count($_POST['ids']);
                switch(strtolower($_POST['a'])) {
                    case 'enable':
                        $sql='UPDATE ost_user SET status=0 '
                            .' WHERE user_id IN ('.implode(',', db_input($_POST['ids'])).')';

                        if(db_query($sql) && ($num=db_affected_rows())) {
                            if($num==$count)
                                $msg = 'Selected client member activated';
                            else
                                $warn = "$num of $count selected client member activated";
                        } else {
                            $errors['err'] = 'Unable to activate selected client member';
                        }
                        break;
                    case 'disable':
                        $sql='UPDATE ost_user SET status=1 '
                            .' WHERE user_id IN ('.implode(',', db_input($_POST['ids'])).')';

                        if(db_query($sql) && ($num=db_affected_rows())) {
                            if($num==$count)
                                $msg = 'Selected client member disabled';
                            else
                                $warn = "$num of $count selected client member disabled";
                        } else {
                            $errors['err'] = 'Unable to disable selected client member';
                        }
                        break;
                    case 'delete':
                        foreach($_POST['ids'] as $k=>$v) {
							$sql='DELETE FROM ost_user WHERE user_id='.db_input($v).' LIMIT 1';
							if(db_query($sql)) {
								$i++;
							}
                        }

                        if($i && $i==$count)
                            $msg = 'Selected client member deleted successfully';
                        elseif($i>0)
                            $warn = "$i of $count selected client member deleted";
                        elseif(!$errors['err'])
                            $errors['err'] = 'Unable to delete selected client member.';
                        break;
                    default:
                        $errors['err'] = 'Unknown action. Get technical help!';
                }
                    
            }
            break;
        default:
            $errors['err']='Unknown action/command';
            break;
    }
}

$page='clientmembers.inc.php';
if($client || ($_REQUEST['a'] && !strcasecmp($_REQUEST['a'],'add')))
    $page='clientmember.inc.php';

$nav->setTabActive('client');
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>
