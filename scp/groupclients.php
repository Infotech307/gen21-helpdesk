<?php
/*********************************************************************
    staff.php

    Evertything about staff members.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('admin.inc.php');
require_once(INCLUDE_DIR.'class.groupclients.php');
$groupclients=null;
if($_REQUEST['id'] && !($groupclients=Groupclients::lookup($_REQUEST['id'])))
    $errors['err']='Unknown or invalid client ID.';

if($_POST){
    switch(strtolower($_POST['do'])){
        case 'update':
            if(!$groupclients){
                $errors['err']='Unknown or invalid client.';
            }elseif($groupclients->update($_POST,$errors)){
                $msg='Client updated successfully';
            }elseif(!$errors['err']){
                $errors['err']='Unable to update client. Correct any error(s) below and try again!';
            }
            break;
        case 'create':
            if(($id=Groupclients::create($_POST, $errors))){
                $msg=Format::htmlchars($_POST['name']).' added successfully';
                $_REQUEST['a']=null;
            }elseif(!$errors['err']){
                $errors['err']='Unable to add group client. Correct any error(s) below and try again.';
            }
            break;
        case 'mass_process':
            if(!$_POST['ids'] || !is_array($_POST['ids']) || !count($_POST['ids'])) {
                $errors['err'] = 'You must select at least one group client.';
            }  else {
                $count=count($_POST['ids']);
                switch(strtolower($_POST['a'])) {
                    case 'enable':
                        $sql='UPDATE ost_groupclients SET status=0 WHERE id IN ('.implode(',', db_input($_POST['ids'])).')';
						
                        if(db_query($sql) && ($num=db_affected_rows())) {
                            if($num==$count)
                                $msg = 'Selected group client enabled';
                            else
                                $warn = "$num of $count selected group client enabled";
                        } else {
                            $errors['err'] = 'Unable to enabled selected group client';
                        }
                        break;
                    case 'disable':
                        $sql='UPDATE ost_groupclients SET status=1 WHERE id IN ('.implode(',', db_input($_POST['ids'])).')';

                        if(db_query($sql) && ($num=db_affected_rows())) {
                            if($num==$count)
                                $msg = 'Selected group client disabled';
                            else
                                $warn = "$num of $count selected group client disabled";
                        } else {
                            $errors['err'] = 'Unable to disable selected group client';
                        }
                        break;
                    case 'delete':
                        foreach($_POST['ids'] as $k=>$v) {
							$sql='DELETE FROM ost_groupclients WHERE id='.db_input($v).' LIMIT 1';
							if(db_query($sql)) {
								$i++;
							}
                        }

                        if($i && $i==$count)
                            $msg = 'Selected group client deleted successfully';
                        elseif($i>0)
                            $warn = "$i of $count selected group client deleted";
                        elseif(!$errors['err'])
                            $errors['err'] = 'Unable to delete selected group client.';
                        break;
                    default:
                        $errors['err'] = 'Unknown action. Get technical help!';
                }
                    
            }
            break;
        default:
            $errors['err']='Unknown action/command';
            break;
    }
}

$page='groupclients.inc.php';
if($groupclients || ($_REQUEST['a'] && !strcasecmp($_REQUEST['a'],'add')))
    $page='grupclient.inc.php';

$nav->setTabActive('client');
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>
