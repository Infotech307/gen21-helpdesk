<?php
/*********************************************************************
    helptopics.php

    Help Topics.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('admin.inc.php');
include_once(INCLUDE_DIR.'class.subcategory.php');

$topic=null;
if($_REQUEST['id'] && !($topic=Subcategory::lookup($_REQUEST['id'])))
    $errors['err']='Unknown or invalid help topic ID.';

if($_POST){
    switch(strtolower($_POST['do'])){
        case 'update':
            if(!$topic){
                $errors['err']='Unknown or invalid sub category.';
            }elseif($topic->update($_POST,$errors)){
                $msg='Sub category updated successfully';
            }elseif(!$errors['err']){
                $errors['err']='Error updating sub category. Try again!';
            }
            break;
        case 'create':
            if(($id=Subcategory::create($_POST,$errors))){
                $msg='Sub category added successfully';
                $_REQUEST['a']=null;
            }elseif(!$errors['err']){
                $errors['err']='Unable to add sub category. Correct error(s) below and try again.';
            }
            break;
        case 'mass_process':
            if(!$_POST['ids'] || !is_array($_POST['ids']) || !count($_POST['ids'])) {
                $errors['err'] = 'You must select at least one sub category';
            } else {
                $count=count($_POST['ids']);

                switch(strtolower($_POST['a'])) {
                    case 'enable':
                        $sql='UPDATE '.TOPIC_TABLE.' SET isactive=1 '
                            .' WHERE topic_id IN ('.implode(',', db_input($_POST['ids'])).')';
                    
                        if(db_query($sql) && ($num=db_affected_rows())) {
                            if($num==$count)
                                $msg = 'Selected sub category enabled';
                            else
                                $warn = "$num of $count selected sub categories enabled";
                        } else {
                            $errors['err'] = 'Unable to enable selected sub categories.';
                        }
                        break;
                    case 'disable':
                        $sql='UPDATE '.TOPIC_TABLE.' SET isactive=0 '
                            .' WHERE topic_id IN ('.implode(',', db_input($_POST['ids'])).')';
                        if(db_query($sql) && ($num=db_affected_rows())) {
                            if($num==$count)
                                $msg = 'Selected sub categories disabled';
                            else
                                $warn = "$num of $count selected sub categories disabled";
                        } else {
                            $errors['err'] ='Unable to disable selected sub categor(y/ies)';
                        }
                        break;
                    case 'delete':
                        $i=0;
                        foreach($_POST['ids'] as $k=>$v) {
                            if(($t=Topic::lookup($v)) && $t->delete())
                                $i++;
                        }

                        if($i && $i==$count)
                            $msg = 'Selected sub categories deleted successfully';
                        elseif($i>0)
                            $warn = "$i of $count selected sub categories deleted";
                        elseif(!$errors['err'])
                            $errors['err']  = 'Unable to delete selected sub categories';

                        break;
                    default:
                        $errors['err']='Unknown action - get technical help.';
                }
            }
            break;
        default:
            $errors['err']='Unknown command/action';
            break;
    }
}

$page='subcategories.inc.php';
if($topic || ($_REQUEST['a'] && !strcasecmp($_REQUEST['a'],'add')))
    $page='subcategory.inc.php';

$nav->setTabActive('manage');
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>
