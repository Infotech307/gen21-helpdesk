<?php
require('admin.inc.php');
include_once(INCLUDE_DIR.'class.issuetype.php');

$div=null;
if($_REQUEST['id'] && !($div=Issuetype::lookup($_REQUEST['id'])))
    $errors['err']='Unknown or invalid issue type ID.';

if($_POST){
    switch(strtolower($_POST['do'])){
        case 'update':
            if(!$div){
                $errors['err']='Unknown or invalid issue type.';
            }elseif($div->update($_POST,$errors)){
                $msg='Issue type updated successfully';
            }elseif(!$errors['err']){
                $errors['err']='Error updating issue type. Try again!';
            }
            break;
        case 'create':
            if(($id=Issuetype::create($_POST,$errors))){
                $msg='Issue type added successfully';
                $_REQUEST['a']=null;
            }elseif(!$errors['err']){
                $errors['err']='Unable to add issue type. Correct error(s) below and try again.';
            }
            break;
        case 'mass_process':
            if(!$_POST['ids'] || !is_array($_POST['ids']) || !count($_POST['ids'])) {
                $errors['err'] = 'You must select at least one issue type';
            } else {
                $count=count($_POST['ids']);

                switch(strtolower($_POST['a'])) {
                    case 'enable':
                        $sql='UPDATE '.ISSUETYPE_TABLE.' SET isactive=1 '
                            .' WHERE issueTypeID IN ('.implode(',', db_input($_POST['ids'])).')';
                        if(db_query($sql) && ($num=db_affected_rows())) {
                            if($num==$count)
                                $msg = 'Selected issue type enabled';
                            else
                                $warn = "$num of $count selected issue type enabled";
                        } else {
                            $errors['err'] ='Unable to enable selected issue type(s)';
                        }
                        break;
                    case 'disable':
                        $sql='UPDATE '.ISSUETYPE_TABLE.' SET isactive=0 '
                            .' WHERE issueTypeID IN ('.implode(',', db_input($_POST['ids'])).')';
                        if(db_query($sql) && ($num=db_affected_rows())) {
                            if($num==$count)
                                $msg = 'Selected issue type disabled';
                            else
                                $warn = "$num of $count selected issue type disabled";
                        } else {
                            $errors['err'] ='Unable to disable selected issue type(s)';
                        }
                        break;
                    case 'delete':
                        $i=0;
                        foreach($_POST['ids'] as $k=>$v) {
                            if(($t=Issuetype::lookup($v)) && $t->delete())
                                $i++;
                        }

                        if($i && $i==$count)
                            $msg = 'Selected issue type deleted successfully';
                        elseif($i>0)
                            $warn = "$i of $count selected issue type deleted";
                        elseif(!$errors['err'])
                            $errors['err']  = 'Unable to delete selected issue type';

                        break;
                    default:
                        $errors['err']='Unknown action - get technical help.';
                }
            }
            break;
        default:
            $errors['err']='Unknown command/action';
            break;
    }
}

$page='issuetypes.inc.php';
if($div || ($_REQUEST['a'] && !strcasecmp($_REQUEST['a'],'add')))
    $page='issuetype.inc.php';

$nav->setTabActive('manage');
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>
