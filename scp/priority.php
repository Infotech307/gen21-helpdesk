<?php
require('admin.inc.php');
include_once(INCLUDE_DIR.'class.priority.php');

$priority=null;
if($_REQUEST['id'] && !($priority=Priority::lookup($_REQUEST['id']))) // function lookup
    $errors['err']='Unknown or invalid API key ID.';

if($_POST){
    switch(strtolower($_POST['do'])){
        case 'update':
            if(!$priority){
                $errors['err']='Unknown or invalid Priority.';
            }elseif($priority->update($_POST,$errors)){ // function update
                $msg='Priority updated successfully';
            }elseif(!$errors['err']){
                $errors['err']='Error updating prioroty. Try again!';
            }
            break;
        case 'add':
            if(($id=Priority::create($_POST,$errors))){ // function create
                $msg='Priority added successfully';
                $_REQUEST['a']=null;
            }elseif(!$errors['err']){
                $errors['err']='Unable to add Priority. Correct error(s) below and try again.';
            }
            break;
		case 'create':
            if(($id=Priority::create($_POST,$errors))){
                $msg=Format::htmlchars($_POST['priority']).' added successfully';
                $_REQUEST['a']=null;
            }elseif(!$errors['err']){
                $errors['err']='Unable to Save Priority. Correct any error(s) below and try again.';
            }
            break;
        case 'mass_process':
            if(!$_POST['ids'] || !is_array($_POST['ids']) || !count($_POST['ids'])) {
                $errors['err'] = 'You must select at least one priority.';
            } else {
                $count=count($_POST['ids']);
                switch(strtolower($_POST['a'])) {
                    case 'delete':
                        $i=0;
                        foreach($_POST['ids'] as $k=>$v) {
                            if(($p=Priority::lookup($v)) && $p->delete()) //function delete
                                $i++;
                        }

                        if($i && $i==$count)
                            $msg = 'Selected Priority deleted successfully';
                        elseif($i>0)
                            $warn = "$i of $count selected priority deleted";
                        elseif(!$errors['err'])
                            $errors['err'] = 'Unable to delete selected priority';
                        break;
                    default:
                        $errors['err']='Unknown action - get technical help.';
                }
            }
            break;
			
        default:
            $errors['err']='Unknown action/command';
            break;
    }
}

$page='priorities.inc.php';
if($priority || ($_REQUEST['a'] && !strcasecmp($_REQUEST['a'],'add')))
    $page='priority.inc.php';

$nav->setTabActive('manage');
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>
