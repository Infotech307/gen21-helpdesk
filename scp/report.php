<?php
/*********************************************************************
    helptopics.php

    Help Topics.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('staff.inc.php');
include_once(INCLUDE_DIR.'class.report.php');

$div=null;
if($_REQUEST['id'] && !($div=Division::lookup($_REQUEST['id'])))
    $errors['err']='Unknown or invalid division ID.';
if($_REQUEST['a'] == 'export') {
        require_once(INCLUDE_DIR.'class.exportreport.php');
        $ts = strftime('%Y%m%d');
        if (!($token=$_REQUEST['h']))
            $errors['err'] = 'Query token required';
        elseif (!($query=$_SESSION['search_'.$token]))
            $errors['err'] = 'Query token not found';
        elseif (!Export::saveTickets($query, "tickets-$ts.csv", 'csv'))
            $errors['err'] = 'Internal error: Unable to dump query results';
}
if($_POST){
    switch(strtolower($_POST['do'])){
        case 'update':
            if(!$div){
                $errors['err']='Unknown or invalid division.';
            }elseif($div->update($_POST,$errors)){
                $msg='Division updated successfully';
            }elseif(!$errors['err']){
                $errors['err']='Error updating division. Try again!';
            }
            break;
        case 'create':
            if(($id=Division::create($_POST,$errors))){
                $msg='Division added successfully';
                $_REQUEST['a']=null;
            }elseif(!$errors['err']){
                $errors['err']='Unable to add division. Correct error(s) below and try again.';
            }
            break;
        case 'mass_process':
            if(!$_POST['ids'] || !is_array($_POST['ids']) || !count($_POST['ids'])) {
                $errors['err'] = 'You must select at least one division';
            } else {
                $count=count($_POST['ids']);

                switch(strtolower($_POST['a'])) {
                    case 'enable':
                        $sql='UPDATE '.DIVISION_TABLE.' SET isactive=1 '
                            .' WHERE DivisionID IN ('.implode(',', db_input($_POST['ids'])).')';
                        if(db_query($sql) && ($num=db_affected_rows())) {
                            if($num==$count)
                                $msg = 'Selected divisions enabled';
                            else
                                $warn = "$num of $count selected divisions enabled";
                        } else {
                            $errors['err'] ='Unable to enable selected division(s)';
                        }
                        break;
                    case 'disable':
                        $sql='UPDATE '.DIVISION_TABLE.' SET isactive=0 '
                            .' WHERE DivisionID IN ('.implode(',', db_input($_POST['ids'])).')';
                        if(db_query($sql) && ($num=db_affected_rows())) {
                            if($num==$count)
                                $msg = 'Selected divisions disabled';
                            else
                                $warn = "$num of $count selected divisions disabled";
                        } else {
                            $errors['err'] ='Unable to disable selected division(s)';
                        }
                        break;
                    case 'delete':
                        $i=0;
                        foreach($_POST['ids'] as $k=>$v) {
                            if(($t=Division::lookup($v)) && $t->delete())
                                $i++;
                        }

                        if($i && $i==$count)
                            $msg = 'Selected divisions deleted successfully';
                        elseif($i>0)
                            $warn = "$i of $count selected divisions deleted";
                        elseif(!$errors['err'])
                            $errors['err']  = 'Unable to delete selected divisions';

                        break;
                    default:
                        $errors['err']='Unknown action - get technical help.';
                }
            }
            break;
        default:
            $errors['err']='';
            break;
    }
}

$page='report.inc.php';
if($div || ($_REQUEST['a'] && !strcasecmp($_REQUEST['a'],'add')))
    $page='report.inc.php';

$nav->setTabActive('manage');
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');
?>
