<?php
require('admin.inc.php');
global $ldapcon;
require_once(INCLUDE_DIR.'class.ldap.php');
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin()) die('Access Denied');

$ldapquery = mysql_query("select * from ".TABLE_PREFIX."ldap_config where ldap_active = 1 order by priority limit 1");
$ldapinfo = mysql_fetch_assoc($ldapquery);

if($_POST){
	if($_POST['do'] == "sync"){
		/*get request parameter*/
		$ou = $ldapinfo['ldap_ou_field'];
		$user = $ldapinfo['ldap_admin']; 		
		$password = $ldapinfo['ldap_admin_pw']?Crypto::decrypt($ldapinfo['ldap_admin_pw'],SECRET_SALT):'';
		require_once("../sync.php");
		
		$msg = $ac.' active users and '.$dac.' deactive users';
	}
	else{
		switch(strtolower($_POST['do'])){
			case 'mass_process':
				if(!$_POST['ids'] || !is_array($_POST['ids']) || !count($_POST['ids'])) {
					$errors['err'] = 'You must select at least one log to delete';
				} else {
					$count=count($_POST['ids']);
					if($_POST['a'] && !strcasecmp($_POST['a'], 'delete')) {
						$sql='DELETE FROM '.TABLE_PREFIX . 'sync_log'
							.' WHERE sync_log_id IN ('.implode(',', db_input($_POST['ids'])).')';
						if(db_query($sql) && ($num=db_affected_rows())){
							if($num==$count)
								$msg='Selected logs deleted successfully';
							else
								$warn="$num of $count selected logs deleted";
						} elseif(!$errors['err'])
							$errors['err']='Unable to delete selected logs';
					} else {
						$errors['err']='Unknown action - get technical help';
					}
				}
				break;
			default:
				$errors['err']='Unknown command/action';
				break;
		}
	}
}

$qstr='';

$qwhere =' WHERE 1';

//dates
$startTime  =($_REQUEST['startDate'] && (strlen($_REQUEST['startDate'])>=8))?strtotime($_REQUEST['startDate']):0;
$endTime    =($_REQUEST['endDate'] && (strlen($_REQUEST['endDate'])>=8))?strtotime($_REQUEST['endDate']):0;
if( ($startTime && $startTime>time()) or ($startTime>$endTime && $endTime>0)){
    $errors['err']='Entered date span is invalid. Selection ignored.';
    $startTime=$endTime=0;
}else{
    if($startTime){
        $qwhere.=' AND date>=FROM_UNIXTIME('.$startTime.')';
        $qstr.='&startDate='.urlencode($_REQUEST['startDate']);
    }
    if($endTime){
        $qwhere.=' AND date<=FROM_UNIXTIME('.$endTime.')';
        $qstr.='&endDate='.urlencode($_REQUEST['endDate']);
    }
}
$sortOptions=array('date'=>'date','notes'=>'notes');
$orderWays=array('DESC'=>'DESC','ASC'=>'ASC');
$sort=($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])])?strtolower($_REQUEST['sort']):'date';
//Sorting options...
if($sort && $sortOptions[$sort]) {
    $order_column =$sortOptions[$sort];
}
$order_column=$order_column?$order_column:'date';

if($_REQUEST['order'] && $orderWays[strtoupper($_REQUEST['order'])]) {
    $order=$orderWays[strtoupper($_REQUEST['order'])];
}
$order=$order?$order:'DESC';

if($order_column && strpos($order_column,',')){
    $order_column=str_replace(','," $order,",$order_column);
}
$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by="$order_column $order ";

$qselect = 'SELECT * ';
$qfrom=' FROM '.TABLE_PREFIX . 'sync_log ';
$total=db_count("SELECT count(*) $qfrom $qwhere");
$page = ($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
//pagenate
//$pageNav=new Pagenate($total, $page, PAGE_LIMIT);
$pageNav=new Pagenate($total, $page, 50);
$pageNav->setURL('syncad.php',$qstr);
$qstr.='&order='.($order=='DESC'?'ASC':'DESC');
$query="$qselect $qfrom $qwhere ORDER BY $order_by LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();
$res=db_query($query);
if($res && ($num=db_num_rows($res)))
    $showing=$pageNav->showing().' Synchronization Logs';
else
    $showing='No logs found!';
	
$nav->setTabActive('settings');
require(STAFFINC_DIR.'header.inc.php');
?>

<h2>Active Directory Synchronization</h2>
<table class="form_table settings_table" width="940" border="0" cellspacing="0" cellpadding="2">
	<form name="syncnow" method="post" action="syncad.php">
	<?php csrf_token(); ?>
	<thead>
        <tr>
            <th colspan="2">
                <h4>Synchronize</h4>
                <em>Synchronize to active directory, all user who didn't exist in active directory will be deactivated</em>
            </th>
        </tr>
    </thead>
	<tbody>
		<tr>
            <td width="220" class="required">Organizational Unit<br /><span style="color:gray;font-size:smaller">See LDAP Connection</span></td>
            <td>: <?php echo $ldapinfo['ldap_ou_field'] ?></td>
        </tr>
        <tr>
			<input type="hidden" value="sync" name="do" />
            <td width="220" class="required">Synchronize to Active Directory</td>
            <td>: <input type="submit" value="Synchronize Now" onclick="return confirm('All user who didn\'t exist in active directory will be deactivated.\nProceed Anyway?');" /></td>
        </tr>
	</form>
</table>
<br />
<div id='filter' >
 <form action="syncad.php" method="get">
    <div style="padding-left:2px;">
        <b>Date Span</b>:
        &nbsp;From&nbsp;<input class="dp" id="sd" size=15 name="startDate" value="<?php echo Format::htmlchars($_REQUEST['startDate']); ?>" autocomplete=OFF>
            &nbsp;&nbsp; to &nbsp;&nbsp;
            <input class="dp" id="ed" size=15 name="endDate" value="<?php echo Format::htmlchars($_REQUEST['endDate']); ?>" autocomplete=OFF>
            &nbsp;&nbsp;
            &nbsp;&nbsp;
            <input type="submit" Value="Go!" />
    </div>
 </form>
</div>
<form action="syncad.php" method="POST" name="syncad">
<?php csrf_token(); ?>
 <input type="hidden" name="do" value="mass_process" >
 <input type="hidden" id="action" name="a" value="" >
 <table class="list" border="0" cellspacing="1" cellpadding="0" width="940">
    <caption><?php echo $showing; ?></caption>
    <thead>
        <tr>
            <th width="7">&nbsp;</th>
            <th width="200" nowrap><a  <?php echo $date_sort; ?>href="syncad.php?<?php echo $qstr; ?>&sort=date">Sync Time</a></th>
            <th width="540"><a  <?php echo $type_sort; ?> href="syncad.php?<?php echo $qstr; ?>&sort=notes">Sync Data</a></th>
        </tr>
    </thead>
    <tbody>
    <?php
        $total=0;
        $ids=($errors && is_array($_POST['ids']))?$_POST['ids']:null;
        if($res && db_num_rows($res)):
            while ($row = db_fetch_array($res)) {
                $sel=false;
                if($ids && in_array($row['sync_log_id'],$ids))
                    $sel=true;
                ?>
            <tr id="<?php echo $row['sync_log_id']; ?>">
                <td width=7px>
                  <input type="checkbox" class="ckb" name="ids[]" value="<?php echo $row['sync_log_id']; ?>" 
                            <?php echo $sel?'checked="checked"':''; ?>> </td>
                <td>&nbsp;<?php echo $row['date']; ?></td>
                <td>&nbsp;<?php echo Format::htmlchars($row['notes']); ?></a></td>
            </tr>
            <?php
            } //end of while.
        endif; ?>
    </tbody>
    <tfoot>
     <tr>
        <td colspan="6">
            <?php if($res && $num){ ?>
            Select:&nbsp;
            <a id="selectAll" href="#ckb">All</a>&nbsp;&nbsp;
            <a id="selectNone" href="#ckb">None</a>&nbsp;&nbsp;
            <a id="selectToggle" href="#ckb">Toggle</a>&nbsp;&nbsp;
            <?php }else{
                echo 'No logs found';
            } ?>
        </td>
     </tr>
    </tfoot>
</table>
<?php
if($res && $num): //Show options..
    echo '<div>&nbsp;Page:'.$pageNav->getPageLinks().'&nbsp;</div>';
?>
<p class="centered" id="actions">
    <input class="button" type="submit" name="delete" value="Delete Selected Entries">
</p>
<?php
endif;
?>
</form>

<div style="display:none;" class="dialog" id="confirm-action">
    <h3>Please Confirm</h3>
    <a class="close" href="">&times;</a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="delete-confirm">
        <font color="red"><strong>Are you sure you want to DELETE selected logs?</strong></font>
        <br><br>Deleted logs CANNOT be recovered.
    </p>
    <div>Please confirm to continue.</div>
    <hr style="margin-top:1em"/>
    <p class="full-width">
        <span class="buttons" style="float:left">
            <input type="button" value="No, Cancel" class="close">
        </span>
        <span class="buttons" style="float:right">
            <input type="button" value="Yes, Do it!" class="confirm">
        </span>
     </p>
    <div class="clear"></div>
</div>
<?php
include(STAFFINC_DIR.'footer.inc.php');
?>
